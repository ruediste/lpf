/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpfExample

import lpf.visualBuilders._
import lpf._
import lpf.element._
import lpf.visual._
import javax.swing.JButton
import java.awt.event.ActionListener
import java.awt.event.ActionEvent
import java.awt.Color
import lpf.visual.listener.MouseOverListener
import javax.swing.JComboBox
import lpf.visualBuilders.DefaultVisualRules

class ExampleVisualRules extends VisualRuleList {

  rule {
    case l@Label("foo") => Border() -: continue()
  }
  rule {
    case l: Label if l.hasParent(_ hasId "main") =>
      RubberMargin(_.thickness = 0)(_.right = 1) -: continue
  }

  rule {
    case g @ HPanel("swing") =>
      val button = new JButton("Yeah")
      button.addActionListener(new ActionListener {
        def actionPerformed(e: ActionEvent) {
          println("Yeah clicked")
        }
      })
      val listener = MouseOverListener()

      RubberMargin(listener.target = _) -:
        Border(_.bind(_.color = listener.isMouseOver match {
          case true => Color.RED
          case false => Color.BLACK
        })) -: SwingPane(_.jComponent = (button))

  }

  rule {
    case g @ HPanel("swing2") =>
      val box = new JComboBox(Array[Object]("a", "b", "c"))
      SwingPane(_.jComponent = (box))
  }

  include(DefaultVisualRules)
}
