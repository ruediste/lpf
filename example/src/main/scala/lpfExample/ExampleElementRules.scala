/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpfExample
import lpf._
import lpf.element._
import scala.collection.immutable.Nil
import lpf.visual.Border
import lpf.binding.OneWayListBinding
import lpf.binding.TwoWayBinding
import lpf.element.GridBuilder
import lpf.visual.RubberMargin

class ExampleElementRules extends ElementRuleList {
  rule {
    case ctrl: SampleViewModel =>
      val root = Root() -:
        VPanel(_.id = "main") / {
          ComboBox(_.bind[Int](ctrl.numberList, ctrl.selectedNumber, ctrl.selectedNumber = _))

          Label() bind (_.text = ctrl.name)
          HPanel() / {
            Button(_.clicked += ctrl.toWorld()) -: Label().bind(_.text = String.format("change %s to World", ctrl.name))
            Button(_.clicked += ctrl.toHello()) -: VPanel() / {
              Button(_.clicked += ctrl.cycleSelectedNumber()) -: Label(_.text = "cycle number")
              Label().bind(_.text = String.format("change %s to Hello", ctrl.name))
            }
          }

          Grid() / { b =>
            b.columnSize(RubberSize(0.75)); Label(_.text = "hello"); b.columnSize(RubberSize(0)); Label(_.text = "World"); b.newLine()
            b.columnSpan(2); Label(_.id="foo").bind(_.text=ctrl.selectedPerson.map(_.toString()).getOrElse(""))
          }

          DataGridSingle[Person]()(_
              .column(Label(_.text = "toString"), p=>Label().bind(_.text=p.toString))
              .column(Label(_.text = "First Name"), p=>TextField().bind(p.firstName,p.firstName=_),Some(RubberSize(1)))
              .column(Label(_.text = "Last Name"), p=>TextField().bind(p.lastName,p.lastName=_))
              .bind(ctrl.personList, ctrl.selectedPerson, ctrl.selectedPerson=_)
              )
          
          TextField().bind(ctrl.name, ctrl.name = _)
          TextField().bind(ctrl.name, ctrl.name = _)

          HPanel(_.id = "swing")
          HPanel(_.id = "swing2")

          ElementList()(_.bind[Int](ctrl.numberList, ctrl.selectedNumber, ctrl.selectedNumber = _))

          TabPanel() / {
            HeaderedElement(_.header = Label(_.text = "A Header")) -: Label(_.text = "A Child")
            HeaderedElement(_.header = Label(_.text = "B Header")) -: Label(_.text = "B Child")
          }
        }
  }

  rule {
    case null => Label(_.text = "<null>")
  }

  rule {
    case o => Label(_.text = o.toString)
  }

}
