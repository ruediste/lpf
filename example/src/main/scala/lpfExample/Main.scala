/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpfExample

import lpf._
import lpf.dependency._
import lpf.visual._
import lpf.visual.listener._
import java.awt.Color
import java.awt.FileDialog
import lpf.visualBuilders.WireframeVisualRules
import java.awt.EventQueue
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.BorderFactory
import java.awt.BorderLayout
import java.awt.FlowLayout
import javax.swing.JButton

class Main {
  var foo = ""
}

case class Person(var firstName:String,var lastName:String){
  
}

class SampleViewModel {
  var name = "Hello"

  def changeName() {
    println("changing name")
    if (name.equals("Hello"))
      name = "World"
    else
      name = "Hello"
  }

  def toWorld() = name = "World"
  def toHello() = {
    name = "Hello"

  }

  val numberList = List(1, 2, 3)
  var selectedNumber: Option[Int] = Some(1)

  val personList=List(Person("Peter","Mueller"),Person("Hans","Muster"),Person("Frank","Frueh"))
  var selectedPerson:Option[Person] = None
  
  def cycleSelectedNumber() {
    selectedNumber = selectedNumber.map { number =>
      val ret = number + 1
      if (numberList.contains(ret))
        ret
      else
        1
    }
  }

}

object Main extends Referencer {
  def main(args: Array[String]): Unit = {

    EventQueue.invokeAndWait(new Runnable {
      def run() {
        val ctrl = new SampleViewModel
        val elementBuilder = ElementBuilder(new ExampleElementRules())
        val visualBuilder = VisualBuilder(elementBuilder, new ExampleVisualRules())

        val root = visualBuilder.build(elementBuilder.build(ctrl, null), null)

        //val swingRoot = JLpfPane(root)

        /*val frame = new JFrame()
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

        val group = new JPanel()
        frame.add(group)
        group.setBorder(BorderFactory.createTitledBorder("Hello"))
        group.setLayout(new BorderLayout())
        group.add(swingRoot, BorderLayout.EAST)
        group.add(new JButton("Hello"))

        //frame.pack()
        frame.setSize(400, 400)
        frame.setVisible(true) */

        val frame = Frame(root)

        frame.setSize(500, 400)
        frame.windowClosing += { java.lang.System.exit(0) }
        frame.setVisible(true)
      }
    })
    System.gc()
  }
}
