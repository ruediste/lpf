package joglExample

import javax.media.opengl.GLProfile
import javax.media.opengl.GLCapabilities
import com.jogamp.newt.opengl.GLWindow
import javax.swing.JFrame
import java.awt.Frame
import javax.swing.BoxLayout
import javax.swing.JButton
import javax.swing.JLabel
import com.jogamp.newt.awt.NewtCanvasAWT
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.media.opengl.GLEventListener
import javax.media.opengl.GL2
import javax.media.opengl.GL
import javax.swing.JPanel
import java.awt.Dimension
import com.jogamp.opengl.util.FPSAnimator
import javax.media.opengl.fixedfunc.GLMatrixFunc
import com.jogamp.newt.event.KeyAdapter
import com.jogamp.newt.event.KeyEvent

object JoglExample {

  def main(args: Array[String]): Unit = {
    GLProfile.initSingleton();

    val glp = GLProfile.getDefault();
    val caps = new GLCapabilities(glp)

    // activate FSAA
    caps.setSampleBuffers(true);
    caps.setNumSamples(4);

    val window = GLWindow.create(caps)
    window.addGLEventListener(new GLEventListener() {
      var ratio = 1.0;
      var angle = 0.0
      def display(drawable: javax.media.opengl.GLAutoDrawable): Unit = {
        //ratio -= 0.001
        if (ratio < 0)
          ratio = 1

        angle += 1

        val gl = drawable.getGL().getGL2();

        // setup the projection
        gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(0, drawable.getWidth(), drawable.getHeight(), 0, 0, 1);
        gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
        gl.glLoadIdentity()

        gl.glScissor(0, 0, drawable.getWidth(), drawable.getHeight())

        gl.glClear(GL.GL_COLOR_BUFFER_BIT)

        gl.glTranslated(drawable.getWidth()/2, drawable.getHeight()/2, 0)
        gl.glRotated(angle, 0, 0, 1)
        gl.glTranslated(-drawable.getWidth()/2, -drawable.getHeight()/2, 0)

        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor3f(1, 0, 0);
        gl.glVertex2f(0, 0);
        gl.glColor3f(0, 1, 0);
        gl.glVertex2f(drawable.getWidth() / 2, drawable.getHeight() * ratio.toFloat);
        gl.glColor3f(0, 0, 1);
        gl.glVertex2f(drawable.getWidth(), 0);
        gl.glEnd();
      }
      def dispose(drawable: javax.media.opengl.GLAutoDrawable): Unit = {

      }
      def init(drawable: javax.media.opengl.GLAutoDrawable): Unit = {
        val gl = drawable.getGL().getGL2();

        gl.glDisable(GL.GL_DEPTH_TEST)
        gl.glEnable(GL.GL_SCISSOR_TEST)
      }
      def reshape(drawable: javax.media.opengl.GLAutoDrawable, x: Int, z: Int, width: Int, height: Int): Unit = {
    	
      }
    })

    window.addKeyListener(new KeyAdapter(){
      override def keyTyped(e: KeyEvent){
        new Error().printStackTrace()
      }
    })
    val frame = new JFrame()
    frame.setTitle("SwingFrame")
    frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS))
    frame.add(new JButton("Yeah"))
    frame.add(new JLabel("myLabel"))
    val panel = new JPanel()
    panel.setSize(200, 200)
    //frame.add(panel)
    frame.add(new NewtCanvasAWT(window) {
      override def getPreferredSize() = new Dimension(0, 0)
    })
    frame.setSize(400, 300)

    frame.setVisible(true)
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

    val animator = new FPSAnimator(window, 60)
    animator.start()

    val lock = new Object()
    lock.synchronized { lock.wait() }
  }

}