package gettingStarted;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SwingSample {

	static PersonRepository personRepository = new PersonRepository();

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		{
			BoxLayout mgr = new BoxLayout(frame.getContentPane(),
					BoxLayout.PAGE_AXIS);
			frame.setLayout(mgr);
		}

		final DefaultListModel data = new DefaultListModel();
		for (Person p : personRepository.getPersons())
			data.addElement(p);

		final JList list = new JList(data);
		frame.add(list);

		JPanel detailsPanel = new JPanel();
		frame.add(detailsPanel);
		detailsPanel.setLayout(new GridLayout(0, 2));

		detailsPanel.add(new JLabel("First Name: "));
		final JLabel firstNameLabel = new JLabel();
		detailsPanel.add(firstNameLabel);

		detailsPanel.add(new JLabel("Last Name: "));
		final JLabel lastNameLabel = new JLabel();
		detailsPanel.add(lastNameLabel);

		detailsPanel.add(new JLabel("Street: "));
		final JLabel streetLabel = new JLabel();
		detailsPanel.add(streetLabel);

		detailsPanel.add(new JLabel("City: "));
		final JLabel cityLabel = new JLabel();
		detailsPanel.add(cityLabel);

		JButton closeButton = new JButton("Close");
		frame.add(closeButton);
		closeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		list.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting())
					return;

				if (list.getSelectedIndex() != -1) {
					Person p = (Person) data.get(list.getSelectedIndex());

					firstNameLabel.setText(p.getFirstName());
					lastNameLabel.setText(p.getLastName());
					streetLabel.setText(p.getStreet());
					cityLabel.setText(p.getCity());
				}
			}
		});

		frame.setSize(500, 400);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
