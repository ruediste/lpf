package gettingStarted

import lpf.instrumentation.ClassInformationAdjusterBase
import lpf.hornet.ClassInformation

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  override def adjustClassInformation(info: ClassInformation) {
	  info.setApplicationClass(true)
  }
}