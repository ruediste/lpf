package gettingStarted;

import java.util.ArrayList;

public class PersonRepository {

	private ArrayList<Person> persons=new ArrayList<Person>();
	
	public PersonRepository() {
		persons.add(new Person("Hans", "Muster", "Langstrasse", "Zuerich"));
		persons.add(new Person("Peter", "Mueller", "Steigweg", "Wald"));
		persons.add(new Person("Joseph", "Dahinden", "Muehleweg", "Dugglingen"));
	}

	public Iterable<Person> getPersons() {
		return persons;
	}
}
