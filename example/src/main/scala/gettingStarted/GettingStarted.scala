package gettingStarted

import lpf._
import lpf.element._
import lpf.visualBuilders.WireframeVisualRules
import lpf.visual._
import scala.collection.JavaConversions._
import java.awt.Color
import lpf.element.SingleSelectorLike

class SamplePresenter {
  private val personRepository = new PersonRepository()

  val persons = personRepository.getPersons().toList;

  var selectedPerson: Option[Person] = persons.headOption

  def close() = System.exit(0)
  
  def giveSelectedPersonAGift(){
    // ...
  }
}

object ElementRules extends ElementRuleList {
  rule {
    case p: Person =>
      Label(_.text=p.getFirstName()+" "+p.getLastName())
  }
  rule {
    case presenter: SamplePresenter =>
      VPanel(_.id = "root") / {
        ElementList(_.bind[Person](presenter.persons, presenter.selectedPerson, presenter.selectedPerson = _))
        VPanel(_.id = "details") / {
          HPanel() / {
            Label(_.text = "First Name: ")
            Label().bind(_.text = presenter.selectedPerson.map(_.getFirstName()).getOrElse(""))
          }
          HPanel() / {
            Label(_.text = "Last Name: ")
            Label().bind(_.text = presenter.selectedPerson.map(_.getLastName()).getOrElse(""))
          }
          HPanel() / {
            Label(_.text = "Street: ")
            Label().bind(_.text = presenter.selectedPerson.map(_.getStreet()).getOrElse(""))
          }
          HPanel() / {
            Label(_.text = "City: ")
            Label().bind(_.text = presenter.selectedPerson.map(_.getCity()).getOrElse(""))
          }
        }
        Button(_.clicked += System.exit(0)) -: Label(_.text = "Close")
      }
  }
}

object VisualRules extends VisualRuleList {
  rule {
    case g @ VPanel("root") =>
      RubberMargin() -: StackPanel() ~: g.children
    case g : VPanel if (g.parent match {
      case Some(VPanel("details")) => true
      case _ => false
    }) =>
      continue().\\[StackPanel].orientation=Orientation.horizontal
      
    case _:SingleSelectorLike =>
      Background(_.color=Color.WHITE) -: continue()
  }
  include(WireframeVisualRules)
}

object GettingStarted {

  def main(args: Array[String]): Unit = {
    val presenter = new SamplePresenter
    val frame = Frame(presenter, ElementRules, VisualRules)
    frame.windowClosing += System.exit(0)
    frame.setSize(500, 400)
    frame.setVisible(true)
  }
}