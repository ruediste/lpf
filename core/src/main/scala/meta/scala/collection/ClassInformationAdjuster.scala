package meta.scala.collection
import lpf.hornet.ClassInformation
import lpf.hornet.MethodInfo
import lpf.instrumentation.ClassInformationAdjusterBase

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  def adjustClassInformation(info: ClassInformation) {
    // GenTraversableOnce: nothing to do
  }
}