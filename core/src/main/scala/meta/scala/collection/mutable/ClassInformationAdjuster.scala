package meta.scala.collection.mutable
import lpf.hornet.ClassInformation
import lpf.hornet.MethodInfo
import lpf.instrumentation.ClassInformationAdjusterBase
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  def adjustClassInformation(info: ClassInformation) {
    if (info.isOf[ListBuffer[_]]) {
      info.isObjectHornet = true;
      for (m <- info.declaredMethods)
        m match {
          case MethodInfo("underlying", _, _) => m.readsObjectHornet = true
          case MethodInfo("writeObject", _, _) => m.readsObjectHornet = true
          case MethodInfo("readObject", _, _) => m.writesObjectHornet = true
          case MethodInfo("length", _, _) => m.readsObjectHornet = true
          case MethodInfo("size", _, _) => m.readsObjectHornet = true
          case MethodInfo("apply", _, _) => m.readsObjectHornet = true
          case MethodInfo("update", _, _) => m.writesObjectHornet = true
          case MethodInfo("$plus$eq", _, _) => m.writesObjectHornet = true
          case MethodInfo("clear", _, _) => m.writesObjectHornet = true
          case MethodInfo("$plus$eq$colon", _, _) => m.writesObjectHornet = true
          case MethodInfo("insertAll", _, _) => m.writesObjectHornet = true
          case MethodInfo("remove", _, _) => m.writesObjectHornet = true
          case MethodInfo("toList", _, _) => m.readsObjectHornet = true
          case MethodInfo("prependToList", _, _) => m.writesObjectHornet = true
          case MethodInfo("$minus$eq", _, _) => m.writesObjectHornet = true
          case MethodInfo("iterator", _, _) => m.readsObjectHornet = true
          case MethodInfo("equals", _, _) => m.readsObjectHornet = true
          case MethodInfo("clone", _, _) => m.readsObjectHornet = true
          case _ =>
        }
    }

    if (info.isOf[ArrayBuffer[_]]) {
      info.isObjectHornet = true;
      for (m <- info.declaredMethods)
        m match {
          case MethodInfo("length", _, _) => m.readsObjectHornet = true
          case MethodInfo("apply", _, _) => m.readsObjectHornet = true
          case MethodInfo("update", _, _) => m.writesObjectHornet = true
          case MethodInfo("foreach", _, _) => m.readsObjectHornet = true
          case MethodInfo("copyToArray", _, _) => m.readsObjectHornet = true
          case MethodInfo("reduceToSize", _, _) => m.writesObjectHornet = true
          case MethodInfo("ensureSize", _, _) => m.writesObjectHornet = true
          case MethodInfo("swap", _, _) => m.writesObjectHornet = true
          case MethodInfo("copy", _, _) => m.writesObjectHornet = true
          case _ =>
        }
    }
  }
}