package meta.scala.collection.generic
import lpf.hornet.ClassInformation
import lpf.hornet.MethodInfo
import lpf.instrumentation.ClassInformationAdjusterBase

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  def adjustClassInformation(info: ClassInformation) {
    if (info.name.equals("scala.collection.generic.GenericCompanion")) {
      info.declaredMethods.filter(_.name.equals("apply")).foreach(_.isObjectHornetFactory = true)
    }
  }
}