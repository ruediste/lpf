package meta

import lpf.instrumentation.PackageAdviceProviderBase

class AdviceProvider extends PackageAdviceProviderBase {
 override def needsInstrumentation(loader: ClassLoader, className: String, bytes: Array[Byte]): Option[Boolean] = {
    if (className.startsWith("$Proxy"))
      Some(false)
    else
      None
  }

}