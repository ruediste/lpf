/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.dependency
import scala.ref.PhantomReference
import scala.ref.Reference
import scala.ref.ReferenceQueue
import scala.collection.mutable.Map

object HornetReadEventDispatcher {

  /**
   * Execute func using the listener. If isSubListener is true, change events will be forwarded to
   * the current listener.
   *
   * In most cases, [[lpf.dependency.HornetEventListener.notifyOfChanges]] will be more convenient
   */
  def withListener[A](listener: HornetEventListener)(func: => A): A =
    withListener(Some(listener))(func)

  /**
   * Execute func using the listener. If isSubListener is true, change events will be forwarded to
   * the current listener.
   *
   * In most cases, [[lpf.dependency.HornetEventListener.notifyOfChanges]] will be more convenient
   */
  def withListener[A](listener: Option[HornetEventListener])(func: => A): A = {
    // install listener
    val parent = currentListener.get()
    val parentFunction = currentFunction.get()

    currentListener.set(listener)

    try {
      // call function
      func
    } finally {
      // restore parent capturer
      currentListener.set(parent)
      currentFunction.set(parentFunction)
    }
  }

  /**
   * Notify the current listener that an object hornet is beeing read
   */
  def hornetWillBeRead(target: AnyRef) {
    if (!currentFunction.get().exists(f => f == target))
      currentListener.get().foreach { _.hornetWillBeRead(target) }
  }

  /**
   * Notify the current listener that a field hornet is beeing read
   */
  def hornetWillBeRead(target: AnyRef, fieldName: String) {
    if (!currentFunction.get().exists(f => f == target))
      currentListener.get().foreach { _.hornetWillBeRead(target, fieldName) }
  }
  
  def hornetWillBeRead(targetClass: Class[_], fieldName: String){
    hornetWillBeRead(targetClass.asInstanceOf[AnyRef], fieldName)
  }

  private val currentListener = new ThreadLocal[Option[HornetEventListener]]() {
    override def initialValue: Option[HornetEventListener] = {
      None
    }
  }

  /**
   * contains the function which is currently executed using the [[withListener()]] function
   */
  private val currentFunction = new ThreadLocal[Option[AnyRef]]() {
    override def initialValue: Option[AnyRef] = {
      None
    }
  }
}
