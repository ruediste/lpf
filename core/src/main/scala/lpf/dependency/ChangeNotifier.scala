package lpf.dependency

import lpf.Referencer
import lpf._

/**
 * Executes a function and calls a notification function if any hornet read by this
 * function is written. Thread safe.
 */
class ChangeNotifier extends Referencer {
  @unobserved private var currentListener: Option[HornetEventListener] = None

  // set to true while the function is executing
  @unobserved private var isExecuting = false
  @unobserved private var alreadyNotified = false

  /**
   * Evaluate function and observe the hornets read. When any of these hornets is written, call
   * notificationFunc. After calling function, setter is called with the result of function.
   * Thread Safe.
   *
   * Warning: notificationFunc() is called within the thread which writes a hornet. Be aware
   * of threading issues.
   *
   * Properties:
   * - notificationFunc is called in the thread which writes the hornet.
   * - notificationFunc is only called function and setter are executed, but possibly before
   *   notifyOfChanges() returns.
   * - the notification function is maximally called once in total per call to notifyOfChanges()
   * - during the execution of function and setter, changes of the same thread are ignored. Changes
   *   caused by other threads block these threads until function and setter are executed.
   * - Only one thread can be within notifyOfChanges() at any given time. The others have to wait.
   * - Recursive calls whithin one thread cause an exception.
   */
  def notifyOfChanges[A](notificationFunc: => Unit, function: => A, singleNotificationCall: Boolean = true): A =
    this.synchronized {
      if (isExecuting)
        throw new RuntimeException("recursive call to notifyOfChanges() detected");
      isExecuting = true

      // create new listener
      currentListener.foreach(_.disable)
      currentListener = Some(HornetEventListener());

      // setup the callback for the listener
      currentListener.get.hornetWritten += this.synchronized {
        // while executing notifyOfChanges(), only the thread executing notifyOfChanges() can
        // enter the lock. Since changes of the same thread are ignored, notificationFunc is not
        // called when isExecuting is set to true.
        if (!isExecuting && (!alreadyNotified || !singleNotificationCall)) {
          alreadyNotified = true
          notificationFunc
        }
      }

      alreadyNotified = false

      // call the function with the listener
      try {
        HornetReadEventDispatcher.withListener(currentListener.get)(function)
      } finally {
        isExecuting = false
      }

    }
  
   def showDependencies() {
    println("Dependencies: ")
    currentListener.foreach { l =>
      println(HornetWrittenEventDispatcher.registrationsForListener(l).mkString("\n"))
    }
  }

  def ignoreChangesFromCurrentThread[A](f: => A) = this.synchronized {
    val oldIsExecuting = isExecuting
    isExecuting = true
    try {
      f
    } finally { isExecuting = oldIsExecuting }
  }
}

object ChangeNotifier {
  def apply() = new ChangeNotifier()
}