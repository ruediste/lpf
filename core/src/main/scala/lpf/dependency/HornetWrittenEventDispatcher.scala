/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.dependency
import java.util.Collections
import scala.collection.mutable._
import lpf.util._
import scala.collection.mutable.Map
import scala.ref._
import scala.collection.{ concurrent => concurrent }

object HornetWrittenEventDispatcher {
  val fieldHornetMap = MapMaker().weakKeys.makeMap[AnyRef, concurrent.Map[String, Set[HornetEventListener]]]
  val objectHornetMap = MapMaker().weakKeys.makeMap[AnyRef, Set[HornetEventListener]]

  def registrationsForListener(listener: HornetEventListener): scala.collection.Iterable[(AnyRef, Option[String])] = {
    return fieldHornetMap.toIterable.flatMap(
      tuple =>
        // take ClassMember->Listener set
        tuple._2.toIterable.
          // keep only the entries where the listener set contains the listener
          filter(_._2.contains(listener)).
          // for each entry, construct a tuple with the target and the field name
          map(t => (tuple._1, Some(t._1)))) ++ objectHornetMap.toIterable.
      filter(_._2.contains(listener)).
      map(t => (t._1, None))

  }

  /**
   * sends a written event for a field hornet
   */
  def hornetWritten(obj: AnyRef, fieldName: String) {
    //println("hornet written: "+fieldName)
    fieldHornetMap.get(obj).flatMap { _.get(fieldName) }.toIterable.flatten.foreach { _.receiveHornetWritten }
  }

  /**
   * sends a written event for a static field hornet
   */
  def hornetWritten(clazz: Class[_], fieldName: String) {
    hornetWritten(clazz.asInstanceOf[AnyRef],fieldName)
  }
  
  /**
   * sends a written event for an object hornet
   */
  def hornetWritten(hornet: AnyRef) {
    objectHornetMap.get(hornet).toIterable.flatten.foreach { _.receiveHornetWritten }
  }

  def register(listener: HornetEventListener, hornet: AnyRef) {
    // get the listenerSet for the object hornet
    objectHornetMap.putIfAbsent(hornet, MapMaker().weakKeys().makeSet())
    val listenerSet = objectHornetMap.get(hornet).get

    // add the listener to the listener set of the object hornet
    listenerSet += listener
  }

  def register(listener: HornetEventListener, target: AnyRef, fieldName: String) {

    // get the map for the target
    fieldHornetMap.putIfAbsent(target, { MapMaker().makeMap() })
    val targetMap = fieldHornetMap.get(target).get

    // get the listenerSet for the member
    targetMap.putIfAbsent(fieldName, MapMaker().weakKeys().makeSet())
    val listenerSet = targetMap.get(fieldName).get

    // add the listener to the set of listening listeners
    listenerSet += listener

    //printf("registered %s\n", dependency)
  }

  def clear() = {
    fieldHornetMap.clear()
    objectHornetMap.clear()
  }
}
