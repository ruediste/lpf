package lpf.dependency

import lpf.Referencer
import lpf._

/**
 * A cache used to cache the result of a method evalueation.
 * The cache is invalidated, as soon as one of the accessed
 * hornets changes. Thread safe
 */
class Cache[A](showDependencies: Boolean, f: () => A) extends Referencer {

  private val notifier = ChangeNotifier()
  @unobserved private var isExecuting = false

  /**
   * holds the cached value, if available
   */
  private var _value: Option[A] = None

  def value = notifier.synchronized {
    if (isExecuting)
      throw new RuntimeException("recursive call to Cache.value detected")
    isExecuting = true;
    try {
      // the value is accessed by the if, thus any outer change listener will observe it
      if (_value.isEmpty) {
        val tmp = notifier.notifyOfChanges(_value = None, f())
        if (showDependencies)
          notifier.showDependencies()
        _value = Some(tmp)
        tmp
      } else
        _value.get
    } finally { isExecuting = false }
  }

}

object Cache {
  def apply[A](f: => A) = new Cache(false, () => f)
  def apply[A](showDependencies: Boolean)(f: => A) = new Cache(showDependencies, () => f)
}