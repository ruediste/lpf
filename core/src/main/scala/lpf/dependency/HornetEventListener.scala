/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.dependency
import scala.ref.WeakReference
import scala.ref.ReferenceQueue
import scala.ref.WeakReferenceWithWrapper
import scala.collection.mutable.Map
import scala.ref.Reference
import scala.ref.PhantomReference
import scala.collection.mutable.Set
import lpf._
import scala.collection.mutable.HashSet

/**
 * Captures dependencies of a function and listens for changes to these dependencies afterwards
 */
class HornetEventListener() extends Referencer {
  @volatile private var disabled: Boolean = false

  def hornetWillBeRead(target: AnyRef): Unit = {
    // register the listener for the target and member
    HornetWrittenEventDispatcher.register(HornetEventListener.this, target);
  }

  def hornetWillBeRead(target: AnyRef, fieldName: String): Unit = {
    // register the listener for the target and member
    HornetWrittenEventDispatcher.register(HornetEventListener.this, target, fieldName);
  }

  /**
   * fired whenever one of the dependencies changes
   */
  val hornetWritten = WeakEventNoArgs()

  /**
   * fired whenever one of the dependencies changes, but before
   * dependencyChanged is fired.
   */
  val previewHornetWritten = WeakEventNoArgs()

  /**
   * notify the listener of a change
   */
  def receiveHornetWritten() = raise

  private def raise() {
    //println("DependencyListener: raise()")
    if (!disabled) {
      previewHornetWritten()
      hornetWritten()
    }
  }

  def disable: Unit = disabled = true

  def enable: Unit = disabled = false
}

/**
 * Captures Dependencies
 */
object HornetEventListener {
  def apply() = new HornetEventListener()
}

