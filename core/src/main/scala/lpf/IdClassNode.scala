/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */

package lpf

trait IdClassNode {
  /**
   * The id of the node, should be unique within a part of the tree, but
   * this is not enforced in any way
   */
  var id = ""

  /**
   * the set of classes of the element
   */
  var cls = Set[String]()

  /**
   * Check if the node has the given id
   */
  def hasId(s: String) = id.equals(s)

  /**
   * Check if the element has all classes specified by clazz.
   * Multiple classes can be space separated
   */
  def hasClass(clazz: String) = clazz.split(' ').forall(cls.contains(_))
}

trait IdClassNodeObject {
  type Self <: IdClassNode
  def unapply(e: Self) = Some(e.id)
}