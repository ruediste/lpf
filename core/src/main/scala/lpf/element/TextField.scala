package lpf.element

import lpf.Element
import lpf.ElementObject
import lpf.binding.TwoWayBinding
import lpf.StrongEventNoArgs
import lpf.ElementBuilder

/**
 * Represents an editable string with an optional caret
 * position and selection.
 *
 * Text positions are given as character indexes, where
 * the position is always to the left of the character.
 * Thus the positions range from 0 to value.length()
 *
 * Internally, both the caret and the selection are
 * represented as TextPositions, to allow easy adjusting
 * of the positions upon inserts and deletes.
 */
class EditableString {
  private case class TextPosition(var index: Int) {}
  private var _value = ""

  def value = _value

  /**
   * Set the value of the string and clear caret and selection
   */
  def value_=(s: String) = {
    _caretPosition = None
    _selection = None
    _value = s
  }

  private var _caretPosition: Option[TextPosition] = None
  def caretPosition = _caretPosition.map(_.index)
  def caretPosition_=(pos: Option[Int]) { _caretPosition = pos.map(TextPosition(_)); checkValid() }
  def placeCaretToEnd(){
    _caretPosition=Some(TextPosition(value.length()))
  }
  
  private var _selection: Option[(TextPosition, TextPosition)] = None
  def selection = _selection.map(p => (p._1.index, p._2.index))
  def selection_=(sel: Option[(Int, Int)]) {
    _selection = sel.map(p => (TextPosition(p._1), TextPosition(p._2)))
    checkValid()
  }

  private def textPositions = _caretPosition.toList ++: _selection.toList.flatMap(p => List(p._1, p._2))
  def insert(position: Int, s: String) {
    assert(position <= value.length(), "position has to be within the string")

    textPositions.foreach { tp =>
      if (tp.index >= position) tp.index += s.length()
    }
    _value = value.substring(0, position) + s + value.substring(position)
    checkValid()
  }

  def delete(position: Int, count: Int) {
    textPositions.foreach { tp =>
      if (tp.index > position) tp.index -= count
    }
    _value = value.substring(0, position) + value.substring(position + count)
    checkValid()
  }

  private def checkValid() {
    val length = value.length()
    textPositions.foreach { tp =>
      if (tp.index > length || tp.index < 0)
        throw new Error("invalid caret position or selection")
    }

  }
}

object EditableString {
  def apply() = new EditableString()
}
class TextField extends Element {
  type Self = TextField
  def self = this

  var _text: String = ""
  def text = _text
  def text_=(s: String) {
    _text = s
    currentText.selection = None
    currentText.caretPosition = None
    currentText.value = s
  }

  val currentText = new EditableString()

  def bind(getter: => String, setter: String => Unit)(implicit theElementBuilder: ElementBuilder) = {
    TwoWayBinding(theElementBuilder.dispatcher)(
      getter, text = _: String,
      text, setter)
    text = getter
    self
  }

}

object TextField extends ElementObject {
  type Self = TextField
  override def apply() = new TextField()
}
