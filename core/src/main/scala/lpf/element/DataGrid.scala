package lpf.element

import lpf.Element
import lpf.ElementObject
import lpf.ElementObjectNoDefaultApply
import scala.collection.mutable.ListBuffer
import lpf.ParentElementLike
import lpf.binding.OneWayListBinding
import lpf.ElementBuilder
import lpf.ReferenceKey
import lpf.binding.TwoWayBinding

case class DataGridColumn[T](
  var header: Element,
  var getter: T => Element,
  var columnSize: Option[GridSize]) {
}

abstract class DataGridLike[T] extends ParentElementLike {
  var _rows = ListBuffer[List[Element]]()
  var _columns = ListBuffer[DataGridColumn[T]]()

  def rows = _rows.toList
  def columns = _columns.toList

  override def childrenImp = _columns.map(_.header) ++ _rows.flatten

  def column(header: Element, getter: T => Element, size: Option[GridSize] = None) = {
    if (!_rows.isEmpty)
      throw new Error("data can be added only if data is empty")
    _columns.append(DataGridColumn(header, getter, size))
    header.addedAsChildOf(this)
    self
  }

  def addRow(row: List[Element]){
    _rows.append(row)
    row.foreach(_.addedAsChildOf(this))
  }
  
  override def childHasBeenRemovedImpl(child: Element) {
    // not supported for grids, seems to be unimportant
    throw new NotImplementedError()
  }
  
  def clearData(){
    _rows.foreach(_.foreach(_.removedAsChildOf(this)))
    _rows.clear()
  }
  
  def clearColumns(){
    _columns.foreach(_.header.removedAsChildOf(this))
    _columns.clear()
  }
  
  def bind(f: =>Iterable[T])(implicit theElementBuilder: ElementBuilder)={
    OneWayListBinding.cache[T,List[Element]](theElementBuilder.dispatcher)(f)(t=> 
      columns.map(column=>column.getter(t))){l=>
      clearData()
      l.foreach(addRow(_))
    }
    self
  }
}


object DataGrid extends ElementObjectNoDefaultApply {
  def apply[T]() = new DataGrid[T]()
}

class DataGrid[T] extends DataGridLike[T] {
  type Self = DataGrid[T]
  def self = this
}

class DataGridSingle[T] extends DataGridLike[T] {
  
  type Self=DataGridSingle[T]
  def self=this
  
  private var _selectedRow: Option[List[Element]] = None
  
  def selectedRow=_selectedRow
  def trySelectRow(index: Int){
    _selectedRow=Some(rows(index))
  }
  
   def bind(f: =>Iterable[T], selectionGetter: => Option[T], selectionSetter: Option[T] => Unit)(implicit theElementBuilder: ElementBuilder)={
    val binding = OneWayListBinding.cache[T,List[Element]](theElementBuilder.dispatcher)(f)(t=> 
      columns.map(column=>column.getter(t))){l=>
      clearData()
      l.foreach(addRow(_))
    }
    
    TwoWayBinding[Option[T], Option[List[Element]]](theElementBuilder.dispatcher)(
      selectionGetter, x => { _selectedRow = x.flatMap(binding.mapping.get(_, 0)); },
      _selectedRow, x => selectionSetter(x.flatMap(binding.mapping.inverseGet(_))))
    self
  }
}

object DataGridSingle extends ElementObjectNoDefaultApply {
  def apply[T]() = new DataGridSingle[T]()
}
