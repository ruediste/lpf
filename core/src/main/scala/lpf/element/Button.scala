/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.element
import lpf._

class Button extends SingleChildElement {
	type Self=Button
	def self=this
	
	val clicked=StrongEvent[Unit]()
	def raiseClicked()={
		clicked()
	}
}

object Button extends ElementObject{
  type Self=Button
  override def apply()= new Button()
}
