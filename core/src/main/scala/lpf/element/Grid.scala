package lpf.element

import lpf.Element
import scala.collection.mutable.HashMap
import lpf.ParentElementLike
import lpf.ElementObject
import scala.collection.mutable.ListBuffer
import lpf.ElementObjectNoDefaultApply

class GridSize {}
case class FractionalSize(value: Double) extends GridSize {}
case class AbsoluteSize(value: Double) extends GridSize {}
case class RubberSize(value: Double) extends GridSize {}

/**
 * A grid with columns, rows, supporting cells spanning multiple rows or columns.
 * 
 * Typically used together with the GridBuilder. Example:
 * Grid() /{b=> b.columnSize(FractionalSize(0.75)); Label(_.text="Hello"); Label(_.text="World") b.newLine();
 * }
 */
class Grid extends ParentElementLike {

  type Self = Grid
  def self = this


  class Cell(val content: Element) {
    var columnSpan = 1
    var rowSpan = 1
  }

  var columnSizes = List[Option[GridSize]]()
  var rowSizes = List[Option[GridSize]]()

  private val _cellMap = HashMap[(Int, Int), Cell]()

  def cellMap: scala.collection.Map[(Int, Int), Cell] = _cellMap

  override protected def childrenImp: Iterable[Element] =
    _cellMap.values.map(_.content)

  override protected def childHasBeenRemovedImpl(removedChild: Element) {
    _cellMap.filter(_._2.content == removedChild).map(_._1).toList.foreach(_cellMap.remove(_))
  }

  def putCell(c: Int, r: Int, cell: Cell): Option[Cell] = {
    val res = _cellMap.put((c, r), cell)
    res.foreach(_.content.removedAsChildOf(this))
    cell.content.addedAsChildOf(this)
    res
  }

  def removeCell(c: Int, r: Int) = {
    val res = _cellMap.remove((c, r))
    res.foreach(_.content.removedAsChildOf(this))
    res
  }
}

object Grid extends ElementObjectNoDefaultApply {
  type Self = Grid
  def apply() = new GridBuilder()
}
