package lpf.element

import lpf.Element
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.collection.mutable.Set

/**
 * Represents a cursor in a grid, which also manages
 * the occupied cells
 */
class GridCursor {
  var column = 0
  var row = 0

  private val occupiedCells = Set[(Int, Int)]()

  def occupyCells(colSpan: Int, rowSpan: Int) {
    for (
      c <- 0 until colSpan;
      r <- 0 until rowSpan
    ) {
      if (!occupiedCells.add((column + c, row + r)))
        throw new Exception("cell already occupied: " + (column + c, row + r))
    }
  }

  /**
   * Move the cursor one cell to the right, skipping
   * occupied cells
   */
  def goRight() {
    column += 1
    goRightToEmptyCell()
  }

  private def goRightToEmptyCell() {
    while (occupiedCells.contains((column, row)))
      column += 1
  }

  def newLine(){
    row+=1
    column=0
    goRightToEmptyCell()
  }
}

class GridBuilder {
  class Event {}
  case class InstantiateElementEvent(element: Element) extends Event {}
  case class ColumnSpanEvent(n: Int) extends Event {}
  case class RowSpanEvent(n: Int) extends Event {}
  case class ColumnSizeEvent(size: GridSize) extends Event {}
  case class RowSizeEvent(size: GridSize) extends Event {}
  case class NewLineEvent() extends Event {}

  val events = ListBuffer[Event]()

  def newLine() {
    events.append(NewLineEvent())
  }

  def columnSize(size: GridSize) {
    events.append(ColumnSizeEvent(size))
  }
  
  def rowSize(size: GridSize) {
    events.append(RowSizeEvent(size))
  }
  
  def columnSpan(columnSpan: Int){
    events.append(ColumnSpanEvent(columnSpan))
  }

  def rowSpan(rowSpan: Int){
    events.append(RowSpanEvent(rowSpan))
  }

  def /(f: GridBuilder => Unit) {
    Element.observeInstantiations(
      e => events.append(InstantiateElementEvent(e)),
      f(this))
    build()
  }

  private def build() = {
    val cursor = new GridCursor()
    var rowSpan = 1
    var columnSpan = 1
    val columnSizes = Map[Int, GridSize]()
    val rowSizes = Map[Int, GridSize]()

    val occupiedCells = Set[(Int, Int)]()
    val grid = new Grid()

    for (event <- events) event match {
      case InstantiateElementEvent(e) if (e.parent.isEmpty) =>
        val cell = new grid.Cell(e)
        cell.rowSpan = rowSpan
        cell.columnSpan = columnSpan
        cursor.occupyCells(columnSpan, rowSpan)
        grid.putCell(cursor.column, cursor.row, cell)
        cursor.goRight()
        rowSpan = 1
        columnSpan = 1

      case NewLineEvent() => cursor.newLine()

      case ColumnSpanEvent(n) => columnSpan = n
      case RowSpanEvent(n) => rowSpan = n
      
      case ColumnSizeEvent(s) => columnSizes(cursor.column) = s
      case RowSizeEvent(s)=> rowSizes(cursor.row)=s
    }

    if (columnSizes.size > 0) grid.columnSizes = ((0 to columnSizes.keys.max) map (columnSizes.get(_))).toList
    if (rowSizes.size > 0) grid.rowSizes = ((0 to rowSizes.keys.max) map (rowSizes.get(_))).toList
  }
}

object GridBuilder {
  def apply() = new GridBuilder
}