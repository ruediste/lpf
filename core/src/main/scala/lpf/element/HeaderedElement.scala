package lpf.element

import lpf._

class HeaderedElement extends SingleChildElement {
  type Self = HeaderedElement
  def self = this

  var _header: Option[Element] = None

  def header = _header

  def header_=(o: Option[Element]) {
    _header.foreach(_.removedAsChildOf(this))
    _header = o
    _header.foreach(_.addedAsChildOf(this))
  }

  def header_=(h: Element): Unit = header = Some(h)
  
  override def childHasBeenRemovedImpl(removedChild: Element) {
	  if (_header.exists(_==removedChild)) _header=None
	  else
	    super.childHasBeenRemovedImpl(removedChild);
  }

  override def childrenImp: Iterable[Element] = {
    super.childrenImp++header
  }
}

object HeaderedElement extends ElementObject{
  type Self=HeaderedElement
  def apply()=new HeaderedElement()
  
  val HEADER=new Object()
  val CHILD=new Object()
}