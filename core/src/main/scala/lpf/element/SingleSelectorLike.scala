package lpf.element

import lpf._
import lpf.binding.OneWayListBinding
import lpf.binding.TwoWayBinding
import lpf.binding.OneWayBinding

abstract class SingleSelectorLike(selectFirstByDefault: Boolean) extends MultiChildrenElementLike {
  private var _selectedElement: Option[Element] = None

  def selectedElement = _selectedElement
  def selectedElement_=(o: Option[Element]): Unit = {
    if (o.isEmpty) clearSelectedItem()
    else
      _selectedElement = o
  }

  def clearSelectedItem() {
    _selectedElement = if (selectFirstByDefault)
      children.headOption else None

  }

  override def addChild(ch: Element) = {
    super.addChild(ch)
    if (selectFirstByDefault && selectedElement.isEmpty) selectedElement = Some(ch)
  }

  override def removeChild(child: Element): Unit = {
    // deselect the selected item if it has been removed
    if (_selectedElement.exists(_.equals(child))) clearSelectedItem()
    super.removeChild(child)
  }

  override def childHasBeenRemoved(removedChild: Element) {
    // deselect the selected item if it has been removed
    if (_selectedElement.exists(_ == removedChild)) clearSelectedItem()

    super.childHasBeenRemoved(removedChild);
  }

  def bind[A <: Any](dataGetter: => Iterable[A], selectionGetter: => Option[A], selectionSetter: Option[A] => Unit)(implicit theElementBuilder: ElementBuilder) {
    val binding = OneWayListBinding.cache[A, Element](theElementBuilder.dispatcher)(dataGetter)(theElementBuilder.build(_, SingleSelectorLike.this))(replaceChildren(_))
    TwoWayBinding[Option[A], Option[Element]](theElementBuilder.dispatcher)(
      selectionGetter, x => { selectedElement = x.flatMap(binding.mapping.get(_, 0)); },
      selectedElement, x => selectionSetter(x.flatMap(binding.mapping.inverseGet(_))))
  }
}