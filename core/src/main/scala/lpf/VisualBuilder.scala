/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import _root_.lpf.element.Label
import scala.collection.mutable.ListBuffer
import scala.collection.immutable.List
import scala.collection.mutable.Buffer
import lpf.dependency.HornetReadEventDispatcher

class VisualRuleList extends BuildingRuleList[Element, AnyRef, Visual] with BuilderDSL {
  implicit def theVisualBuilder = finalContext.asInstanceOf[VisualBuilder]
}

/**
 * Base class for visual builders.
 *
 * The element builder is used in case data objects have to be converted to elements.
 */
class VisualBuilder(elementBuilder: ElementBuilder, val ruleList: VisualRuleList) extends BuilderLike[Element, AnyRef, Visual] {

  def dispatcher=elementBuilder.dispatcher

  private class Context(val source: Element, val context: Any) {
    val elements = ListBuffer[Element]()
    val visuals = ListBuffer[Visual]()

    def rootNodes = (elements ++ visuals).filter(_.parent.isEmpty)

    /**
     * return all nodes without parents
     */
    def rootOption: Option[TreeNode[_, _]] = {
      // filter the elements and visuals which have no parent
      val roots = rootNodes

      // make sure there at most one node left
      if (roots.size > 1)
        throw new Error("too many root nodes: " + roots)

      return roots.headOption
    }
  }

  @unobserved private var context: Option[Context] = None

  override def rootOption = context.get.rootOption

  def root: AnyRef = {
    rootOption match {
      case None => throw new Error(String.format("no root nodes. for source %s, requester %s,\n elements: %s,\n visuals: %s", context.get.source, context.get.elements, context.get.visuals))
      case Some(a) => a
    }
  }

  def build(source: Element, context: Any): Visual = build(source, context, applyRules(source, context))

  /**
   * recursively apply a style
   */
  private def build(source: Element, ctx: Any, ruleApplicationFunc: => Unit): Visual =
    // the build process is never observed by any listener
    HornetReadEventDispatcher.withListener(None) {

      val oldContext = context
      context = Some(new Context(source, ctx))

      try {
        val localContext = context
        // observe element and visual instantiations
        Element.observeInstantiations(e => localContext.get.elements += e,
          Visual.observeInstantiations(v => localContext.get.visuals += v, {
            ruleApplicationFunc

            // if no match is found, construct a label or red background by default
            if (rootOption.isEmpty)
              throw new Exception("no root node constructed for source " + source)
          }))

        root match {

          case v: Visual => {
            //println("root: " + v)
            // set the templated element of the visual
            v.templatedElement = Some(source)

            // notify the parent observer of the visual
            Visual.notifyInstantiationObserver(v)

            // return the visual
            v
          }

          case e: Element => {
            //println("root: " + e)
            // set the parent of the element and the templated data
            e.setParentOneWay(source.parent)
            e.templatedData = Some(source)

            // recursively apply style				
            build(e, ctx)
          }

          // there has to be a visual or an element
          case a: Any => throw new Error("should not happen: " + a)
        }
      } finally {
        //println("context restored, roots: "+oldContext.map(_.rootNodes))
        context = oldContext
      }
    }

  override def continue(): Visual = build(context.get.source, context.get.context, continueRuleApplication())
}

object VisualBuilder {
  def apply(elementBuilder: ElementBuilder, ruleList: VisualRuleList) = new VisualBuilder(elementBuilder, ruleList)
}
