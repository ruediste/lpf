/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.event
import lpf.Visual
import java.awt.event.InputEvent
import java.awt.event.{ MouseEvent => AwtMouseEvent }
import java.awt.geom.AffineTransform
import java.awt.geom.Point2D
import lpf.Point

class MouseEvent(
  val target: Visual,
  val eventType: VisualEventType[MouseEvent],
  awtEvent: java.awt.event.MouseEvent) extends VisualEvent {
  type This = MouseEvent

  var x: Double = awtEvent.getX()
  var y: Double = awtEvent.getY()
  
  lazy val modifiers: Set[EventModifier.EventModifier] = EventModifier.getModifiers(awtEvent.getModifiersEx())
  lazy val button: MouseButton.MouseButton = MouseButton.getMouseButton(awtEvent.getButton()).get

  override def transformCoodinates(t: => AffineTransform) {
    val p = new Point2D.Double(awtEvent.getX(), awtEvent.getY())
    t.inverseTransform(p, p)
    x = p.getX()
    y = p.getY()
  }
  
  def point = Point(x,y)
}

object MouseButton extends Enumeration {
  type MouseButton = Value
  val Button3 = Value
  val Button2 = Value
  val Button1 = Value
  val NoButton = Value

  def getMouseButton(button: Int) = mapping.find(_._1 == button).map(_._2)

  private val mapping = List(
    AwtMouseEvent.BUTTON1 -> Button1,
    AwtMouseEvent.BUTTON2 -> Button2,
    AwtMouseEvent.BUTTON3 -> Button3,
    AwtMouseEvent.NOBUTTON -> NoButton)
}

object EventModifier extends Enumeration {
  type EventModifier = Value
  val AltGraph = Value
  val Button3 = Value
  val Button2 = Value
  val Button1 = Value
  val Alt = Value
  val Meta = Value
  val Ctrl = Value
  val Shift = Value

  def getModifiers(modifiersEx: Int) = masks.filter(t => (modifiersEx & t._1) != 0).map(_._2).toSet

  val buttonModifiers = Set(Button1, Button2, Button3)

  private val masks = List(
    InputEvent.ALT_GRAPH_DOWN_MASK -> AltGraph,
    InputEvent.BUTTON3_DOWN_MASK -> Button3,
    InputEvent.BUTTON2_DOWN_MASK -> Button2,
    InputEvent.BUTTON1_DOWN_MASK -> Button1,
    InputEvent.ALT_DOWN_MASK -> Alt,
    InputEvent.META_DOWN_MASK -> Meta,
    InputEvent.CTRL_DOWN_MASK -> Ctrl,
    InputEvent.SHIFT_DOWN_MASK -> Shift)
}

object MouseEventType {
  val entered = new VisualEventType[MouseEvent]("entered", RoutingStrategy.direct)
  val leaved = new VisualEventType[MouseEvent]("leaved", RoutingStrategy.direct)
  val pressed = new VisualEventType[MouseEvent]("pressed", RoutingStrategy.bubbling)
  val released = new VisualEventType[MouseEvent]("released", RoutingStrategy.bubbling)
  val moved = new VisualEventType[MouseEvent]("moved", RoutingStrategy.bubbling)
  val dragged = new VisualEventType[MouseEvent]("dragged", RoutingStrategy.bubbling)
  val clicked = new VisualEventType[MouseEvent]("clicked", RoutingStrategy.bubbling)
}
