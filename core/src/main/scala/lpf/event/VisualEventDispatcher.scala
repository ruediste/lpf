/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.event
import lpf._
import _root_.lpf.TreeNode
import scala.collection.mutable.Set
import util.MapMaker
import scala.collection.immutable.Stack
import java.awt.geom.AffineTransform

class FocusHandle {}

/**
 * Dispatches events along the Visual hierarchy
 */
object VisualEventDispatcher {

  private val observersKey = ReferenceKey(MapMaker().makeMap[VisualEventType[_], Set[(_ => Unit, Boolean)]]())

  /**
   * register a function for an eventType on a node. If handlesToo is set to true,
   * the function will be called even if an event is already marked as handled
   */
  def register[TEvent <: VisualEvent](node: Visual, eventType: VisualEventType[TEvent], handlesToo: Boolean = false)(func: TEvent => Unit): Unit = {
    val tuple = (func, handlesToo)
    // add the function to the observers of the node
    observersKey.get(node).getOrElseUpdate(eventType, MapMaker().makeSet()) += tuple
  }

  /**
   * raise an event on a node
   */
  private def raise[TEvent <: VisualEvent](node: Visual, e: TEvent) = {
    //println("raise "+e+" to node "+node)
    val observerSetOption = observersKey.getIfDefined(node).flatMap { _.get(e.eventType) }.
      toTraversable.flatten(x => x).
      // call the function if the event is not handled, or if the function was registered
      // with handlesToo=true
      filter(t => !e.handled || t._2);

    if (!observerSetOption.isEmpty) {
      e.transformCoodinates(node.transformFromThisToRoot)
      observerSetOption.foreach { t =>
        // println("invoking... "+e+ " on "+node)
        t._1.asInstanceOf[TEvent => Unit](e)
      }
    }
  }

  /**
   * dispatch an event
   */
  def dispatch(e: VisualEvent): Unit = {
    //println("dispatching "+e)
    // find a matching entry on the focus stack
    val focusTupleOption = focusStack.find(_._3(e))

    if (focusTupleOption.isDefined) {
      // only send the event to the focused node
      raise(focusTupleOption.get._2, e)
    } else {
      // normal event processing
      e.eventType.strategy match {
        case RoutingStrategy.direct => raise(e.target, e)
        case RoutingStrategy.bubbling => e.target.ancestors.foreach(raise(_, e))
        case RoutingStrategy.tunneling => e.target.ancestors.reverse.foreach(raise(_, e))
      }
    }
  }

  private var focusStack = Stack[(FocusHandle, Visual, VisualEvent => Boolean)]()

  /**
   * grab the focus for the events matching the filter
   */
  def grabFocus(focusNode: Visual, eventFilter: VisualEvent => Boolean): FocusHandle = {
    val handle = new FocusHandle()

    focusStack = focusStack.push((handle, focusNode, eventFilter))
    return handle
  }

  /**
   * release the focus which was claimed under the handle
   */
  def releaseFocus(handle: FocusHandle) {
    focusStack = focusStack.filterNot(_._1 == handle)
  }
}
