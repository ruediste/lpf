package lpf.event

import lpf.Visual
import lpf.ReferenceKey
import lpf.util.MapMaker
import scala.collection.mutable.Set
import java.awt.event.{ KeyEvent => AwtKeyEvent }
import lpf.visual.JLpfRootLike$
import lpf.visual.JLpfRootLike

class KeyEvent(val eventType: KeyEventType.KeyEventType, awtEvent: AwtKeyEvent) {

  def keyChar = awtEvent.getKeyChar()
  def keyCode = awtEvent.getKeyCode()
  def modifiers = EventModifier.getModifiers(awtEvent.getModifiersEx())
  override def toString() = awtEvent.toString()

}

object KeyEventType extends Enumeration {
  type KeyEventType = Value
  val typed = Value
  val pressed = Value
  val released = Value
}

/**
 * Responsible for dispatching key events.
 */
object KeyEventDispatcher {
  private val handlersKey = ReferenceKey(MapMaker().makeMap[KeyEventType.KeyEventType, Set[KeyEvent => Unit]]())

  def dispatch(e: KeyEvent): Unit = {
    //println("dispatch event " + e + "=>" + focusVisual.flatMap(handlersKey.getIfDefined(_)))
    KeyboardFocusManager.focusVisual.flatMap(handlersKey.getIfDefined(_)).flatMap(_.get(e.eventType)).
      foreach(_.foreach(_(e)))
  }

  /**
   * register a handler for a visual and keyboard event type
   */
  def register(v: Visual, t: KeyEventType.KeyEventType)(handler: KeyEvent => Unit): Unit = {
    handlersKey.get(v).getOrElseUpdate(t, Set()).add(handler)
  }
}