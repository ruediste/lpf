/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.event
import lpf.Visual
import lpf._
import java.awt.geom.AffineTransform

/**
 * determines the strategy of dispatching a routed event
 */
object RoutingStrategy extends Enumeration{
	/**
	 * the event should bubble up the tree from the target
	 */
	val bubbling= Value("bubbling")
	
	/**
	 * the event should be delivered directly to the target
	 */
	val direct= Value("direct")
	
	/**
	 * the event should tunnel from the root of the tree downwards to the target
	 */
	val tunneling= Value("tunneling")
}


class VisualEventType[T <: VisualEvent](val name: String, val strategy: RoutingStrategy.Value) {
  override def toString = name
}

abstract class VisualEvent {
  type This <: VisualEvent

  val eventType: VisualEventType[This]
  val target: Visual

  /**
   * Indicates if this event has already been handled. If
   * set to true, the further event handlers will not receive the event
   */
  @unobserved var handled = false

  override def toString = getClass().getSimpleName() + "(" + eventType.toString + "->" + target + ")"

  def transformCoodinates(t: =>AffineTransform){
    
  }
}
