package lpf.event

import lpf.Visual
import lpf.visual.JLpfRootLike
import lpf.Referencer
import lpf.ReferenceKey
import lpf.WeakEvent

/**
 * Defines the order in which visuals are traversed.
 *
 * For all methods, a return value of None signifies that the next
 * policy up the visual hierarchy should be used
 */
abstract class FocusTraversalPolicy {
  def getNextVisual(focusCycleRoot: Visual, v: Visual): Option[Option[Visual]]
  def getPreviousVisual(focusCycleRoot: Visual, v: Visual): Option[Option[Visual]]
  def getDefaultVisual(focusCycleRoot: Visual): Option[Option[Visual]]
}

/**
 * The standard Focus Traversal Policy. Does never delegate,
 * thus all overridden methods NEVER return None
 */
class StandardFocusTraversalPolicy extends FocusTraversalPolicy {

  private def firstFocusableVisual(focusCycleRoot: Visual) =
    focusCycleRoot.subTree.find(canGetFocus(_))

  private def lastFocusableVisual(focusCycleRoot: Visual) =
    focusCycleRoot.subTree.reverse.find(canGetFocus(_))

  override def getNextVisual(focusCycleRoot: Visual, v: Visual): Option[Option[Visual]] = {
    Some(
      // go to the visual after v
      focusCycleRoot.subTree.dropWhile(_ != v).tail
        // find the next visual which can get the focus
        .find(canGetFocus(_))
        // or use the first focusable visual if none is found
        .orElse(firstFocusableVisual(focusCycleRoot)))
  }

  override def getPreviousVisual(focusCycleRoot: Visual, v: Visual): Option[Option[Visual]] = {
    Some(
      // go to the visual before v
      focusCycleRoot.subTree.reverse.dropWhile(_ != v).tail
        // find the previous visual which can get the focus
        .find(canGetFocus(_))
        // or use the last focusable visual
        .orElse(lastFocusableVisual(focusCycleRoot)))
  }

  override def getDefaultVisual(focusCycleRoot: Visual): Option[Option[Visual]] = {
    Some(firstFocusableVisual(focusCycleRoot))
  }

  def canGetFocus(v: Visual): Boolean = StandardFocusTraversalPolicy.canGetFocus(v)
}

object StandardFocusTraversalPolicy {
  def apply() = new StandardFocusTraversalPolicy()

  private val canGetFocusKey = ReferenceKey[Boolean]()

  def setCanGetFocus(v: Visual, b: Boolean) {
    if (b)
      canGetFocusKey.set(v, true)
    else
      canGetFocusKey.remove(v)
  }

  def canGetFocus(v: Visual) = canGetFocusKey.isDefined(v)
}

object KeyboardFocusManager {
  private case class Focus(cycleRoot: Visual, visual: Visual) {}

  case class FocusChangeEventArgs(oldFocusedVisual: Option[Visual], newFocusedVisual: Option[Visual]) {
    def visualGainedFocus(v: Visual) = !oldFocusedVisual.exists(_ == v) && newFocusedVisual.exists(_ == v)
    def visualLostFocus(v: Visual) = oldFocusedVisual.exists(_ == v) && !newFocusedVisual.exists(_ == v)
  }

  val focusChanged = WeakEvent[FocusChangeEventArgs]()

  private var _focus: Option[Focus] = None
  private def focus = _focus
  private def focus_=(newFocus: Option[Focus]) {
    focusChanged(FocusChangeEventArgs(_focus.map(_.visual), newFocus.map(_.visual)))
    _focus = newFocus
  }

  var defaultFocusTraversalPolicy = new StandardFocusTraversalPolicy

  def focusVisual = focus.map(_.visual)

  def isFocused(v: Visual) = focusVisual.exists(_ == v)

  def focusNextVisual(): Unit = focus.foreach { f =>
    focus = defaultFocusTraversalPolicy.getNextVisual(f.cycleRoot, f.visual).get
      .map(Focus(f.cycleRoot, _))
  }

  def focusPreviousVisual(): Unit = focus.foreach { f =>
    focus = defaultFocusTraversalPolicy.getPreviousVisual(f.cycleRoot, f.visual).get
      .map(Focus(f.cycleRoot, _))
  }

  /**
   * put the keyboard focus to the given visual
   */
  def focus(v: Visual): Unit = {
    val cycleRoot = v.ancestors.find(isFocusCycleRoot(_)).getOrElse(v.path.head)
    focus = Some(Focus(cycleRoot, v))
    JLpfRootLike.getRoot(v).foreach(_.requestFocusInWindow())
  }

  /**
   * remove the focus from the given visual
   */
  def unFocus(): Unit = focus = None

  private val focusCycleRootKey = ReferenceKey[Boolean]()

  def isFocusCycleRoot(v: Visual) = focusCycleRootKey.isDefined(v)
  def setFocusCycleRoot(v: Visual, isFocusCycleRoot: Boolean) {
    if (isFocusCycleRoot)
      focusCycleRootKey.set(v, true)
    else
      focusCycleRootKey.remove(v)
  }

}