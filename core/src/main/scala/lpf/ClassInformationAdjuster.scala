package lpf

import lpf.instrumentation.ClassInformationAdjusterBase
import lpf.hornet.ClassInformation

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  override def adjustClassInformation(info: ClassInformation) {
    info.name match {
      case "lpf.VisibilityBuffer" => info.setApplicationClass(false)
      case _ => info.setApplicationClass(true)
    }
  }
}