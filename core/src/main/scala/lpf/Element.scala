/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import scala.collection.mutable._
import scala.collection.immutable.List
import scala.collection.Set

abstract class Element extends TreeNode[Element, ParentElementLike] with Referencer with IdClassNode {
  var templatedData: Option[Any] = None

  //println("instantiated "+this)
  Element.instantiationObserver.foreach(_(this))
}

trait ParentElementLike extends Element
  with ParentTreeNodeLike[Element, ParentElementLike] {}

trait SingleChildElementLike extends ParentElementLike
  with SingleChildNodeLike[Element, ParentElementLike] {}

trait MultiChildrenElementLike extends ParentElementLike
  with MultiChildrenNodeLike[Element, ParentElementLike] {}

abstract class ElementObject extends CompanionObjectBase with IdClassNodeObject {
  type Self <: Element
}

abstract class ElementObjectNoDefaultApply extends IdClassNodeObject{
  type Self <: Element
}

object Element {
  @unobserved private var instantiationObserver: Option[Element => Unit] = None

  def observeInstantiations[A](observer: Element => Unit, func: => A): A = {
    val oldObserver = instantiationObserver
    instantiationObserver = Some(observer)
    try {
      func
    } finally {
      instantiationObserver = oldObserver
    }
  }

  def notifyInstantiationObserver(e: Element) = instantiationObserver.foreach(_(e))
}
