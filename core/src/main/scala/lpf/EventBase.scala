/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf

import scala.collection.mutable._
import _root_.lpf.util._

class FunctionHandle {}

abstract class EventBase[A] {
	val weakFunctions = MapMaker().weakValues().makeSynchronizedMap[FunctionHandle,A => Unit]
	val strongFunctions = MapMaker().makeSynchronizedMap[FunctionHandle,A => Unit]()

	def addWeak(f: A => Unit)(implicit theReferencer: Referencer): FunctionHandle = {

		// get the event=>functions map of the referencer
		val functionMap = EventBase.functionsKey.get(theReferencer)

		// get the function bag of this event
		val functionSet = functionMap.getOrElseUpdate(this, MapMaker().makeSet[_ => Unit]())
		
		// add the function to the function list and to the referencer 
		// since removal should happen only when addition is complete, 
		// we don't have to lock
		functionSet += f
		
		val handle=new FunctionHandle
		weakFunctions.put(handle,f)
		
		return handle
	}

	def addStrong(f: A => Unit): FunctionHandle = {
		val handle=new FunctionHandle
		strongFunctions.put(handle,f)
		return handle
	}
	
	def addStrong(f: => Unit): FunctionHandle = addStrong(a => f)

	def removeWeak(handle: FunctionHandle)(implicit theReferencer: Referencer) = {
		val functionOption=weakFunctions.get(handle)
		if (functionOption.isEmpty)
			throw new Exception("Handle not defined")
		else{
			
			val functionSet = EventBase.functionsKey.getIfDefined(theReferencer).flatMap(_.get(this))
			
			// remove function from referencer
			functionSet.foreach(_ -= functionOption.get)
	
			// remove function from list
			weakFunctions-=handle
		}
	}

	def removeStrong(handle: FunctionHandle) = {
		val functionOption=strongFunctions.get(handle)
		if (functionOption.isEmpty)
			throw new Exception("Handle not defined")
		else
			strongFunctions -= handle
	}

	def apply(arg: A): Unit = {
		// invoke weak functions
		weakFunctions.values.foreach (_(arg))
		
		// invoke strong functions
		strongFunctions.values.foreach (_(arg))
	}
}

object EventBase {
	private val functionsKey = ReferenceKey( MapMaker().weakKeys().makeSynchronizedMap[EventBase[_], Set[_ => Unit]]())
}

abstract class EventBaseNoArgs(private val event: EventBase[Unit]){
	
	def addWeakF(f: () => Unit)(implicit theReferencer: Referencer)=
	{
		event.addWeak({a:Unit=>f})
	}
	
	def addWeak(f : =>Unit)(implicit theReferencer: Referencer)=event.addWeak(a=>f)
	
	def addStrongF(f: ()=>Unit)={
		event.addStrong(f())
	}
	
	def addStrongF(f: Unit=>Unit)={
		event.addStrong(f())
	}
	
	def addStrong(f: =>Unit)=event.addStrong(f)
	
	def removeWeak(handle: FunctionHandle)(implicit theReferencer: Referencer): Unit ={
		event.removeWeak(handle);
	}
	
	def removeStrong(handle: FunctionHandle)={
		event.removeStrong(handle);
	}
	
	def apply()=
	{
		event(())
	}
	
}







