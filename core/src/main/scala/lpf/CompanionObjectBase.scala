package lpf

/**
 * Base class for companion classes supporting the convenient syntax of
 *   Foo(_.x="a")
 */
abstract class CompanionObjectBase {
  type Self <: Any

  def apply(): Self
  def apply(f: Self => Unit): Self = {
    val e = apply()
    f(e)
    e
  }
}