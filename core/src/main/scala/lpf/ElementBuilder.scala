/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import scala.collection.mutable.ListBuffer
import lpf.element.Label
import scala.reflect.ClassTag
import lpf.dependency.HornetReadEventDispatcher

trait BuilderLike[Source, Root, Product]
  extends BuildingRuleContext[Source, Root, Product] {

  val ruleList: BuildingRuleList[Source, Root, Product]
  @unobserved private var ruleIterator: Option[BuildingRuleListIterator[Source, Root, Product]] = None
  @unobserved private var currentSource: Option[Source] = None
  @unobserved private var currentContext: Option[Any] = None

  def applyRules(source: Source, context: Any): Unit = {
    val oldRuleIterator = ruleIterator
    val oldSource = currentSource
    val oldContext = currentContext

    try {
      ruleIterator = Some(ruleList.ruleIterator)
      currentSource = Some(source)
      currentContext = Some(context)
      continueRuleApplication()
    } finally {
      ruleIterator = oldRuleIterator
      currentSource = oldSource
      currentContext = oldContext
    }
  }

  def continueRuleApplication() {
    while (ruleIterator.get.hasNext) {
      if (ruleIterator.get.next.apply(currentSource.get, currentContext.get, ruleIterator.get))
        return
    }
  }

  // initialize the rule list
  ruleList.init(this)
}

class ElementRuleList extends BuildingRuleList[Any, Element, Element] with BuilderDSL {
  implicit def theElementBuilder= finalContext.asInstanceOf[ElementBuilder]
}

class ElementBuilder(val ruleList: ElementRuleList, val dispatcher: Dispatcher) extends BuilderLike[Any, Element, Element] {
  //@unobserved implicit val theElementBuilder = this
  //

  /**
   * Context to collect the instantiated elements
   */
  private class Context(val source: Any, val context: Any) {
    val elements = ListBuffer[Element]()

    def rootNodes = elements.filter(_.parent.isEmpty)
    def rootOption = {
      val res = rootNodes
      if (res.size > 1)
        throw new Error("Too many root elements: " + res)
      res.headOption
    }
  }

  @unobserved private var context: Option[Context] = None

  /**
   * Option for the root element instantiated by the current building process
   */
  def rootOption = context.get.rootOption

  /**
   * root element instantiated by the current building process
   */
  def root = rootOption match {
    case Some(e: Element) => e
    case _ => throw new Error("no root element")
  }

  /**
   * apply the builder to a target
   */
  override def build(source: Any, context: Any): Element = {
    build(source, context, applyRules(source, context))
  }

  private def build(source: Any, ctx: Any, applyRulesFunc: => Unit): Element =
    // the build process is never observed by any listener
    HornetReadEventDispatcher.withListener(None) {
      // setup new context
      val oldContext = context
      context = Some(new Context(source, ctx))
      try {
        val localContext = context
        Element.observeInstantiations(e => localContext.get.elements += e, {
          applyRulesFunc

          // If none matches, throw an exception
          if (rootOption.isEmpty) throw new Exception("No root element was constructed for " + String.valueOf(source))
        })

        val element = root

        // notify parent instantiation observer
        Element.notifyInstantiationObserver(element)

        element.templatedData = Some(source)

        element
      } finally {
        // restore context
        context = oldContext
      }
    }

  override def continue(): Element = build(context.get.source, context.get.context, continueRuleApplication())
}

object ElementBuilder {
  def apply(ruleList: ElementRuleList) = new ElementBuilder(ruleList, AwtBindingDispatcher())
}
