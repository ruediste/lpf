/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import java.awt.Graphics2D
import _root_.lpf.visual.SingleChildVisualLike
import scala.collection.Iterable
//import scala.reflect.runtime.universe._
import scala.reflect.ClassTag
import java.awt.geom.AffineTransform
import java.awt.geom.Rectangle2D

abstract class Visual extends TreeNode[Visual, ParentVisualLike] with Referencer with IdClassNode {

  var templatedElement: Option[Element] = None

  def ancestorTemplatedElement: Option[Element] = {
    ancestors.flatMap(_.templatedElement).headOption
  }

  /**
   * The rectangle obtained by transforming the bounds given
   * by measureOverride() with the layout transform, thus
   * located in the coordinate system of the parent visual
   */
  private var _desiredRectangle: Rectangle2D = new Rectangle2D.Double()

  /**
   * the desired size of this visual in the coordinate system
   * of this visual
   */
  private var _measureOverrideResult: Size = Size()
  
  
  /**
   * the desired size of this visual in the coordinate system
   * of this visual
   */
  def measureOverrideResult=_measureOverrideResult
  
  /**
   * the actual size in the coordinate system of this visual
   */
  private var _actualSize: Size = Size()

  private val _placementTransform: AffineTransform = new AffineTransform()
  var layoutTransform: Option[AffineTransform] = None
  var renderTransform: Option[AffineTransform] = None

  /**
   * the desired size of this visual in the coordinate system of the
   * parent
   */
  def desiredSize = Size(_desiredRectangle)
  def desiredRectangle = _desiredRectangle

    /**
   * the actual size in the coordinate system of this visual
   */
  def actualSize = _actualSize
  def placementTransform = _placementTransform

  protected def measureOverride(availableSize: Size): Size

  final def measure(availableSize: Size): Size = {
    // do the actual measurement
    _measureOverrideResult = measureOverride(availableSize)

    // set the desired rectangle 
    _desiredRectangle.setRect(0, 0, _measureOverrideResult.width, _measureOverrideResult.height)

    // transform the desired rectangle by the layout transform
    // to put it into the coordinate system of the parent
    layoutTransform.foreach(t => _desiredRectangle =
      t.createTransformedShape(_desiredRectangle).getBounds2D())

    // return the desired size, which is calculated from the desired rectangle 
    desiredSize
  }

  protected def arrangeOverride(actualSize: Size): Unit

  /**
   * Arrange this visual. The size given is in the coordinate system
   * of the child visual. This visual is placed into the rectangle
   * defined by (0,0) and actualSize, transformed by transform.
   */
  final def arrange(actualSize: Size, transform: AffineTransform): Unit = {
    if (layoutTransform.isDefined)
      _actualSize = _measureOverrideResult
    else
      _actualSize = actualSize

    _placementTransform.setTransform(transform)

    arrangeOverride(_actualSize)
  }

  final def arrange(actualSize: Size) = {
    if (layoutTransform.isDefined)
      _actualSize = _measureOverrideResult
    else
      _actualSize = actualSize

    _placementTransform.setToIdentity();

    arrangeOverride(_actualSize)
  }

  final def arrange(placement: Rectangle) = {
    if (layoutTransform.isDefined)
      _actualSize = _measureOverrideResult
    else
      _actualSize = placement.size

    _placementTransform.setToTranslation(placement.position.x, placement.position.y);

    arrangeOverride(_actualSize)
  }

  protected def paintOverride(g: Graphics): Unit = {
    children.foreach(_.paint(g))
  }

  final def paint(g: Graphics): Unit = {
    //println("paint enter " + this + " ")
    val childGraphics = g.create(this)
    try {
      paintOverride(childGraphics)
    } finally {
      childGraphics.dispose
      //println("paint leave " + this)
    }
  }

  def transformFromThisToRoot = {
    val t = new AffineTransform()
    path.foreach(_.concatenateVisualTransformTo(t))
    t
  }
  
  def pointInRootCoordinates(p: Point): Point = {
    var awtPoint = p.toAwtPoint2D;
    transformFromThisToRoot.transform(awtPoint, awtPoint)

    Point(awtPoint)
  }

  /**
   * Concatenate the given transformation with the
   * placement, layout and render transformation of this visual.
   *
   * If the transformation of the parent of this visual is
   * given as parameter, it will be set to the transformation
   * of this visual as a result of this method
   */
  def concatenateVisualTransformTo(t: AffineTransform) {
    t.concatenate(placementTransform)
    if (layoutTransform.isDefined) {
      t.translate(-desiredRectangle.getX(), -desiredRectangle.getY())
      t.concatenate(layoutTransform.get)
    }
    renderTransform.foreach(t.concatenate(_))
  }

  // notify the observer of the instantiation
  // println("instantiated Visual " + this)
  Visual.instantiationObserver.foreach(_(this))
}

trait ParentVisualLike extends Visual with ParentTreeNodeLike[Visual, ParentVisualLike] {}

trait MultiChildrenVisualLike extends Visual
  with MultiChildrenNodeLike[Visual, ParentVisualLike]
  with ParentVisualLike {

}

abstract class VisualObject extends IdClassNodeObject {
  type Self <: Visual

  def apply(): Self
  def apply(f: Self => Unit): Self = {
    val e = apply()
    f(e)
    e
  }

}

object Visual {
  @unobserved private var instantiationObserver: Option[Visual => Unit] = None

  def observeInstantiations[A](observer: Visual => Unit, func: => A): A = {
    val oldObserver = instantiationObserver
    instantiationObserver = Some(observer)
    try {
      func
    } finally {
      instantiationObserver = oldObserver
    }
  }

  def notifyInstantiationObserver(v: Visual) = instantiationObserver.foreach(_(v))
}
