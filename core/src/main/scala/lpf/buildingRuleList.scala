/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import scala.collection.mutable.ListBuffer
import lpf.element.Label

/**
 * A building rule context provides the environment for building rules.
 */
abstract trait BuildingRuleContext[Source, Root, Product] {
  /**
   * Continue the application of building rules after the current rule.
   * The result is returned.
   */
  def continue(): Product

  /**
   * Start a new building process for the new source
   */
  def build(s: Source, context: Any): Product
  
  /**
   * get the single tree node created which does not have a parent
   */
  def root: Root
  
  /**
   * get the single tre enode created which does not have a parent,
   * if any exists.
   */
  def rootOption: Option[Root]
  
  def finalContext = this
}

trait DelegatingBuildingRuleContext[Source, Root, Product] extends BuildingRuleContext[Source, Root, Product] {
  private var delegate: BuildingRuleContext[Source, Root, Product] = null
  override def continue(): Product = delegate.continue()
  override def build(s: Source, context: Any): Product = delegate.build(s, context)
  override def root: Root = delegate.root
  override def rootOption: Option[Root] = delegate.rootOption
  override def finalContext = delegate.finalContext
  
  def init(d: BuildingRuleContext[Source, Root, Product]): Unit = delegate = d
}

/**
 * An iterator which iterates over the rules in a rule list.
 * SubLists can be iterated using diveInto()
 */
class BuildingRuleListIterator[Source, Root, Product](val baseIterator: Iterator[BuildingRule[Source, Root, Product]]) extends Iterator[BuildingRule[Source, Root, Product]] {
  @unobserved var subListIterator: Option[BuildingRuleListIterator[Source, Root, Product]] = None
  def hasNext: Boolean = subListIterator match {
    // forward to the sub iterator if available
    case Some(i) if i.hasNext => true
    // otherwise check our own iterator
    case _ => baseIterator.hasNext

  }

  def next(): lpf.BuildingRule[Source, Root, Product] = subListIterator match {
    // if the sub iterator still has an element, use it
    case Some(i) if i.hasNext => i.next
    case _ =>
      // if there is a sub iterator, it has to be empty. discard it
      if (subListIterator.isDefined) subListIterator = None
      // use an element from the base iterator
      baseIterator.next
  }

  /**
   * continue iteration with the rules in the subList
   */
  def diveInto(subList: BuildingRuleList[Source, Root, Product]): Unit = subListIterator match {
    // forward the dive when a sub iterator is active
    case Some(i) => i.diveInto(subList)
    // otherwis add a sub iterator
    case _ => subListIterator = Some(subList.ruleIterator)
  }
}

class BuildingRuleList[Source, Root, Product] extends DelegatingBuildingRuleContext[Source, Root, Product] {
  private var rules = ListBuffer[BuildingRule[Source, Root, Product]]()

  def addRule(r: BuildingRule[Source, Root, Product]): Unit = rules += r
  def rule(func: PartialFunction[Source, Unit]): Unit = rules += new FunctionBuildingRule[Source, Root, Product](func)
  def rule2(func: PartialFunction[(Source, Any), Unit]): Unit = rules += new FunctionBuildingRule2[Source, Root, Product](func)
  def include(predicate: PartialFunction[(Source, Any), Unit], subList: BuildingRuleList[Source, Root, Product]) {
    rules += new IncludeBuildingRule[Source, Root, Product](predicate, subList)
    subList.init(this)
  }

  def include(subList: BuildingRuleList[Source, Root, Product]) { include({ case _ => }, subList) }

  def ruleIterator: BuildingRuleListIterator[Source, Root, Product] = new BuildingRuleListIterator(rules.iterator)
}

abstract class BuildingRule[Source, Root, Product] {
  /**
   * Checks if the rule matches the source. If true, either executes the function or
   * dives into the sub list.
   *
   * returns true if a function has been executed, false otherwise
   *
   */
  def apply(source: Source, context: Any, i: BuildingRuleListIterator[Source, Root, Product]): Boolean
}

class FunctionBuildingRule2[Source, Root, Product](val func: PartialFunction[(Source, Any), Unit])
  extends BuildingRule[Source, Root, Product] {
  override def apply(source: Source, context: Any, i: BuildingRuleListIterator[Source, Root, Product]): Boolean = {
    if (func.isDefinedAt(source, context)) {
      // execute the function
      func(source, context)

      // we have a match
      true
    } else false
  }
}

class FunctionBuildingRule[Source, Root, Product](val func: PartialFunction[Source, Unit])
  extends BuildingRule[Source, Root, Product] {
  override def apply(source: Source, context: Any, i: BuildingRuleListIterator[Source, Root, Product]): Boolean = {
    if (func.isDefinedAt(source)) {
      // execute the function
      func(source)

      // we have a match
      true
    } else false
  }
}

class IncludeBuildingRule[Source, Root, Product](val predicate: PartialFunction[(Source, Any), Unit], val subList: BuildingRuleList[Source, Root, Product])
  extends BuildingRule[Source, Root, Product] {
  override def apply(source: Source, context: Any, i: BuildingRuleListIterator[Source, Root, Product]): Boolean = {
    if (predicate.isDefinedAt(source, context)) {
      // dive into the sub list
      i.diveInto(subList)
    }
    false
  }
}
