/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.binding
import lpf.dependency.HornetEventListener
import lpf.Referencer
import lpf.dependency.ChangeNotifier
import lpf.Dispatcher

/**
 * Bind the target to the source, using the specified mapping function
 */
class OneWayListBinding[A, B](dispatcher: Dispatcher, private val source: ListBindingEndPoint[A], private val func: A => B, private val target: ListBindingEndPoint[B], parentReferencer: Referencer)
  extends BindingBase(parentReferencer) with Referencer {

  val notifier = ChangeNotifier()

  override def updateImpl(): Unit = {
    // evaluate the source. As soon as one of the dependencies changes, call invalidate
    val src = notifier.notifyOfChanges(invokeUpdate(dispatcher), source.read(), singleNotificationCall=true)
    target.update(src.map(func))
  }

  // perform an initial invalidation
  updateInitial()

}

class Mapping[A, B] {
  private var mapping = Map.empty[(A, Int), B]
  private var inverseMapping = Map.empty[B, A]

  abstract class Definer {
    def getOrElseDefine(a: A, b: => B): B
  }

  def reDefine[R](f: Definer => R): R = {
    // build a new mapping with the reused and newly created objects
    var newMapping = Map.empty[(A, Int), B]
    var newInverseMapping = Map.empty[B, A]

    val result = f(new Definer {
      // the index map contains the next index for each element
      // used for duplicate element handling
      var indexMap = Map.empty[A, Int]

      def getOrElseDefine(a: A, generator: => B): B = {
        // get the index
        val index = indexMap.getOrElse(a, 0)

        // set the index for the next element which is equal to the current
        indexMap = indexMap + ((a, index + 1))

        // get the element from the cache
        val b = mapping.getOrElse((a, index), generator)

        // update the cache
        newMapping = newMapping + (((a, index), b))
        newInverseMapping = newInverseMapping + ((b, a))

        // return the element
        b
      }
    })

    // update the mapping
    mapping = newMapping
    inverseMapping = newInverseMapping

    // return the result of the definig function
    result
  }

  def get(a: A, index: Int): Option[B] = mapping.get((a, index))
  def inverseGet(b: B): Option[A] = inverseMapping.get(b)
  override def toString() = "[Mapping " + mapping + "]"
}

/**
 * Bind the target to the source, using the specified mapping function
 * - when the source changes, the target is updated
 * - the values in of the target are cached. The key is the element. If duplicate
 *   elements are present, every element is mapped to a different element in the
 *   target
 */
class OneWayCachingListBinding[A, B](dispatcher: Dispatcher, private val source: ListBindingEndPoint[A], private val mappingFunction: A => B, private val target: ListBindingEndPoint[B], parentReferencer: Referencer)
  extends BindingBase(parentReferencer) with Referencer {

  val mappingField = new Mapping[A, B]

  val notifier = ChangeNotifier()

  def mapping = mappingField

  override def updateImpl(): Unit = {
    // evaluate the source. As soon as one of the dependencies changes, call invalidate
    val src = notifier.notifyOfChanges(invokeUpdate(dispatcher), source.read(), singleNotificationCall=true)

    // set the target
    target.update(
      mapping.reDefine { definer =>
        {
          // Map the source using the mapping function of the binding.
          // If an element of the source was mapped before, the mapped
          // target element is reused
          src.map(s => definer.getOrElseDefine(s, mappingFunction(s))).toList
        }
      })

  }

  // perform an initial invalidation
  updateInitial()

}

object OneWayListBinding {
  def bind[A, B](dispatcher: Dispatcher)(getter: => TraversableOnce[A])(map: A => B)(setter: Iterable[B] => Unit)(implicit theReferencer: Referencer) =
    new OneWayListBinding[A, B](dispatcher, ListBindingEndPoint.readOnly(() => getter), map, ListBindingEndPoint.writeOnly(x => setter(x.toList)), theReferencer)

  def cache[A, B](dispatcher: Dispatcher)(getter: => TraversableOnce[A])(map: A => B)(setter: Iterable[B] => Unit)(implicit theReferencer: Referencer) =
    new OneWayCachingListBinding[A, B](dispatcher, ListBindingEndPoint.readOnly(() => getter), map, ListBindingEndPoint.writeOnly(x => setter(x.toList)), theReferencer)

}
