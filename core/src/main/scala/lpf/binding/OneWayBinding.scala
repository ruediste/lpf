/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.binding
import lpf.dependency._
import lpf.Referencer
import lpf.Dispatcher
import lpf.Dispatcher

class OneWayBinding(
  dispatcher: Dispatcher,
  private val func: () => Unit,
  parentReferencer: Referencer,
  private val showDependencies: Boolean)
  extends BindingBase(parentReferencer)
  with Referencer {

  private val changeNotifier = ChangeNotifier()

  override def updateImpl(): Unit = {
    // Update the target from the source. As soon as one of the dependencies changes, call invalidate
    changeNotifier.notifyOfChanges(invokeUpdate(dispatcher), func(), singleNotificationCall = true)
    if (showDependencies)
      changeNotifier.showDependencies
  }

  // perform initial invalidation
  updateInitial()
}

object OneWayBinding {

  /**
   * Bind the target to the source, using the specified mapping function
   * - when the source changes, the target is updated
   * - when a dependency of the target changes, the binding is reapplied
   */
  //def withQueue[A](dispatcher: Dispatcher, showDependencies: Boolean = false)(func: => Unit)(implicit theReferencer: Referencer) =
  //	  new OneWayBinding(dispatcher, () => func, theReferencer, showDependencies)

  def apply(dispatcher: Dispatcher, showDependencies: Boolean = false)(func: => Unit)(implicit theReferencer: Referencer) =
    new OneWayBinding(dispatcher, () => func, theReferencer, showDependencies)

}
