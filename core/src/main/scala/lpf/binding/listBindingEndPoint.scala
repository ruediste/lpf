package lpf.binding

import scala.collection.generic.Growable
import scala.collection.mutable.ListBuffer
import lpf.dependency.ChangeNotifier

/**
 * Represents one collection involved in a list binding
 */
abstract class ListBindingEndPoint[A] {
  /**
   * Update the list from the list provided
   */
  def update(l: TraversableOnce[A]): Unit

  /**
   * Return a copy of the list. For immutable collections, this can be
   * the collection itself. For mutable collection it is required to iterate
   * over the collection, to make notifiers observe the right hornets.
   */
  def read(): TraversableOnce[A]
}

/**
 * List Binding EndPoint for immutable lists.
 *
 * The getter and setter are optional. This allows to express writeOnly and readOnly endPoints
 */
class ListBindingEndPointImmutable[A](get: Option[() => TraversableOnce[A]], set: Option[TraversableOnce[A] => Unit]) extends ListBindingEndPoint[A] {
  override def update(l: TraversableOnce[A]): Unit = {
    assert(set.isDefined, "Cannot write to a ReadOnly ListBinding endpoint")
    set.get.apply(l)
  }

  override def read(): TraversableOnce[A] = {
    assert(get.isDefined, "Cannot read from a WriteOnly ListBinding endpoint")
    get.get.apply()
  }
}

/**
 * List Binding EndPoint for Growables
 */
class ListBindingEndPointGrowable[A](get: () => Growable[A] with TraversableOnce[A]) extends ListBindingEndPoint[A] {
  override def update(l: TraversableOnce[A]): Unit = {
    val list = get()
    list.clear
    list ++= l
  }

  override def read(): TraversableOnce[A] = {
    val buf = ListBuffer[A]()
    buf ++= get()
    buf
  }
}

object ListBindingEndPoint {
  def apply[A](f: => Growable[A] with TraversableOnce[A]) = new ListBindingEndPointGrowable(() => f)
  def apply[A](get: () => TraversableOnce[A], set: TraversableOnce[A] => Unit) =
    new ListBindingEndPointImmutable(Some(get), Some(set))
  def readOnly[A](get: () => TraversableOnce[A]) =
    new ListBindingEndPointImmutable(Some(get), None)
  def writeOnly[A](set: TraversableOnce[A] => Unit) =
    new ListBindingEndPointImmutable[A](None, Some(set))
}

