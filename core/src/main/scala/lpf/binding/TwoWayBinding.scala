package lpf.binding

import lpf.Referencer
import lpf.dependency.ChangeNotifier
import lpf.Dispatcher

/**
 * Two Way binding between two properties.
 *
 * If A is invalidated while B is invalid, or vice versa, getA is used to obtain
 * a new value on the next update.
 *
 * During the constructor, getAtoB() and setAtoB() are executed for an initial update, as
 * well as getBtoA() to observe future changes.
 *
 * There are two binding queues, one to execute the AtoB getter and another to execute the BtoA getter.
 * The binding is put into the queue associated with the getter which accessed the hornet which
 * is written first.
 *
 */
class TwoWayBinding[AtoB, BtoA](dispatcherA: Dispatcher, dispatcherB: Dispatcher, showDependencies: Boolean,
  parentReferencer: Referencer,
  getAtoB: () => AtoB,
  setAtoB: AtoB => Unit,
  getBtoA: () => BtoA,
  setBtoA: BtoA => Unit)
  extends BindingBase(parentReferencer) {

  /**
   * None: not in queue
   * Some(true): put into queue A
   * Some(false): put ino queu B
   */
  @volatile private var putToQueueA: Option[Boolean] = None

  def invalidateA(): Unit = this.synchronized {
    if (putToQueueA.isEmpty) {
      putToQueueA = Some(true)
      invokeUpdate(dispatcherA)
    }

  }

  def invalidateB(): Unit = this.synchronized {
    if (putToQueueA.isEmpty) {
      putToQueueA = Some(false)
      invokeUpdate(dispatcherB)
    }
  }

  private val aNotifier = ChangeNotifier()
  private val bNotifier = ChangeNotifier()

  override def updateImpl(): Unit = {
    // since the updates are all asynchronous, only one thread will ever
    // call the update function at a time (no synchronization needed)

    // Invalidations at this point will simply add the binding to the queue
    // again. The latest invalidation will determine the direction of this update and
    // the direction of the future update.

    // This is the point the direction of the update is fixed. No synchronization needed,
    // a race condition keeps beeing a race condition, no matter what we do.
    val updateAtoB = putToQueueA.exists(_ == true)

    // now we allow putting the binding into a queue again
    putToQueueA = None

    if (updateAtoB) {

      // Invalidations at this point will add the binding to the queue and set the invalidation
      // direction of the future update.

      // get the the new value for B. While reading, writes from the other threads are already
      // detected and cause an invalidation, which will be handled in the future
      val value = aNotifier.notifyOfChanges(invalidateA, getAtoB(), singleNotificationCall = true)

      // while updating B, the bNotifier should ignore changes from the current thread,
      // since they are caused by the setter called here
      bNotifier.ignoreChangesFromCurrentThread {
        // perform the update
        setAtoB(value)
      }

      /* Now we are in the unfortunate situation that the setter might have changed the 
       the data at B in such a way, that the bNotifier does not observe the right hornets
       anymore. 
      
       There might have been a change from outside (event completely before the update
       function) as well, which had the same effect.
       
       In both cases, we could pretend that the invalidation of A somehow happened after such
       a change. But we have to make sure the bNotifier observes the right hornets again,
       so we just execute the getter.
       */

      // at last B's getter has to be evaluated, to set up the notifier correctly again
      bNotifier.notifyOfChanges(invalidateB(), getBtoA(), singleNotificationCall = true)
    } else {
      val value = bNotifier.notifyOfChanges(invalidateB, getBtoA(), singleNotificationCall = true)
      aNotifier.ignoreChangesFromCurrentThread {
        setBtoA(value)
      }

      aNotifier.notifyOfChanges(invalidateA, getAtoB(), singleNotificationCall = true)
    }

    if (showDependencies) {
      println("dependencies of A")
      aNotifier.showDependencies
      println("dependencies of B")
      bNotifier.showDependencies
    }
  }

  // perform initial update
  putToQueueA = Some(true)
  updateInitial()

}

object TwoWayBinding {
  def apply[AtoB, BtoA](dispatcher: Dispatcher, showDependencies: Boolean = false)(
    getAtoB: => AtoB,
    setAtoB: AtoB => Unit,
    getBtoA: => BtoA,
    setBtoA: BtoA => Unit)(implicit theReferencer: Referencer) = {
    new TwoWayBinding(dispatcher, dispatcher, showDependencies, theReferencer, () => getAtoB, setAtoB, () => getBtoA, setBtoA)
  }
}