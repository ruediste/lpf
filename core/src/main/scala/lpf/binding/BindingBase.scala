package lpf.binding

import compat.Platform.EOL
import lpf._
import lpf.util.MapMaker
import scala.collection.mutable.LinkedHashSet

abstract class BindingBase(val parentReferencer: Referencer) {
  private var disposed = false

  private val instantiationThrowable = new Throwable()

  final def updateInitial(){
    update(List())
  }
  
  final def invokeUpdate(d: Dispatcher) {
    // retrieve the binding chain which caused this update
    val chain = BindingBase.bindingChain.get()

    // Check if this binding is not yet part of the chain
    // Otherwise, a recursive invalidation occurred, which
    // is an error (possibly leading to an infinite loop)
    if (chain.contains(this)) {

      
      // determine bindings in loop
      val causes = chain.toList.dropWhile(_ != this)
        // create errors
        .map("Binding instantiated at:\n" +
            _.instantiationThrowable.getStackTrace()
            .take(BindingBase.instantiationStackTraceDepth)
            .mkString("", EOL, EOL))
        .mkString(EOL)

      throw new Error("Binding Loop detected \n" + causes)
    }

    // queue the update invocation
    d(update(chain))
  }

  final def update(bindingChain: List[BindingBase]): Unit = {
    // only update if the binding is not yet disposed
    if (!disposed) {

      // set the binding chain, including this
      BindingBase.bindingChain.set(this +: bindingChain)

      try {
        // process the update
        updateImpl()
      } finally {
        // clear the binding chain
        BindingBase.bindingChain.set(List())
      }
    }
  }

  protected def updateImpl(): Unit

  def dispose() {
    if (!disposed) {
      BindingBase.referenceKey.getIfDefined(parentReferencer).foreach(_.remove(BindingBase.this))
      disposed = true
    }
  }

  // register the binding with the referencer
  BindingBase.referenceKey.get(parentReferencer).add(BindingBase.this)
}

object BindingBase {
  /**
   * The number of stack traces to show for the 
   * instantiations when binding loops are detected
   */
  var instantiationStackTraceDepth=10
  private val referenceKey = ReferenceKey(MapMaker().makeSet[BindingBase]())

  private val bindingChain = new ThreadLocal[List[BindingBase]] {
    override def initialValue() = List()
  }
}