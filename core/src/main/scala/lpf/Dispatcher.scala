package lpf

import scala.collection.mutable.SynchronizedPriorityQueue
import scala.collection.mutable.Queue
import java.util.concurrent.PriorityBlockingQueue
import java.util.Comparator
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.TimeUnit
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.awt.EventQueue

abstract class Dispatcher {
  def invoke(f: => Unit): Unit
  def invokeAndWait[A](f: => A): A
  def apply(f: => Unit) = invoke(f)

  /**
   * If called on the thread of the dispatcher, nothing happens. If called from
   * a different thread, and error is thrown
   */
  def verifyAccess() = assert(canAccess, "Dispatcher accessed from wrong thread")

  /**
   * return true if on the thread of the dispatcher, false otherwise
   */
  def canAccess: Boolean

}

object AwtEDTDispatcher extends Dispatcher {

  def invoke(f: => Unit): Unit = {
    EventQueue.invokeLater(new Runnable { def run = f })
  }

  def invokeAndWait[A](f: => A): A = {
    var result: A = null.asInstanceOf[A]
    EventQueue.invokeAndWait(new Runnable { def run = result = f })
    result
  }

  def canAccess = EventQueue.isDispatchThread()
}

class AwtBindingDispatcher(thread: Thread) extends StandaloneDispatcher(thread) {
  override def canAccess = EventQueue.isDispatchThread()
}

object AwtBindingDispatcher {
  private val lock = new Object()
  private var dispatcher: Option[StandaloneDispatcher] = None

  def apply(): StandaloneDispatcher = {
    lock.synchronized {
      if (dispatcher.isEmpty) {
        if (EventQueue.isDispatchThread()) {
          dispatcher = Some(new AwtBindingDispatcher(Thread.currentThread()))
        } else {
          // initialize the dispatcher instance in the EDT
          EventQueue.invokeAndWait(new Runnable() {
            def run() {
              dispatcher = Some(new AwtBindingDispatcher(Thread.currentThread()))
            }
          })
        }
      }
      return dispatcher.get
    }
  }
}

class StandaloneDispatcher(val thread: Thread) extends Dispatcher {

  @unobserved private var stopping = 0

  /**
   * Raised for each queued task in the thread
   * which added the task. Watch out for threading
   * problems.
   */
  val taskQueued = WeakEventNoArgs()

  private class QueueContainer {
    private val queue = new LinkedBlockingQueue[() => Unit]()
    def addTaskToQueue(f: () => Unit) {
      queue.add(f)
      taskQueued()
    }

    def poll() = queue.poll()
    def take() = queue.take()
    def poll(timeout: Long, unit: TimeUnit) = queue.poll(timeout, unit)

    def tasksPending = !queue.isEmpty()
  }

  def tasksPending = queueContainer.tasksPending

  private val queueContainer = new QueueContainer()

  /**
   * Queue a function to be executed
   */
  def invoke(f: => Unit): Unit = {
    queueContainer.addTaskToQueue(() => f)
  }

  /**
   * Queue a function to be executed. Block the current
   * thread until the function is executed and return the result
   */
  def invokeAndWait[A](f: => A): A = {
    if (canAccess)
      throw new Error("Calling invokeAndWait() on the dispatcher thread just blocks the thread. Makes no sense.")

    val lock = new Object()
    var result: A = null.asInstanceOf[A]

    // create a dispatcher task with the same priority as the supplied one
    // which notifies the lock at the end
    val realFunction = () => {
      result = f
      lock.synchronized {
        lock.notify()
      }
    }

    // enqueue the task and wait for the lock to be notified
    lock.synchronized {
      queueContainer.addTaskToQueue(realFunction)
      lock.wait()
    }
    result
  }

  /**
   * Execute the next queued function
   */
  def tryExecuteSingle(): Boolean = {
    verifyAccess
    val f = queueContainer.poll()
    if (f == null)
      false
    else {
      f()
      true
    }
  }

  def tryExecuteSingle(timeout: Long, unit: TimeUnit): Boolean = {
    verifyAccess
    val f = queueContainer.poll(timeout, unit)
    if (f == null)
      false
    else {
      f()
      true
    }
  }

  /**
   * Execute functions until the queue becomes empty
   */
  def executeAll() {
    verifyAccess
    while (tryExecuteSingle) {}
  }

  /**
   * Execute functions, until stop() is called
   */
  def run() {
    verifyAccess
    while (stopping <= 0) {
      queueContainer.take().apply()
    }
    stopping -= 1
  }

  /**
   * return true if on the thread of the dispatcher, false otherwise
   */
  def canAccess = thread == Thread.currentThread()

  /**
   * When this thread is in the run() loop,
   * break the loop when the currently executing function
   * is finished
   */
  def stop() {
    verifyAccess
    stopping += 1
  }
}

object StandaloneDispatcher {
  def apply() = new StandaloneDispatcher(Thread.currentThread())
  def apply(t: Thread) = new StandaloneDispatcher(t)
}