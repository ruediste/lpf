/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf
import scala.collection.mutable.MutableList
import scala.collection.mutable.WeakHashMap
import scala.collection.mutable.Set
import _root_.lpf.util._

class WeakEvent[A] extends EventBase[A] {
	def +=(f: (A) => Unit)(implicit theReferencer: Referencer) = addWeak(f)
	def +=(f: =>Unit)(implicit theReferencer: Referencer) =addWeak{(arg: A)=>f}
	
	def -=(handle: FunctionHandle)(implicit theReferencer: Referencer) = removeWeak(handle)
}


object WeakEvent {
	def apply[A]() = new WeakEvent[A]()
}

class WeakEventNoArgs extends EventBaseNoArgs(WeakEvent()) {
	def +=(f: =>Unit)(implicit theReferencer: Referencer) =addWeak(f)
	def -=(handle: FunctionHandle)(implicit theReferencer: Referencer)=removeWeak(handle)
}

object WeakEventNoArgs {
	def apply() = new WeakEventNoArgs()
}
