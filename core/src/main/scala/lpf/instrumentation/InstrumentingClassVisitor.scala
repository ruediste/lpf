package lpf.instrumentation

import org.objectweb.asm._
import JoinPointKind._
import org.objectweb.asm.commons._
import scala.collection.mutable.ListBuffer
import org.objectweb.asm.util.Textifier
import org.objectweb.asm.util.TraceClassVisitor
import java.io.PrintWriter

/**
 * A class visitor performing instrumentation
 */
class InstrumentingClassVisitor(loader: ClassLoader, adviceProvider: AdviceProviderBase, cv: ClassVisitor) extends ClassVisitor(Opcodes.ASM4, cv) {
  var className = ""
  val instrumentedAnnotationName="Llpf/rt/$lpf$Instrumented;"
  override def visit(version: Int,
    access: Int,
    name: String,
    signature: String,
    superName: String,
    interfaces: Array[String]) {
    this.className = name
    super.visit(version, access, name, signature, superName, interfaces)
    super.visitAnnotation(instrumentedAnnotationName, false).visitEnd()
  }

  override def visitAnnotation(desc: String, visible: Boolean) = {
    assert(!desc.equals(instrumentedAnnotationName), "Duplicate instrumentation !!!")
    super.visitAnnotation(desc, visible)
  }
  
  override def visitMethod(methodAccess: Int, methodName: String, methodDesc: String, methodSignature: String, methodExceptions: Array[String]): MethodVisitor = {
    val isStatic = (methodAccess & Opcodes.ACC_STATIC) != 0

    val location = ClassMember(Type.getObjectType(className).getClassName(), methodName, methodDesc, isStatic)
    location.loader = loader

    // get the method visitor to forward to
    var mv = super.visitMethod(methodAccess, methodName, methodDesc, methodSignature, methodExceptions)

    val data = new AnalysisData()

    // setup the visitor chain
    mv = new InstrumentingMethodVisitor(data, adviceProvider, location, mv, methodAccess, methodName, methodDesc)
    mv = new AnalyzingMethodAdapter(data, adviceProvider, location, mv, methodAccess, methodName, methodDesc)

    return mv;
  }
}


