package lpf.instrumentation

/**
 * Represents a member of a class
 * 
 * @param className name of the class containing the member
 * @param memberName name of the member
 * @param isStatic true if the class member is static
 */
case class ClassMember(className: String, memberName: String,  desc: String, isStatic: Boolean) {
  var loader: ClassLoader = null
  
  //override def toString()=className //.split('.').last+"::"+memberName+
  //","+desc+
  //","+isStatic
}