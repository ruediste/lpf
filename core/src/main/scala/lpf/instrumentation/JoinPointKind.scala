package lpf.instrumentation

/**
 * Kind of a join point
 */
object JoinPointKind extends Enumeration {
  type JoinPointKind = Value

  /**
   * located at the entry of a method.
   *
   * member: the method itself
   *
   * arguments: none
   */
  val MethodEnter = Value

  /**
   * located at the exit of a method.
   *
   * member: the method itself
   *
   * arguments: none
   */
  val MethodLeave = Value

  /**
   * located at the exit of a method, both for normal return
   * and when the method is left with an exception
   *
   * member: the method itself
   *
   * arguments: none
   */
  val MethodLeaveFinally = Value

  /**
   * located before a field is read.
   *
   * member: the field beeing read
   *
   * arguments for normal fields: object containing the field (java.lang.Object)
   * arguments for static fields: none
   */
  val BeforeGetField = Value

  /**
   * located after a field is read.
   *
   * member: the field beeing read
   *
   * arguments for normal fields: object containing the field (java.lang.Object)
   * arguments for static fields: none
   */
  val AfterGetField = Value

  /**
   * located before a field is set.
   *
   * member: the field beeing set
   *
   * arguments for normal fields: object containing the field (java.lang.Object), value beeing written (type of the field)
   * arguments for static fields: value beeing written (type of the field)
   */
  val BeforeSetField = Value

  /**
   * located after a field is set.
   *
   * member: the field beeing set
   *
   * arguments for normal fields: object containing the field (java.lang.Object), value beeing written (type of the field)
   * arguments for static fields: value beeing written (type of the field)
  */
  val AfterSetField = Value

  /**
   * located before a method is called
   *
   * member: the method beeing called
   *
   * arguments for normal methods: object the method is called on (java.lang.Object), original arguments of the method (type of the argument)
   * arguments for static methods: original arguments of the method (type of the argument)
   */
  val BeforeCall = Value

  /**
   * located after a method is called
   *
   * member: the method beeing called
   *
   * arguments for normal methods: object the method is called on (java.lang.Object), original arguments of the method (type of the argument)
   * arguments for static methods: original arguments of the method (type of the argument)
   */
  val AfterCall = Value

  /**
   * Located after a method is called, both for normal return of the method and when the method throws an exception.
   * Always after the AfterCall join point.
   *
   * member: the method beeing called
   *
   * arguments for normal methods: object the method is called on (java.lang.Object), original arguments of the method (type of the argument)
   * arguments for static methods: original arguments of the method (type of the argument)
   */
  val AfterCallFinally = Value

  /**
   * located before an array is read
   *
   * member: same as location
   *
   * arguments: array (java.lang.Object), array index (int)
   */
  val BeforeArrayRead = Value
  
  /**
   * located after an array is read
   *
   * member: same as location
   *
   * arguments: array (java.lang.Object), array index (int)
   */
  val AfterArrayRead = Value

   /**
   * located before an array is written
   *
   * member: same as location
   *
   * arguments: array (java.lang.Object), array index (int), value (type of array element, java.lang.Object for reference arrays)
   */
  val BeforeArrayWrite = Value
  
  /**
   * located after an array is written
   *
   * member: same as location
   *
   * arguments: array (java.lang.Object), array index (int), value (type of array element, java.lang.Object for reference arrays)
   */
  val AfterArrayWrite = Value
  
  
  /**
   * located before an array length is read
   *
   * member: same as location
   *
   * arguments: array (java.lang.Object)
   */
  val BeforeArrayLength = Value
  
  /**
   * located after an array length is read
   *
   * member: same as location
   *
   * arguments: array (java.lang.Object)
   */
  val AfterArrayLength = Value
  
  
  val New = Value
}