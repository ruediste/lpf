package lpf.instrumentation

class PackageAdviceProviderBase extends AdviceProviderBase {
  override def needsInstrumentation(loader: ClassLoader, className: String,  bytes: Array[Byte]): Option[Boolean] = None
  override def getAdvice(jp: JoinPoint): Option[Advice] = None
}