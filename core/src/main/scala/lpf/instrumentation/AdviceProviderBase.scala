package lpf.instrumentation

import lpf.util.TimeMonitor

/**
 * Provides [[lpf.instrumentation.Advice]] for a [[lpf.instrumentation.JoinPoint]]
 *
 * The [[lpf.instrumentation.Instrumentor]] searches classes for join points, and asks it's AdviceProvider for
 * advice at each join point.
 *
 * To be subclassed
 */
class AdviceProviderBase {
  /**
   * determine if a class, loaded by a class loader, needs to be
   * instrumented
   */
  def needsInstrumentation(loader: ClassLoader, className: String, bytes: Array[Byte]): Option[Boolean] = {
    findPackageAdviceProvider(loader, className).flatMap(_.needsInstrumentation(loader, className, bytes))
  }

  /**
   * Get the advice for a certain join point. If None is returned, no action should be taken at the join point
   */
  def getAdvice(jp: JoinPoint): Option[Advice] =
    findPackageAdviceProvider(jp.location).flatMap(_.getAdvice(jp))

  /**
   * locate an adviceProvider based on a the package of a location
   */
  final def findPackageAdviceProvider(location: ClassMember): Option[AdviceProviderBase] =
    findPackageAdviceProvider(location.loader, location.className)

  final def findPackageAdviceProvider(loader: ClassLoader, className: String): Option[AdviceProviderBase] = {
    InstrumentationUtil.searchClass(
      loader,
      InstrumentationUtil.packageName(className),
      "AdviceProvider",
      classOf[AdviceProviderBase])
      .headOption
      .map(_.newInstance().asInstanceOf[AdviceProviderBase])
  }
}