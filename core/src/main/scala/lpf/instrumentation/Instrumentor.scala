package lpf.instrumentation

import org.objectweb.asm._
import JoinPointKind._
import org.objectweb.asm.commons._
import scala.collection.mutable.ListBuffer
import org.objectweb.asm.util.Textifier
import org.objectweb.asm.util.TraceClassVisitor
import java.io.PrintWriter
import lpf.hornet.ClassInformationPool
import lpf.rt.ObjectHornetClassVisitor
import org.objectweb.asm.util.CheckClassAdapter

/**
 * Instruments a class based on an [[lpf.instrumentation.AdviceProviderBase]]
 */
class Instrumentor(val adviceProvider: AdviceProviderBase) {

  /**
   * Return true if a class needs to be instrumented by the instrumentor
   */
  def needsInstrumentation(loader: ClassLoader, className: String, bytes: Array[Byte]): Boolean = {
    adviceProvider.needsInstrumentation(loader, className, bytes).getOrElse(false)
  }

  /**
   * instrument a class
   *
   * @param input ClassReader the class is read from
   * @param output ClassWriter the instrumented class is written to
   */
  def instrument(className: String, loader: ClassLoader, input: ClassReader, output: ClassWriter) {
    try {
      val info = ClassInformationPool.getClassInformation(className)
      var cv: ClassVisitor = output
      // cv = new TraceClassVisitor(cv, new PrintWriter(System.out))
      if (info.needsObjectHornetInstrumentation)
        cv = new ObjectHornetClassVisitor(info, cv)
      cv = new InstrumentingClassVisitor(loader, adviceProvider, cv)

      input.accept(cv, ClassReader.EXPAND_FRAMES)
    } catch {
      case t: Throwable => throw new Error("Error while instrumenting " + className, t);
    }
  }
}