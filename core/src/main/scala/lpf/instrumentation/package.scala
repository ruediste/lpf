package lpf

/**
 * Provides a simple Aspect Oriented Programming library.
 * The core class is the [[lpf.instrumentation.Instrumentor Instrumentor]]. It can load a class
 * and search it for [[lpf.instrumentation.JoinPoint JoinPoint]]s. For each join point, it asks its
 * [[lpf.instrumentation.AdviceProviderBase AdviceProvider]] for [[lpf.instrumentation.Advice Advice]].
 *
 * If an advice is found, the corresponding bytecode changes are performed. Usage example:
 *
 * {{{
 * val adviceProvider = new AdviceProvider {
 *   override def getAdvice(jp: JoinPoint): Option[Advice] = {
 *     jp match {
 *       case JoinPoint(_, ClassMember(_, "i", _), BeforeGetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod",
 *         OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
 *       case JoinPoint(_, ClassMember(_, "i", _), AfterGetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod",
 *         OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
 *     }
 *   }
 * }
 * val instrumentor = new Instrumentor(adviceProvider)
 * val cr = new ClassReader(classOf[<<YourClass>>].getName())
 * val cw = new ClassWriter(cr, 0)
 * instrumentor.instrument(cr, cw)
 * val bytes = cw.toByteArray()
 * }}}
 *
 */
package object instrumentation {

}