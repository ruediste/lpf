package lpf.instrumentation

import scala.collection.mutable.ListBuffer
import org.objectweb.asm.commons._
import org.objectweb.asm._
import JoinPointKind._
import scala.Some.apply
import scala.collection.immutable.List.apply

/**
 * Contains various utility functions for the instrumentation
 */
object InstrumentationUtil extends RtInstrumentationUtilLike {

  def getTypeOfArrayStoreInstruction(opcode: Int): Type = opcode match {
    case Opcodes.IASTORE => Type.INT_TYPE
    case Opcodes.LASTORE => Type.LONG_TYPE
    case Opcodes.FASTORE => Type.FLOAT_TYPE
    case Opcodes.DASTORE => Type.DOUBLE_TYPE
    case Opcodes.AASTORE => Type.getObjectType("java/lang/Object")
    case Opcodes.BASTORE => Type.BOOLEAN_TYPE
    case Opcodes.CASTORE => Type.CHAR_TYPE
    case Opcodes.SASTORE => Type.SHORT_TYPE
  }

  def simpleClassName(className: String) = className.split('.').last
  /**
   * Convert a class name in normal java form (java.lang.Object) to the internal form (java/lang/Object)
   */
  def toInternalName(className: String): String = className.replace(".", "/")

  /**
   * create the bytecode for an advice
   *
   * @param advice Advice to create bytecode for
   * @param mv MethodVisitor to generate bytecode on
   * @param argVars list containing the local variables the original method arguments are stored in
   * @param joinPoint join point beeing instrumented
   */
  def handleAdvice(advice: Option[Advice], mv: GeneratorAdapter, argVars: Seq[Int], joinPoint: JoinPoint) {
    if (advice.isDefined) {
      advice.get match {
        case a: InvokeStaticAdvice =>
          val adviceArgTypes = InstrumentationUtil.pushAdviceArgs(mv, a.arguments, argVars, joinPoint)
          // call the method
          val method = new Method(a.methodName, Type.VOID_TYPE, adviceArgTypes.toArray)
          mv.invokeStatic(Type.getObjectType(toInternalName(a.className)), method)
        case NullAdvice => // do nothing
        case a: InvokeStaticAdviceDesc =>
          val method = new Method(a.methodName, a.desc)
          mv.invokeStatic(Type.getObjectType(toInternalName(a.className)), method)
          a.castTo.foreach(t=>{
            mv.checkCast(t)
          })
      }
    }
  }

  /**
   * Push the arguments for an advice onto the stack
   * @param mv MethodVisitor to generate bytecode on
   * @param a advice to push the arguments for
   * @param argVars list containing the local variables the original method arguments are stored in
   * @param joinPoint join point beeing instrumented
   */
  def pushAdviceArgs(mv: GeneratorAdapter, arguments: Seq[MethodArgument], argVars: Seq[Int], joinPoint: JoinPoint): ListBuffer[Type] = {
    val adviceArgTypes = ListBuffer[Type]()
    for (arg <- arguments) {
      arg match {
        case ThisMethodArgument() if (!joinPoint.location.isStatic) => // only within non-static methods
          mv.loadThis()
          adviceArgTypes.append(Type.getObjectType("java/lang/Object"))

        case MemberClassNameMethodArgument() =>
          mv.push(joinPoint.member.className)
          adviceArgTypes.append(Type.getObjectType("java/lang/String"))

        case MemberClassMethodArgument() =>
          mv.push(Type.getObjectType(toInternalName(joinPoint.member.className)))
          adviceArgTypes.append(Type.getObjectType("java/lang/Class"))

        case MemberNameMethodArgument() =>
          mv.push(joinPoint.member.memberName)
          adviceArgTypes.append(Type.getObjectType("java/lang/String"))

        case JoinPointNameMethodArgument() =>
          mv.push(joinPoint.kind.toString())
          adviceArgTypes.append(Type.getObjectType("java/lang/String"))

        case OriginalArgumentMethodArgument(number) if (number < argVars.length) =>
          mv.loadLocal(argVars(number))
          adviceArgTypes.append(mv.getLocalType(argVars(number)))

        case OriginalArgumentMethodArgument(number) =>
          throw new Error("OriginalArgumentMethodArgument: there is no original argument number " + number)
      }
    }
    return adviceArgTypes
  }
}