package lpf.instrumentation

import org.objectweb.asm._
import JoinPointKind._
import org.objectweb.asm.commons._
import scala.collection.mutable.ListBuffer
import org.objectweb.asm.util.Textifier
import org.objectweb.asm.util.TraceClassVisitor
import java.io.PrintWriter
import InstrumentationUtil._
import scala.collection.JavaConversions._
import org.objectweb.asm.tree.FieldInsnNode
import org.objectweb.asm.tree.VarInsnNode
import org.objectweb.asm.tree.analysis.SourceValue
import org.objectweb.asm.tree.AbstractInsnNode



/**
 * Method adapter instrumenting method enter/leave/finally join points
 *
 * @constructor creates a new adapter
 * @param adviceProvider asked for advice for all join points found
 * @param location method beein instrumented
 * @param mv MethodVisitor to forward to
 * @param methodAccess the method's access flags
 * @param methodName the method's name.
 * @param methodDesc the method's descriptor
 */
class InstrumentingMethodVisitor(data: AnalysisData, adviceProvider: AdviceProviderBase, location: ClassMember, mv: MethodVisitor, methodAccess: Int, methodName: String, methodDesc: String) extends FinallyAdapter(mv, methodAccess, methodName, methodDesc) {

  /*Field Access Adapter 
  Method visitor to instrument field accesses
 *
 * Searches a method for [[lpf.instrumentation.JoinPointKind.BeforeGetField]], [[lpf.instrumentation.JoinPointKind.AfterGetField]],
 * [[lpf.instrumentation.JoinPointKind.BeforeSetField]] and [[lpf.instrumentation.JoinPointKind.AfterSetField]] join points.
 *
 * At each join point the advice container is asked for advice */

  private val localVariablePool = new LocalVariablePool(this)

  /**
   * 0 is TOS
   */
  private def isConstructorThis(stackIndex: Int): Boolean = {
    if (!methodName.equals("<init>"))
      return false;

    val frame = data.currentFrame
    val instructions = frame.getStack(frame.getStackSize() - 1 - stackIndex).asInstanceOf[SourceValue].insns
    return instructions.exists(_ match {
      case n: VarInsnNode if (n.getOpcode() == Opcodes.ALOAD && n.`var` == 0) => true
      case _ => false
    })
  }

  override def visitFieldInsn(opcode: Int, owner: String, name: String, desc: String) {

    val frame = data.currentFrame

    opcode match {
      case Opcodes.GETFIELD =>
        // check if the the field is set on THIS in a constructor
        if (isConstructorThis(1)) {
          // just emit the instruction and skip all instrumentation
          super.visitFieldInsn(opcode, owner, name, desc)
          return
        }

        val member = ClassMember(Type.getObjectType(owner).getClassName(), name, desc, false)
        val beforeAdvice = adviceProvider.getAdvice(JoinPoint(location, member, BeforeGetField))
        val afterAdvice = adviceProvider.getAdvice(JoinPoint(location, member, AfterGetField))
        val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++ afterAdvice.toList.flatMap(_.getArguments)
        val useTemp = allArgs.exists(_.isInstanceOf[OriginalArgumentMethodArgument])

        var start: Label = null
        var end: Label = null
        var argVars = ListBuffer[Int]()
        // we only need to store the objectRef if we need it for the advice 
        if (useTemp) {
          start = new Label()
          end = new Label()
          visitLabel(start)
          argVars += localVariablePool.newLocal(Type.getObjectType("java/lang/Object"))
          storeLocal(argVars(0))
        }

        // handle before advice
        InstrumentationUtil.handleAdvice(beforeAdvice, this, argVars, JoinPoint(location, member, BeforeGetField))

        // load the objectRef for the getfield onto the stack again if using tempVar
        if (useTemp)
          loadLocal(argVars(0))

        // do the GETFIELD
        super.visitFieldInsn(opcode, owner, name, desc)

        // handle after advice
        InstrumentationUtil.handleAdvice(afterAdvice, this, argVars, JoinPoint(location, member, AfterGetField))

        if (useTemp) {
          visitLabel(end)
          localVariablePool.free(argVars(0))
        }

      case Opcodes.GETSTATIC =>
        val member = ClassMember(Type.getObjectType(owner).getClassName(), name, desc, true)
        val beforeAdvice = adviceProvider.getAdvice(JoinPoint(location, member, BeforeGetField))
        val afterAdvice = adviceProvider.getAdvice(JoinPoint(location, member, AfterGetField))
        val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++ afterAdvice.toList.flatMap(_.getArguments)

        // handle before advice
        InstrumentationUtil.handleAdvice(beforeAdvice, this, List(), JoinPoint(location, member, BeforeGetField))

        // do the GETFIELD
        super.visitFieldInsn(opcode, owner, name, desc)

        // handle after advice
        InstrumentationUtil.handleAdvice(afterAdvice, this, List(), JoinPoint(location, member, AfterGetField))

      case Opcodes.PUTFIELD =>
        // check if the the field is set on THIS in a constructor
        if (isConstructorThis(1)) {
          // just emit the instruction and skip all instrumentation
          super.visitFieldInsn(opcode, owner, name, desc)
          return
        }

        val member = ClassMember(Type.getObjectType(owner).getClassName(), name, desc, false)
        val beforeAdvice = adviceProvider.getAdvice(JoinPoint(location, member, BeforeSetField))
        val afterAdvice = adviceProvider.getAdvice(JoinPoint(location, member, AfterSetField))
        val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++ afterAdvice.toList.flatMap(_.getArguments)
        val useTemp = allArgs.exists(_.isInstanceOf[OriginalArgumentMethodArgument])

        var start: Label = null
        var end: Label = null
        var argVars = ListBuffer[Int]()

        // we only need to store the objectRef if we need it for the advice 
        if (useTemp) {
          start = new Label()
          end = new Label()
          visitLabel(start)

          argVars += localVariablePool.newLocal(Type.getObjectType("java/lang/Object"))
          argVars += localVariablePool.newLocal(Type.getType(desc))

          storeLocal(argVars(1))
          storeLocal(argVars(0))
        }

        // handle before advice
        InstrumentationUtil.handleAdvice(beforeAdvice, this, argVars, JoinPoint(location, member, BeforeSetField))

        // load the objectRef for the getfield onto the stack again if using tempVar
        if (useTemp) {
          loadLocal(argVars(0))
          loadLocal(argVars(1))
        }

        // do the GETFIELD
        super.visitFieldInsn(opcode, owner, name, desc)

        // handle after advice
        InstrumentationUtil.handleAdvice(afterAdvice, this, argVars, JoinPoint(location, member, AfterSetField))

        if (useTemp) {
          visitLabel(end)
          argVars.foreach(localVariablePool.free(_))
        }

      case Opcodes.PUTSTATIC =>
        val member = ClassMember(Type.getObjectType(owner).getClassName(), name, desc, true)
        val beforeAdvice = adviceProvider.getAdvice(JoinPoint(location, member, BeforeSetField))
        val afterAdvice = adviceProvider.getAdvice(JoinPoint(location, member, AfterSetField))
        val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++ afterAdvice.toList.flatMap(_.getArguments)
        val useTemp = allArgs.exists(_.isInstanceOf[OriginalArgumentMethodArgument])

        var start: Label = null
        var end: Label = null
        var argVars = ListBuffer[Int]()

        // we only need to store the objectRef if we need it for the advice 
        if (useTemp) {
          start = new Label()
          end = new Label()
          visitLabel(start)

          argVars += localVariablePool.newLocal(Type.getType(desc))

          storeLocal(argVars(0))
        }

        // handle before advice
        InstrumentationUtil.handleAdvice(beforeAdvice, this, argVars, JoinPoint(location, member, BeforeSetField))

        // load the objectRef for the getfield onto the stack again if using tempVar
        if (useTemp) {
          loadLocal(argVars(0))
        }

        // do the GETFIELD
        super.visitFieldInsn(opcode, owner, name, desc)

        // handle after advice
        InstrumentationUtil.handleAdvice(afterAdvice, this, argVars, JoinPoint(location, member, AfterSetField))

        if (useTemp) {
          visitLabel(end)
          argVars.foreach(localVariablePool.free(_))
        }

      case _ => super.visitFieldInsn(opcode, owner, name, desc)
    }

  }

  /* Array Access Adapter */

  override def visitInsn(opcode: Int) {

    opcode match {
      // handle array loads
      case op if (op >= Opcodes.IALOAD && op <= Opcodes.SALOAD) =>
        val beforeAdvice = adviceProvider.getAdvice(JoinPoint(location, location, BeforeArrayRead))
        val afterAdvice = adviceProvider.getAdvice(JoinPoint(location, location, AfterArrayRead))
        val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++ afterAdvice.toList.flatMap(_.getArguments)
        val useTemp = allArgs.exists(_.isInstanceOf[OriginalArgumentMethodArgument])

        var start: Label = null
        var end: Label = null
        var argVars = ListBuffer[Int]()
        // we only need to store the objectRef if we need it for the advice 
        if (useTemp) {
          start = new Label()
          end = new Label()
          visitLabel(start)

          argVars += localVariablePool.newLocal(Type.getObjectType("java/lang/Object"))
          argVars += localVariablePool.newLocal(Type.INT_TYPE)
          storeLocal(argVars(1))
          storeLocal(argVars(0))
        }

        // handle before advice
        InstrumentationUtil.handleAdvice(beforeAdvice, this, argVars, JoinPoint(location, location, BeforeArrayRead))

        // load the array and the index onto the stack again if using tempVar
        if (useTemp) {
          loadLocal(argVars(0))
          loadLocal(argVars(1))
        }

        // do the GETFIELD
        super.visitInsn(opcode)

        // handle after advice
        InstrumentationUtil.handleAdvice(afterAdvice, this, argVars, JoinPoint(location, location, AfterArrayRead))

        if (useTemp) {
          visitLabel(end)
          localVariablePool.free(argVars(0))
          localVariablePool.free(argVars(1))
        }

      // handle array stores
      case op if (op >= Opcodes.IASTORE && op <= Opcodes.SASTORE) =>
        val beforeAdvice = adviceProvider.getAdvice(JoinPoint(location, location, BeforeArrayWrite))
        val afterAdvice = adviceProvider.getAdvice(JoinPoint(location, location, AfterArrayWrite))
        val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++ afterAdvice.toList.flatMap(_.getArguments)
        val useTemp = allArgs.exists(_.isInstanceOf[OriginalArgumentMethodArgument])

        var start: Label = null
        var end: Label = null
        var argVars = ListBuffer[Int]()
        // we only need to store the objectRef if we need it for the advice 
        if (useTemp) {
          start = new Label()
          end = new Label()
          visitLabel(start)

          argVars += localVariablePool.newLocal(Type.getObjectType("java/lang/Object"))
          argVars += localVariablePool.newLocal(Type.INT_TYPE)
          argVars += localVariablePool.newLocal(InstrumentationUtil.getTypeOfArrayStoreInstruction(opcode))
          storeLocal(argVars(2))
          storeLocal(argVars(1))
          storeLocal(argVars(0))
        }

        // handle before advice
        InstrumentationUtil.handleAdvice(beforeAdvice, this, argVars, JoinPoint(location, location, BeforeArrayWrite))

        // load the array and the index onto the stack again if using tempVar
        if (useTemp) {
          loadLocal(argVars(0))
          loadLocal(argVars(1))
          loadLocal(argVars(2))
        }

        // do the array store
        super.visitInsn(opcode)

        // handle after advice
        InstrumentationUtil.handleAdvice(afterAdvice, this, argVars, JoinPoint(location, location, AfterArrayWrite))

        if (useTemp) {
          visitLabel(end)
          localVariablePool.free(argVars(0))
          localVariablePool.free(argVars(1))
          localVariablePool.free(argVars(2))
        }

      case Opcodes.ARRAYLENGTH =>
        val beforeAdvice = adviceProvider.getAdvice(JoinPoint(location, location, BeforeArrayLength))
        val afterAdvice = adviceProvider.getAdvice(JoinPoint(location, location, AfterArrayLength))
        val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++ afterAdvice.toList.flatMap(_.getArguments)
        val useTemp = allArgs.exists(_.isInstanceOf[OriginalArgumentMethodArgument])

        var start: Label = null
        var end: Label = null
        var argVars = ListBuffer[Int]()
        // we only need to store the objectRef if we need it for the advice 
        if (useTemp) {
          start = new Label()
          end = new Label()
          visitLabel(start)

          argVars += localVariablePool.newLocal(Type.getObjectType("java/lang/Object"))
          storeLocal(argVars(0))
        }

        // handle before advice
        InstrumentationUtil.handleAdvice(beforeAdvice, this, argVars, JoinPoint(location, location, BeforeArrayLength))

        // load the array and the index onto the stack again if using tempVar
        if (useTemp) {
          loadLocal(argVars(0))
        }

        // get the array length
        super.visitInsn(Opcodes.ARRAYLENGTH)

        // handle after advice
        InstrumentationUtil.handleAdvice(afterAdvice, this, argVars, JoinPoint(location, location, AfterArrayLength))

        if (useTemp) {
          visitLabel(end)
          localVariablePool.free(argVars(0))
        }
      case _ => super.visitInsn(opcode)
    }
  }

  /* Method Call Adapter */
  override def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String) {
    val isStatic = opcode == Opcodes.INVOKESTATIC;
    val member = ClassMember(Type.getObjectType(owner).getClassName(), name, desc, isStatic)

    var jp = JoinPoint(location, member, BeforeCall)
    jp.isInvokeSpecial = opcode == Opcodes.INVOKESPECIAL
    val beforeAdvice = adviceProvider.getAdvice(jp)

    jp = JoinPoint(location, member, AfterCall)
    jp.isInvokeSpecial = opcode == Opcodes.INVOKESPECIAL
    val afterAdvice = adviceProvider.getAdvice(jp)

    jp = JoinPoint(location, member, AfterCallFinally)
    jp.isInvokeSpecial = opcode == Opcodes.INVOKESPECIAL
    val finallyAdvice = adviceProvider.getAdvice(jp)

    val allArgs = beforeAdvice.toList.flatMap(_.getArguments) ++
      afterAdvice.toList.flatMap(_.getArguments) ++
      finallyAdvice.toList.flatMap(_.getArguments)
    val useTemp = allArgs.exists(_.isInstanceOf[OriginalArgumentMethodArgument])

    val methodType = Type.getMethodType(desc)
    val argVars = ListBuffer[Int]()

    // the parameterTypes contains all the parameters and the method receiver 
    var parameterTypes = methodType.getArgumentTypes().toList
    if (!isStatic) parameterTypes = Type.getObjectType("java/lang/Object") :: parameterTypes

    if (useTemp) {
      // save all method parameters and, if the receiver is non-static, the receiver of the call
      parameterTypes.reverse.foreach { argType =>
        val argVar = localVariablePool.newLocal(argType)
        storeLocal(argVar)
        argVars.prepend(argVar)
      }
    }

    // invoke the before advice
    InstrumentationUtil.handleAdvice(beforeAdvice, this, argVars, JoinPoint(location, member, BeforeCall))

    if (useTemp) {
      // put all method arguments back on the stack
      argVars.foreach(loadLocal(_))
    }

    // set a label to be used for the finally advice
    val beforeLabel = new Label()
    val finallyLabel = new Label()
    val continueLabel = new Label()

    if (finallyAdvice.exists(_ != NullAdvice)) {
      mv.visitTryCatchBlock(beforeLabel, finallyLabel, finallyLabel, null);
      visitLabel(beforeLabel)
    }

    // invoke the original method
    mv.visitMethodInsn(opcode, owner, name, desc)

    // invoke the after advice
    InstrumentationUtil.handleAdvice(afterAdvice, this, argVars, JoinPoint(location, member, AfterCall))

    // invoke the finally advice
    if (finallyAdvice.exists(_ != NullAdvice)) {
      visitJumpInsn(Opcodes.GOTO, continueLabel)

      visitLabel(finallyLabel)

      // this invocation is used only when an exception has been thrown
      InstrumentationUtil.handleAdvice(finallyAdvice, this, argVars, JoinPoint(location, member, AfterCallFinally))

      // rethrow the exception
      visitInsn(Opcodes.ATHROW)

      visitLabel(continueLabel)

      // this is the invocation in the normal control flow
      InstrumentationUtil.handleAdvice(finallyAdvice, this, argVars, JoinPoint(location, member, AfterCallFinally))
    }
  }
  /* MethodEnterLeaveAdapter */
  val enterAdvice = adviceProvider.getAdvice(JoinPoint(location, location, MethodEnter))
  val leaveAdvice = adviceProvider.getAdvice(JoinPoint(location, location, MethodLeave))
  val leaveFinallyAdvice = adviceProvider.getAdvice(JoinPoint(location, location, MethodLeaveFinally))

  override def isFinallyRequired: Boolean = leaveFinallyAdvice.exists(_ != NullAdvice)

  override def onMethodEnter() {
    InstrumentationUtil.handleAdvice(enterAdvice, this, List(), JoinPoint(location, location, MethodEnter))
  }

  override def onMethodExitImpl(opcode: Int) {
    InstrumentationUtil.handleAdvice(leaveAdvice, this, List(), JoinPoint(location, location, MethodLeave))
  }

  override def onFinally(opcode: Int) {
    InstrumentationUtil.handleAdvice(leaveFinallyAdvice, this, List(), JoinPoint(location, location, MethodLeaveFinally))
  }
}