/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import lpf.instrumentation.Instrumentor
import org.objectweb.asm._

/**
 * Class to perform the load time instrumentation of all field accesses
 */
@deprecated("to be removed or made working again","nov.12")
class InstrumentingLoader(val instrumentor: Instrumentor) {
  private val classLoader = new ClassLoader(classOf[InstrumentingLoader].getClassLoader()) {

    override def loadClass(name: String): java.lang.Class[_] = this.synchronized {
      // First, check if the class has already been loaded
      //var c = findLoadedClass(name);
      var c: Class[_] = null;
      if (c == null) {
        // use the base implementation if the class does not need to be instrumented
        if (!instrumentor.needsInstrumentation(this, name,null)) {
          c = super.loadClass(name)
        } else {
          val cr = new ClassReader(name)
          val cw = new ClassWriter(cr, 0) // ClassWriter.COMPUTE_FRAMES)
          //if (instrumentor.needsInstrumentation(name))
          instrumentor.instrument(name, this, cr, cw)
          val bytes = cw.toByteArray()
          c = defineClass(name, bytes, 0, bytes.length)
          resolveClass(c)
        }
      }
      c
    }
  }

  def loadClass(name: String): Class[_] = classLoader.loadClass(name)

  /**
   * Perform the load time instrumentation and call the main class.
   */
  def invokeMain(mainClass: String, args: Array[String]) = {

    val clazz = classLoader.loadClass(mainClass);
    val main = clazz.getMethod("main", classOf[Array[String]])
    main.invoke(null, args)
  }
}
