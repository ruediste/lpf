/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.instrumentation
import scala.collection.mutable.HashSet
import scala.collection.Set
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

@deprecated("not used anymore", "11.12")
class MethodInstrumentationRuleList {
	private class Rule(val className: String, val predicates: Set[String], val actions: Set[String]){
	}

	private val rules=HashMap[String,ArrayBuffer[Rule]]()
	
	protected def when[A](invokedMethods: String*)(changedMethods: String*)(implicit m: ClassTag[A]): Unit={
		val className=m.runtimeClass.getName()
		val ruleSet=rules.getOrElseUpdate(className,ArrayBuffer())
		ruleSet+=new Rule(className, invokedMethods.toSet, changedMethods.toSet)
	}

	def getAffectedDependencies(className: String, invokedMethod: String): Set[String]={
		val affectedMethods=HashSet[String]()
		rules.get(className).foreach(rules=>{
			affectedMethods++=
				rules
					// get all rules which have the invoked method in the predicates
					.filter(_.predicates.exists(invokedMethod.equals(_)))
					// join all affected methods
					.flatMap(_.actions)
		})
		return affectedMethods
	}
	
	when[Rule]("add","remove")("getIterator")
}
