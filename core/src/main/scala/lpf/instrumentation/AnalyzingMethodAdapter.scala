package lpf.instrumentation

import org.objectweb.asm._
import org.objectweb.asm.tree._
import org.objectweb.asm.tree.analysis._

class AnalysisData {
  var currentIndex: Int = 0
  var frames: Array[analysis.Frame] = null
  def currentFrame = frames(currentIndex)
}

class MyInsnList(data: AnalysisData) extends InsnList {
  override def accept(mv: MethodVisitor) {
    var insn = getFirst();
    var index = 0
    while (insn != null) {
      data.currentIndex = index
      insn.accept(mv);
      insn = insn.getNext();
      index = index + 1
    }
  }
}

class MyMethodNode(data: AnalysisData, methodAccess: Int, methodName: String, methodDesc: String) extends MethodNode(methodAccess, methodName, methodDesc, null, null) {
  instructions = new MyInsnList(data)
}

class AnalyzingMethodAdapter(data: AnalysisData, adviceProvider: AdviceProviderBase, location: ClassMember, next: MethodVisitor, methodAccess: Int, methodName: String, methodDesc: String)
  extends MethodVisitor(Opcodes.ASM4, new MyMethodNode(data, methodAccess, methodName, methodDesc)) {

  override def visitEnd() {
    val methodNode = mv.asInstanceOf[MethodNode];
    val analyzer = new Analyzer(new SourceInterpreter())
    data.frames = analyzer.analyze(InstrumentationUtil.toInternalName(location.className), methodNode)
    
    methodNode.accept(next)
  }
}