package lpf.instrumentation
import JoinPointKind._

/**
 * Represents a join point
 * 
 * During instrumentation, the classes are searched for so-called join points. At each join point,
 * an [[lpf.instrumentation.AdviceProviderBase AdviceProvider]] is queried for advice. If an advice is returned,
 * the join point is instrumented.
 * 
 * @constructor creates a JoinPoint
 * @param location location of the join point (method)
 * @param member member accessed at the join point
 * @param kind kind of the join point
 */
case class JoinPoint(location: ClassMember, member: ClassMember, kind: JoinPointKind) {
  var isInvokeSpecial = false 
}