package lpf.instrumentation

import java.lang.instrument.ClassFileTransformer
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Type
import lpf.util.TimeMonitor
import org.objectweb.asm.util.CheckClassAdapter

/**
 * A ClassFileTransformer using an [[lpf.instrumentation.Instrumentor]] to transform the classes.
 * Used by the InstrumentationAgent
 *
 * @constructor Create a new InstrumentingTransformer
 *
 * @param class name of the advice provider to be used by the instrumentor
 */
class InstrumentingTransformer(val adviceProviderClassName: String) extends ClassFileTransformer {
  val adviceProvider: AdviceProviderBase = getClass().getClassLoader()
    .loadClass(adviceProviderClassName)
    .newInstance().asInstanceOf[AdviceProviderBase]

  val instrumentor = new Instrumentor(adviceProvider)

  override def transform(loader: ClassLoader, slashedClassName: String, clazz: Class[_],
    domain: java.security.ProtectionDomain, bytes: Array[Byte]): Array[Byte] = {
    // println("InstrumentingTransformer: called for " + slashedClassName)
    

    try {
      // replace the slashes in the provided class name
      val className = Type.getObjectType(slashedClassName).getClassName()

      // skip classes in some packages causing troubles
      if (className.startsWith("java.") || className.startsWith("sun.")|| className.startsWith("com.sun."))
        return bytes
        
      // skip CGLIB generated stuff (might be weakened in the future)
      if (className.split("\\.").last.contains("$$EnhancerByCGLIB$$")){
        //println("skipping" + className) 
        return bytes
      }
        
      // check if the class needs to be instrumented
      if (!instrumentor.needsInstrumentation(loader, className, bytes))
        return bytes

      //println("instrumenting " + slashedClassName)

      // load the class
      val cr = new ClassReader(bytes)
      val cw = new ClassWriter(cr, ClassWriter.COMPUTE_FRAMES)
      instrumentor.instrument(className, loader, cr, cw)
      
      // verify class
      if (true){
        val ccr=new ClassReader(cw.toByteArray())
        val ccw=new ClassWriter(ccr,0)
        val ccv=new CheckClassAdapter(ccw)
        ccr.accept(ccv, ClassReader.EXPAND_FRAMES)
      }
      return cw.toByteArray()
    } catch {
      case t: Throwable => {
        t.printStackTrace()
        throw t
      }

    }
  }
}