package lpf.instrumentation

import org.objectweb.asm.Type

/**
 * Represents actions to be taken at a [[lpf.instrumentation.JoinPoint]]
 */
abstract class Advice {

  /**
   * return all arguments used by this advice
   */
  def getArguments: Seq[MethodArgument] = Seq()
}

/**
 * Advice to invoke a static method
 *
 * @constructor create an InvokeStaticAdvice
 * @param className class containing the static method to be invoked
 * @param methodName name of the static method to be invoked
 * @param arguments list of the arguments of the method
 */
class InvokeStaticAdvice(val className: String, val methodName: String, val arguments: Seq[MethodArgument]) extends Advice {
  override def getArguments: Seq[MethodArgument] = arguments
}

object InvokeStaticAdvice {
  def apply(className: String, methodName: String, arguments: MethodArgument*) = new InvokeStaticAdvice(className, methodName, arguments)
}

class InvokeStaticAdviceDesc(val className: String, val methodName: String, val desc: String, val castTo: Option[Type]) extends Advice {

}

object InvokeStaticAdviceDesc {
  def apply(className: String, methodName: String, desc: String, castTo: Option[Type]) = new InvokeStaticAdviceDesc(className, methodName, desc, castTo)
}

object NullAdvice extends Advice {
}