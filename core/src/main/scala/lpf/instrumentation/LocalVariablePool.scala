package lpf.instrumentation

import org.objectweb.asm.commons.LocalVariablesSorter
import scala.collection.mutable.ListBuffer
import org.objectweb.asm.Type
import scala.collection.mutable.HashMap
import scala.collection.mutable.Stack
import scala.collection.mutable.Set

/**
 * A local variable pool is used to keep track of newly added local variables.
 *
 * It contains a list of variables for each type. If a method visitor is done with a variable,
 * it can use free() to return the variable to the pool.
 *
 * If a variable of the same type is required later, it is reused.
 *
 * All local variables passed to the free() method must have been acquired using the newLocal()
 * method.
 */
class LocalVariablePool(val mv: LocalVariablesSorter) {

  /**
   * all variables managed by the pool
   */
  private val managedVariables = Set[Int]()
  
  /**
   * all variables currently free. Contains all variables in ''variables''
   */
  private val freeVariables = Set[Int]()

  /**
   * maps each type to the list of free variables
   */
  private val variables = HashMap[Type, Stack[Int]]()
  
  /**
   * maps each managed variable to it's type
   */
  private val variableType = HashMap[Int, Type]()

  private def getVariableStack(t: Type) = variables.getOrElseUpdate(t, Stack())

  /**
   * get the type of a managed variable
   */
  def getVariableType(localVar: Int) = variableType.get(localVar).get

  /**
   * allocate a new local variable which can be returned to the pool later using free()
   */
  def newLocal(t: Type): Int = {
    val vars = getVariableStack(t)
    if (vars.isEmpty) {
      // There are no free variables for the requested type at the moment
      // Get a new one
      val tmp = mv.newLocal(t)
      variableType.put(tmp, t)
      managedVariables.add(tmp)
      return tmp
    } else {
      // Variable for reuse found
      val tmp = vars.pop
      assert(freeVariables.contains(tmp))
      freeVariables.remove(tmp)
      tmp
    }
  }

  /**
   * return a local variable to the pool. The variable must have been created using the newLocal() method
   */
  def free(localVar: Int) = {
    if (!managedVariables.contains(localVar))
      throw new Error("Unmanaged variable: " + localVar)
    if (freeVariables.contains(localVar))
      throw new Error("Variable has been freed before: " + localVar)
    
    freeVariables.add(localVar)
    val vars = getVariableStack(getVariableType(localVar))
    vars.push(localVar)
  }
}