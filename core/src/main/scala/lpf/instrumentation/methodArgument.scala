package lpf.instrumentation

/**
 * Base class for method arguments used by [[lpf.instrumentation.Advice]]s
 */
abstract class MethodArgument {}

/**
 * The this object at the location of the join point beeing instrumented.
 * Not available in static methods (java.lang.Object)
 */
case class ThisMethodArgument() extends MethodArgument {}

/**
 * The name of the member accessed by the current join point (String)
 */
case class MemberNameMethodArgument() extends MethodArgument {}

/**
 * the name of the class containing the member of the current join point (String)
 */
case class MemberClassNameMethodArgument() extends MethodArgument {}

/**
 * The class of the class containing the member of the current join point (Class[_])
 */
case class MemberClassMethodArgument() extends MethodArgument {}

/**
 * The kind of the join point (String). Obtained by 
 * {{{
 * joinPoint.kind.toString()
 * }}}
 */
case class JoinPointNameMethodArgument() extends MethodArgument {}

/**
 * Original argument of the join point. Depends on the kind of join point. 
 * See [[lpf.instrumentation.JoinPointKind]] for details
 */
case class OriginalArgumentMethodArgument(val number: Int) extends MethodArgument{}

