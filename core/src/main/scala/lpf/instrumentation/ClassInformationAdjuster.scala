package lpf.instrumentation

import lpf.hornet.ClassInformation

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  override def adjustClassInformation(info: ClassInformation) {
	  info.setApplicationClass(false)
  }
}