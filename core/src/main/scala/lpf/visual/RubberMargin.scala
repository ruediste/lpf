/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.visual
import _root_.lpf._

class RubberMargin extends SingleChildVisualLike with VisualWithThickness[RubberMargin]{
	override type Self=RubberMargin
	override def self=this
	
	override def arrangeOverride(actualSize: Size): Unit = {
		val childSize=if (child.isDefined)
			child.get.desiredSize
		else
			Size(0,0)
			
		val rubberLengthX=actualSize.width-childSize.width
		val rubberSumX=left+right
		val rubberFactorX=if (rubberSumX==0) 0.0 else rubberLengthX/rubberSumX
		
		val rubberLengthY=actualSize.height-childSize.height
		val rubberSumY=top+bottom
		val rubberFactorY=if (rubberSumY==0) 0.0 else rubberLengthY/rubberSumY
		
		
		child.foreach(_.arrange(Rectangle(
				Point(left*rubberFactorX,top*rubberFactorY),
				Size(
				actualSize.width-(left+right)*rubberFactorX,
				actualSize.height-(top+bottom)*rubberFactorY
				))))
	}
}

object RubberMargin extends VisualObject{
  type Self=RubberMargin
  override def apply()=new RubberMargin()
}
