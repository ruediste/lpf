/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual.listener

import lpf.WeakEventNoArgs
import lpf.event.FocusHandle
import lpf.event.VisualEventDispatcher
import lpf.event.MouseEventType
import lpf.Visual
import lpf.Referencer
import lpf.StrongEventNoArgs
import lpf.event.EventModifier
import lpf.event.MouseButton

/**
 * Listener to listen for button-like clicks.
 *
 * Some Rules are:
 * - when mouse is pressed over the button it get held down
 * - if the mouse is released over the button, a click is generated
 * - if the mouse is pressed moved away from the button, it gets up again
 * - if the mouse is pressed and moved over the button afterwards, it does not get
 *   held down
 */
class ButtonLikeClickListener extends ListenerBase {
  /**
   * true if the button is currently held down
   */
  var isDown = false

  /**
   * raised when the button is clicked
   */
  val clicked = StrongEventNoArgs()

  var button = MouseButton.Button1

  private var focusHandleOption: Option[FocusHandle] = None

  def initialize() {
    VisualEventDispatcher.register(target, MouseEventType.pressed) { e =>
      if (e.button != button) return

      // the button gets held down
      isDown = true
      e.handled = true

      // grab the focus
      if (focusHandleOption.isEmpty) {
        focusHandleOption = Some(VisualEventDispatcher.grabFocus(target, _.eventType == MouseEventType.released))
      }
    }

    VisualEventDispatcher.register(target, MouseEventType.released) { e =>
      if (e.button != button)
        return

      //println("released")
      val wasDown = isDown
      isDown = false
      e.handled = true

      // check if we have the focus
      if (focusHandleOption.isDefined) {
        // release the focus
        VisualEventDispatcher.releaseFocus(focusHandleOption.get);
        focusHandleOption = None
        if (wasDown) {
          //println("BLCL: clicked()")
          clicked()
        }
      }

    }

    VisualEventDispatcher.register(target, MouseEventType.entered) { e =>
      //println("entered")
      if (focusHandleOption.isDefined) isDown = true
    }

    VisualEventDispatcher.register(target, MouseEventType.leaved) { e =>
      //println("leaved")
      if (focusHandleOption.isDefined) isDown = false
    }
  }
}

object ButtonLikeClickListener extends ListenerObjectBase {
  type Self = ButtonLikeClickListener
  def apply() = new ButtonLikeClickListener()
}
