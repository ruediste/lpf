package lpf.visual.listener

import lpf._
import lpf.event.VisualEventDispatcher
import lpf.event.MouseEventType
import lpf.event.EventModifier._
import lpf.event.MouseButton
import lpf.event.MouseEvent

class ClickListener extends ListenerBase {
  val clicked = StrongEvent[MouseEvent]()
  

  var button = MouseButton.Button1

  override def initialize() {
    VisualEventDispatcher.register(target, MouseEventType.clicked) { e =>
      if (e.button != button) return
      
      e.handled = true
      clicked(e)
    }
  }
}

object ClickListener extends ListenerObjectBase {
  type Self = ClickListener
  def apply() = new ClickListener
}