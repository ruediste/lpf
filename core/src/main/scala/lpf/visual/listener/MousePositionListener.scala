package lpf.visual.listener

import lpf.event.VisualEventDispatcher
import lpf.event.VisualEventType
import lpf.event.MouseEventType
import lpf.Point

class MousePositionListener extends ListenerBase {
  var lastPosition: Point=Point()
  override def initialize() {
    VisualEventDispatcher.register(target, MouseEventType.moved, true) { e =>
      lastPosition=e.point
    }
  }
}

object MousePositionListener extends ListenerObjectBase {
  type Self = MousePositionListener
  def apply() = new MousePositionListener
}