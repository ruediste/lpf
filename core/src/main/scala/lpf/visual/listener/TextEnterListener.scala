package lpf.visual.listener

import lpf.event.KeyEventDispatcher
import lpf.event.KeyEventType
import lpf.StrongEvent
import lpf.StrongEventNoArgs

class TextEnterListener extends ListenerBase {
  val charEntered = StrongEvent[Char]()
  val delete = StrongEventNoArgs()
  val backspace = StrongEventNoArgs()
  val textCommited = StrongEventNoArgs()

  def initialize() {
    KeyEventDispatcher.register(target, KeyEventType.typed) { e =>
      //println("typed "+e.keyChar.toInt)
      e.keyChar.toInt match {
        case 127 => delete()
        case 8 => backspace()
        case 10 => textCommited()
        case ch if (Character.isISOControl(ch)) => // ignore
        case _ => charEntered(e.keyChar)
      }
    }
  }
}

object TextEnterListener extends ListenerObjectBase {
  type Self = TextEnterListener
  def apply() = new TextEnterListener()
}