package lpf.visual.listener

import lpf.StrongEventNoArgs
import lpf.event.KeyEventDispatcher
import lpf.event.KeyEventType
import lpf.event.EventModifier
import lpf.event.KeyboardFocusManager
import lpf.event.StandardFocusTraversalPolicy

class KeyboardFocusTraversalListener extends ListenerBase {
  val focusNext = StrongEventNoArgs()
  val focusPrevious = StrongEventNoArgs()

  def initialize() {
    // make the target reachable by the focus traversal
    StandardFocusTraversalPolicy.setCanGetFocus(target, true)

    // register for key press events
    KeyEventDispatcher.register(target, KeyEventType.pressed) { e =>
      e.keyCode match {
        // tab plus shift
        case 9 if (e.modifiers.equals(Set(EventModifier.Shift))) => focusPrevious()
        // tab only
        case 9 => focusNext()
        case _ => // ignore
      }
    }
  }
}

object KeyboardFocusTraversalListener extends ListenerObjectBase {
  type Self = KeyboardFocusTraversalListener
  def apply() = new KeyboardFocusTraversalListener()
}