package lpf.visual.listener

import lpf.Visual
import lpf.StrongEventNoArgs
import lpf.event.VisualEventDispatcher
import lpf.Referencer
import lpf.CompanionObjectBase

abstract class ListenerBase extends Referencer {

  protected def initialize(): Unit

  private var _target: Visual = null

  def target_=(v: Visual) = {
    if (_target != null)
      throw new Error("can't redefine target")

    _target=v
    initialize()
  }

  def target = _target
}

abstract class ListenerObjectBase extends CompanionObjectBase {
  type Self <: ListenerBase
  def apply(v: Visual): Self = {
    val result = apply()
    result.target=v
    result
  }
}