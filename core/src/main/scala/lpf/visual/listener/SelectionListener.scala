package lpf.visual.listener

import lpf.StrongEvent
import lpf.Point
import lpf.event.VisualEventDispatcher
import lpf.event.MouseEventType
import lpf.event.MouseButton
import lpf.event.FocusHandle

class SelectionListener extends ListenerBase {
  val selectionStarted = StrongEvent[Point]()
  val selectionChanged = StrongEvent[(Point, Point)]()

  var button = MouseButton.Button1

  private var startPosition: Option[Point] = None
  var focusHandle: Option[FocusHandle] = None

  def initialize() {
    VisualEventDispatcher.register(target, MouseEventType.dragged, true) { e =>
      println("mouseMoved")
      // during a selection, a mouse move modifies the selection
      startPosition.foreach(p => selectionChanged((p, e.point)))
    }

    // the selection is started whenever the mouse button is pressed
    VisualEventDispatcher.register(target, MouseEventType.pressed, true) { e =>
      if (e.button != button) return

      focusHandle = Some(VisualEventDispatcher.grabFocus(target, e => List(MouseEventType.dragged, MouseEventType.released).contains(e.eventType)))
      startPosition = Some(e.point)
      selectionStarted(startPosition.get)
    }

    // the selection ends when the mouse button is released again
    VisualEventDispatcher.register(target, MouseEventType.released, true) { e =>
      if (e.button != button) return
      focusHandle.foreach(VisualEventDispatcher.releaseFocus(_))
      startPosition = None
    }

  }
}

object SelectionListener extends ListenerObjectBase {
  type Self = SelectionListener
  def apply() = new SelectionListener()
}