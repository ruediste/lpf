package lpf.visual.listener

import lpf.event.EventModifier._
import lpf.event.MouseEvent

trait ModifierFilteringListener extends ListenerBase {
  /**
   * An event is accepted only if all the modifiers in the modifierMask are set. See also useOr
   */
  var modifierMask: Set[EventModifier] = Set()

  /**
   * By default, an event is only accepted if all the modifiers in the modifierMask are set.
   * If this variable is set to true, the event is accepted if at least one of the modifiers
   * in the mask is set.
   */
  var useOrForModifierMask = false

  protected def isAccepted(e: MouseEvent) = useOrForModifierMask match {
    case true => !modifierMask.intersect(e.modifiers).isEmpty
    case false => modifierMask.subsetOf(e.modifiers)
  }
}