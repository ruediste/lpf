package lpf.visual.listener

import lpf.event.KeyEventDispatcher
import lpf.event.KeyEventType
import lpf.StrongEventNoArgs

class TextMoveListener extends ListenerBase {
  val left=new StrongEventNoArgs()
  val right=new StrongEventNoArgs()
  
  def initialize(){
	  KeyEventDispatcher.register(target, KeyEventType.pressed) { e =>
        e.keyCode match {
          case 37 => left()
          case 39 => right()
          case _ =>
        }
      }
	}
}

object TextMoveListener extends ListenerObjectBase {
  type Self = TextMoveListener
  def apply() = new TextMoveListener()
}