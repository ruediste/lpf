/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual.listener
import lpf._
import _root_.lpf.event.VisualEventDispatcher
import lpf.event.MouseEventType

class MouseOverListener extends ListenerBase {
  var isMouseOver: Boolean = false

  private def setMouseOver(b: Boolean) {
    //println("setMouseOver")
    if (b != isMouseOver) isMouseOver = b
  }

  override def initialize() {
    VisualEventDispatcher.register(target, MouseEventType.entered) { e => setMouseOver(true) }
    VisualEventDispatcher.register(target, MouseEventType.leaved) { e => setMouseOver(false) }
  }

}

object MouseOverListener extends ListenerObjectBase {
  type Self = MouseOverListener
  def apply() = new MouseOverListener()
}