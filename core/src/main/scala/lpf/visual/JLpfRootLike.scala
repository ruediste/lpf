/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual;

import lpf._
import lpf.dependency.HornetEventListener
import java.awt.event.ComponentListener
import java.awt.event.ComponentEvent
import java.awt.Graphics2D
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.event.ComponentAdapter
import lpf.event.VisualEventDispatcher
import lpf.event.{ MouseEvent => LpfMouseEvent }
import lpf.event.MouseEventType
import java.awt.AWTEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseMotionAdapter
import lpf.dependency.ChangeNotifier
import javax.swing.JComponent
import java.awt.Dimension
import java.awt.LayoutManager2
import java.awt.event.KeyListener
import lpf.event.KeyEventDispatcher
import lpf.event.KeyEvent
import lpf.event.KeyEventType
import java.awt.geom.AffineTransform
import java.awt.RenderingHints
import scala.collection.JavaConversions._

trait JLpfRootLike extends JComponent with Referencer {
  /**
   * abstract variable to contain the root visual
   */
  var _visual: Option[Visual] = None

  def visual = _visual
  def visual_=(v: Option[Visual]) {
    // detach the old visual
    _visual.foreach(JLpfRootLike.key.set(_, None))

    // check if the supplied visual is not already in another root pane
    if (v.exists(JLpfRootLike.key.isDefined(_)))
      throw new Error("Visual already in another root pane")

    // set the new visual
    _visual = v
    _visual.foreach(JLpfRootLike.key.set(_, Some(this)))
  }

  /**
   * The visibility buffer is used for hit testing
   */
  @unobserved var visibilityBuffer: Option[VisibilityBuffer] = None

  val enterLeaveEventGenerator = new EnterLeaveEventGenerator()

  private def findVisual(awtEvent: java.awt.event.MouseEvent) = {
    val res = visibilityBuffer flatMap (_.getVisual(awtEvent.getX(), awtEvent.getY()))
    //println("findVisual: "+res)
    res

  }

  addMouseListener(new MouseAdapter {
    override def mouseEntered(evt: MouseEvent) = enterLeaveEventGenerator.setNewVisual(evt, findVisual(evt))
    override def mouseExited(evt: MouseEvent) = enterLeaveEventGenerator.setNewVisual(evt, None)
    override def mousePressed(e: MouseEvent) = sendMouseEvent(MouseEventType.pressed, e)
    override def mouseReleased(e: MouseEvent) = sendMouseEvent(MouseEventType.released, e)
    override def mouseClicked(e: MouseEvent) = sendMouseEvent(MouseEventType.clicked, e)
  })

  addMouseMotionListener(new MouseMotionAdapter {
    override def mouseMoved(e: MouseEvent) = {
      enterLeaveEventGenerator.setNewVisual(e, findVisual(e))
      sendMouseEvent(MouseEventType.moved, e)
    }

    override def mouseDragged(e: MouseEvent) = {
      enterLeaveEventGenerator.setNewVisual(e, findVisual(e))
      sendMouseEvent(MouseEventType.dragged, e)
    }

  });

  private def sendMouseEvent(event: _root_.lpf.event.VisualEventType[LpfMouseEvent], awtEvent: MouseEvent) = {
    // get the target from the visibility buffer
    findVisual(awtEvent).foreach {
      (target =>
        // construct and send the event
        VisualEventDispatcher.dispatch(new LpfMouseEvent(target, event, awtEvent)))
    }
    //awtEvent.consume()
  }

  addKeyListener(new KeyListener() {
    def keyPressed(e: java.awt.event.KeyEvent): Unit =
      KeyEventDispatcher.dispatch(new KeyEvent(KeyEventType.pressed, e))
    def keyReleased(e: java.awt.event.KeyEvent): Unit =
      KeyEventDispatcher.dispatch(new KeyEvent(KeyEventType.released, e))
    def keyTyped(e: java.awt.event.KeyEvent): Unit =
      KeyEventDispatcher.dispatch(new KeyEvent(KeyEventType.typed, e))
  })

  /**
   * overridden to avoid flickering (there is a clearRect() call in the super implementation)
   */
  override def update(g: java.awt.Graphics) = if (isShowing()) paint(g)

  private def createVisibilityBuffer: Unit = {
    if (visibilityBuffer.isEmpty)
      visibilityBuffer = Some(new VisibilityBuffer(getWidth(), getHeight()))
  }

  def myPaintChildren(g: java.awt.Graphics): Unit

  val paintNotifier = ChangeNotifier()
  override def paint(g: java.awt.Graphics) {
    //println(Thread.currentThread().hashCode() + " JLpfRootLike:paint() enter ")

    val g2 = g.asInstanceOf[Graphics2D]

    GraphicsParametersManager.setupGraphicsHints(g2)
    
    // make sure there is a visibility buffer
    createVisibilityBuffer
    visibilityBuffer.get.reset()

    // Capture the dependencies of the paint process and call repaint when one of them changes.
    // the repaint function initates a new painting
    // sometimes in the future
    paintNotifier.notifyOfChanges(Application.awtQueueHelper.queuePaint(() => repaint()), {
      visual.foreach { v =>
        v.paint(new _root_.lpf.Graphics(
          new AffineTransform(),
          g2,
          visibilityBuffer.get.graphics(v),
          GraphicsContext(g.asInstanceOf[Graphics2D], visibilityBuffer.get)))
      }
    })

    // paint Swing children contained in SwingPanes
    myPaintChildren(g)
    //println(Thread.currentThread().hashCode()+" JLpfRootLike:paint() leave")
  }

  private var measuredSize: Option[Size] = None

  private val layoutNotifier = ChangeNotifier()
  
  @unobserved private var layoutPending=false 
  /**
   * Queue the layout in the AwtQueueHelper
   */
  private def queueLayout(){
    // only queue the layout if it is not yet pending
    if (!layoutPending){
      layoutPending=true
      Application.awtQueueHelper.queueLayout(()=>performLpfLayout)
    }
  } 
  
  /**
   * Called when the AwtQueueHelper processes a queued layout
   */
  def performLpfLayout() {
    // only do the layout if it has been made pending before
    if (!layoutPending)
      return
      
    layoutPending=false
    
    val insets = getInsets()
    val size = Size(getWidth() - insets.left - insets.right, getHeight() - insets.top - insets.bottom)
    val rect = Rectangle(_root_.lpf.Point(insets.left, insets.top), size)

    layoutNotifier.notifyOfChanges(queueLayout(), {
      visual.foreach { v =>
        measuredSize = Some(v.measure(size))
        v.arrange(rect)
        revalidate()
      }
    })
  }

  override def doLayout() {
    removeAll()
    visual.toList.flatMap(_.transitiveChildren).foreach(_ match {
      case sp: SwingPane => {
        sp.jComponent.foreach(add(_))
        sp.updateBoundsOfComponent
      }
      case _ =>
    })
  }

  def getMeasuredSize: Option[Size] = {
    performLpfLayout()
    measuredSize
  }

  override def getPreferredSize() = {
    getMeasuredSize.map(_.toDimension).getOrElse(new java.awt.Dimension())
  }
  override def getMinimumSize() = getPreferredSize()

  addComponentListener(new ComponentAdapter() {
    override def componentResized(e: ComponentEvent): Unit = {
      // destroy the old visibilityBuffer
      visibilityBuffer.foreach(_.dispose)
      visibilityBuffer = None

      // update layout whenever the size of the canvas changes
      queueLayout()
    }
  });

  setLayout(null)
  setDoubleBuffered(true);
  setEnabled(true)
  setFocusable(true)
  setFocusTraversalKeysEnabled(false)

  // queue an initial layout
  queueLayout()
}

object JLpfRootLike {
  private val key = ReferenceKey[Option[JLpfRootLike]](None)

  def getRoot(v: Visual): Option[JLpfRootLike] = key.get(v.path.head)
}