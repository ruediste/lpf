package lpf.visual

import lpf.VisualObject
import lpf.Graphics
import lpf.Point

class CrossPainter extends SingleChildVisualLike {
  override type Self = CrossPainter
  override def self = this

  var crossPosition = Point()
  override def paintOverride(g: Graphics) {
    super.paintOverride(g)
    g.drawLine(crossPosition.x - 10, crossPosition.y, crossPosition.x + 10, crossPosition.y)
    g.drawLine(crossPosition.x, crossPosition.y - 10, crossPosition.x, crossPosition.y + 10)

  }
}

object CrossPainter extends VisualObject {
  type Self = CrossPainter
  def apply() = new CrossPainter()
}