/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual
import lpf._
import _root_.lpf.visual._

trait SingleChildVisualLike extends Visual
  with SingleChildNodeLike[Visual, ParentVisualLike]
  with ParentVisualLike {

  override def measureOverride(availableSize: Size): Size = {
    if (child.isDefined)
      child.get.measure(availableSize)
    else
      Size(0, 0)
  }

  override def arrangeOverride(actualSize: Size): Unit = {
    children.foreach(_ arrange (actualSize))
  }
}

class SingleChildVisual extends SingleChildVisualLike {
  type Self = SingleChildVisual
  def self = this
}

object SingleChildVisual extends VisualObject {
  type Self = SingleChildVisual
  def apply() = new SingleChildVisual()
}