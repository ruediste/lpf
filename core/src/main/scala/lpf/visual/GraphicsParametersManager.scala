package lpf.visual

import java.awt.Graphics2D
import java.awt.RenderingHints
import scala.collection.JavaConversions._
import java.awt.font.FontRenderContext

/**
 * Manage graphics parameters.
 * 
 * They have to be linked to the FontRenderContext, since the measuring
 * of text is affected by the parameters.
 */
object GraphicsParametersManager {

  def setupGraphicsHints(g: Graphics2D) {
    g.setRenderingHints(Map(
      RenderingHints.KEY_TEXT_ANTIALIASING -> RenderingHints.VALUE_TEXT_ANTIALIAS_ON,
      RenderingHints.KEY_FRACTIONALMETRICS -> RenderingHints.VALUE_FRACTIONALMETRICS_ON))
  }
  
  val fontRenderContext=new FontRenderContext(null, true, true);
}