/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual

import lpf.MultiChildrenVisualLike
import lpf.Size
import lpf.Point
import lpf.Rectangle
import lpf.Graphics
import lpf.ElementObject
import lpf.VisualObject

class StackPanel extends MultiChildrenVisualLike {
  type Self = StackPanel
  def self = this

  var orientation = Orientation.vertical

  override def measureOverride(availableSize: Size): Size = {
    var remainingSize = availableSize
    var size = Size(0, 0)
    for (child <- children) {
      val childSize = child.measure(remainingSize)
      orientation match {
        case Orientation.horizontal =>
          size = Size(size.width + childSize.width, size.height.max(childSize.height))
          remainingSize = Size(availableSize.width - size.width, size.height)
        case Orientation.vertical =>
          size = Size(size.width.max(childSize.width), size.height + childSize.height)
          remainingSize = Size(size.width, availableSize.height - size.height)
      }
    }
    size
  }

  override def arrangeOverride(actualSize: Size): Unit = {
    orientation match {
      case Orientation.horizontal =>
        var currentLeft = 0.0
        for (child <- children) {
          child.arrange(Rectangle(Point(currentLeft, 0), Size(child.desiredSize.width, actualSize.height)))
          currentLeft += child.desiredSize.width
        }

      case Orientation.vertical =>
        var currentTop = 0.0
        for (child <- children) {
          child.arrange(Rectangle(Point(0, currentTop), Size(actualSize.width, child.desiredSize.height)))
          currentTop += child.desiredSize.height
        }
    }

  }
}

object StackPanel extends VisualObject {
  type Self = StackPanel
  def apply() = new StackPanel()
}
