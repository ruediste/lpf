package lpf.visual

import lpf._
import javax.swing.JComponent
import java.awt.geom.AffineTransform

class SwingPane extends Visual {
  override type Self = SwingPane
  override def self = this

  private var _jComponent: Option[JComponent] = None

  def jComponent_=(component: JComponent) {
    _jComponent = Some(component)
  }

  def jComponent = _jComponent

  override def measureOverride(availableSize: Size): Size = {
    if (jComponent.isEmpty) {
      Size(0, 0)
    } else {
      Size(_jComponent.get.getPreferredSize())
    }
  }

  override def arrangeOverride(actualSize: Size): Unit = {

  }

  /**
   * update the bounds of the Swing component contained in this pane
   */
  def updateBoundsOfComponent() {
    val t=new AffineTransform()
    path.foreach(_.concatenateVisualTransformTo(t))
    val placement=t.createTransformedShape(new java.awt.geom.Rectangle2D.Double(0,0,actualSize.width,actualSize.height)).getBounds()
    _jComponent.foreach(_.setBounds(placement))
  }
}

object SwingPane extends VisualObject {
  type Self = SwingPane
  def apply() = new SwingPane()
}
