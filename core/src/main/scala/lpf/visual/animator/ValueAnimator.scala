package lpf.visual.animator

import lpf.visual.Animator

abstract class ValueAnimator extends Animator {
  protected var _value: Double = 0.0
  def value = _value

  private var _target: Double = 0.0
  def target = _target
  def target_=(d: Double) {
    if (_target != d) {
      _target = d
      enable()
    }
  }
}

class LinearAnimator extends ValueAnimator {

  private var _speed: Double = 1.0
  def speed = _speed
  def speed_=(d: Double) = {
    if (d == 0)
      throw new IllegalArgumentException("speed may not be zero")

    if (d < 0)
      throw new IllegalArgumentException("speed may not be negative, was: " + d)
    _speed = d
  }

  override protected def frame(elapsedTime: Double) {
    val distance = speed * elapsedTime
    val direction = Math.signum(target - value)
    val newValue = value + (direction * distance)
    val newDirection = Math.signum(target - newValue)
    if (newDirection != direction) {
      // we went past the target
      _value = target
      disable()
    } else {
      _value = newValue
    }
  }
}