package lpf.visual.animator

import java.awt.Color
import java.awt.color.ColorSpace

class TwoColorBlender[T <: ValueAnimator](val animator: T) {

  var colorA: Color = Color.BLACK
  var colorB: Color = Color.WHITE

  def goToA() { animator.target = 0 }
  def goToB() { animator.target = 1 }

  private val cs = ColorSpace.getInstance(ColorSpace.CS_sRGB)

  def color: Color = {
    val componentsA = colorA.getRGBComponents(null)
    val componentsB = colorB.getRGBComponents(null)
    for (i <- 0 until componentsA.length) {
      componentsA(i) = componentsA(i) * (1.0f - animator.value.toFloat) + componentsB(i) * (animator.value.toFloat)
    }
    new Color(componentsA(0), componentsA(1), componentsA(2), componentsA(3))
  }
}