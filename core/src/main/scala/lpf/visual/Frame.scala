/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual
import lpf._
import java.awt.event._
import java.awt._
import java.awt.image.BufferedImage
import _root_.lpf.Graphics
import _root_.lpf.VisibilityBuffer
import _root_.lpf.Visual
import _root_.lpf.event.VisualEventDispatcher
import _root_.lpf.dependency.HornetEventListener
import scala.collection.immutable.List
import lpf.dependency.HornetReadEventDispatcher
import javax.swing.JFrame
import lpf.StrongEvent

/**
 * Wrapper class around JFrame, providing a simple interface
 */
class Frame() {

  val jFrame = new JFrame()
  private val root = JLpfPane()

  jFrame.getContentPane().add(root)

  def setSize(width: Int, height: Int) = jFrame.setSize(width, height)

  val windowClosing = StrongEvent[Unit]()

  // setup window listener
  jFrame.addWindowListener(new WindowAdapter() {
    override def windowClosing(e: WindowEvent): Unit = {
      Frame.this.windowClosing()
    }
  });

  def setVisible(b: Boolean): Unit = {
    println("Frame:setVisible: enter " + b)
    //if (b) jFrame.pack()
    jFrame.setVisible(b)
    println("Frame:setVisible: leave ")
  }

  def setRoot(v: Visual) = root.visual = Some(v)
  def setRoot(v: Option[Visual]) = root.visual = v

}

object Frame {
  def apply() = new Frame()
  def apply(v: Visual) = {
    val result = new Frame()
    result.setRoot(v)
    result
  }

  def apply(data: AnyRef, elementRules: ElementRuleList, visualRules: VisualRuleList): Frame = {
    val elementBuilder = ElementBuilder(elementRules)

   
    val visualBuilder = VisualBuilder(elementBuilder, visualRules)

    val root = visualBuilder.build(elementBuilder.build(data, null), null)
    return apply(root)
  }
}
