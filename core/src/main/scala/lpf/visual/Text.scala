/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.visual
import lpf._
import java.awt.font.FontRenderContext
import java.awt.Font
import java.awt.font.TextLayout

class Text extends Visual {
	type Self=Text
	def self=this
	
	var text: String =""
	val font=new Font("Dialog", Font.PLAIN, 12)
	
	override def measureOverride(availableSize: Size): Size = {
		val frc=GraphicsParametersManager.fontRenderContext
		val bounds=font.getStringBounds(text, frc);
		Size(bounds.getWidth(),bounds.getHeight());
	}

	override def arrangeOverride(actualSize: Size) = {
		// nothing to do
	}
	override def paintOverride(g: Graphics): Unit = {
		g.setFont(font);
		g.drawString(text, Point());
		//g.fillRect(0, 0, measureOverrideResult.width, measureOverrideResult.height)
	}
}

object Text extends VisualObject{
  type Self=Text
  def apply()=new Text()
	
}
