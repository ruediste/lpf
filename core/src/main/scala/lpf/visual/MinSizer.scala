/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual

import lpf._

class MinSizer extends SingleChildVisualLike {
  type Self = MinSizer
  override def self = MinSizer.this

  var width: Option[Double] = None
  var height: Option[Double] = None

  def width_=(d: Double): Unit = width = Some(d)
  def height_=(d: Double): Unit = height = Some(d)

  override def measureOverride(availableSize: Size): Size = {
    val childSize = super.measureOverride(availableSize)

    return Size(childSize.width.max(width.getOrElse(0)), childSize.height.max(height.getOrElse(0)))
  }

}

object MinSizer extends VisualObject {
  type Self = MinSizer
  def apply() = new MinSizer()
}
