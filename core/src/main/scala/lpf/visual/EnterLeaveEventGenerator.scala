/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.visual;
import lpf._
import lpf.event.VisualEventDispatcher
import lpf.event.MouseEvent
import lpf.event.{ MouseEvent => LpfMouseEvent }
import lpf.event.MouseEventType

class EnterLeaveEventGenerator() {
	var currentVisual: Option[Visual] = None

	def setNewVisual(e: java.awt.event.MouseEvent, newVisual: Option[_root_.lpf.Visual]): Unit = {
		//println("setNewVisual " + newVisual)
		if (!currentVisual.equals(newVisual)) {
			val oldPath = currentVisual.toList.flatMap(_.path)
			val newPath = newVisual.toList.flatMap(_.path)

			val (leftVisuals, enteredVisuals) =
				// combine the old and the new path
				oldPath.zipAll(newPath, null, null)
				// drop the common prefix
				.dropWhile(x => x._1 == x._2)
				// separate the remaining paths again
				.unzip(x => x)

			// send leave events
			leftVisuals.takeWhile(_ != null).reverse
				.foreach(v => VisualEventDispatcher.dispatch(new LpfMouseEvent(v, MouseEventType.leaved, e)))

			// send enter events
			enteredVisuals.takeWhile(_ != null)
				.foreach(v => VisualEventDispatcher.dispatch(new LpfMouseEvent(v, _root_.lpf.event.MouseEventType.entered, e)))

			// update current visual
			currentVisual = newVisual
		}
	}
}
