package lpf.visual

import lpf.VisualObject
import lpf.MultiChildrenVisualLike
import lpf.element.Grid
import lpf.Size
import lpf.element.AbsoluteSize
import scala.math.Numeric._
import lpf.Graphics
import lpf.Visual
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import lpf.element.FractionalSize
import lpf.element.AbsoluteSize
import lpf.element.RubberSize
import lpf.Rectangle
import lpf.Point
import lpf.element.GridSize
import lpf.element.RubberSize
import lpf.element.FractionalSize
import lpf.ParentVisualLike

class GridPainter {
  def paintBefore(g: Graphics) : Unit ={}
  def paintAfter(g: Graphics) : Unit ={}
}
class GridVisual extends ParentVisualLike {
  type Self = GridVisual
  def self = this

  class Cell(val content: Visual) {
    var rowSpan = 1
    var columnSpan = 1
  }

  override protected def childrenImp: Iterable[Visual] = cellMap.values.map(_.content)
  override protected def childHasBeenRemovedImpl(removedChild: Visual) {
    cellMap.filter(p => p._2.content == removedChild).toList.foreach(p => cellMap.remove(p._1))
  }

  val columnSizes = ListBuffer[Option[GridSize]]()
  val rowSizes = ListBuffer[Option[GridSize]]()
  val cellMap = HashMap[(Int, Int), Cell]()

  def columnCount = columnSizes.size max (if (cellMap.isEmpty) 0 else cellMap.map(p => p._1._1 + p._2.columnSpan).max)
  def rowCount = rowSizes.size max (if (cellMap.isEmpty) 0 else cellMap.map(p => p._1._2 + p._2.rowSpan).max)

  def cellsInRow(row: Int) = (0 until columnCount).flatMap(cellMap.get(_, row))
  def cellsInColumn(column: Int) = (0 until rowCount).flatMap(cellMap.get(column, _))

  def putCell(c: Int, r: Int, cell: Cell): Option[Cell] = {
    val res = cellMap.put((c, r), cell)
    res.foreach(_.content.removedAsChildOf(this))
    cell.content.addedAsChildOf(this)
    res
  }

  def clearCells() {
    cellMap.foreach(_._2.content.removedAsChildOf(this))
    cellMap.clear()
  }

  def calculateMinSize(
    laneCount: Int,
    laneGridSizes: List[Option[GridSize]],
    measuredSizeConstraints: Map[(Int, Int), Double]): Double = {

    // if there is a fractional size, the size of the grid cannot be measured
    if (laneGridSizes.exists(_ match {
      case Some(FractionalSize(_)) => true
      case _ => false
    })) {
      return 0
    }

    var constraints = measuredSizeConstraints

    // first add the constraints of the lane sizes
    laneGridSizes.zipWithIndex.foreach { p =>
      val key = (p._2, p._2 + 1)
      p._1 match {
        case Some(AbsoluteSize(v)) => constraints += key -> (constraints.getOrElse(key, 0.0) max v)
        case _ => // empty
      }
    }

    var lanePositions = ListBuffer[Double]()
    lanePositions.appendAll((0 to laneCount).map(_ => 0.0))

    // iterate over the lanes to calculate the positions
    (0 to laneCount).foreach { i =>
      val position = lanePositions(i)
      (i to laneCount).foreach { p =>
        constraints.get(i, p).foreach { c =>
          // move the lane position to the right if necessary
          if (lanePositions(p) < position + c) lanePositions(p) = position + c
        }
      }
    }

    lanePositions(laneCount)
  }

  override def measureOverride(availableSize: Size): Size = {
    // measure children
    children.foreach(_.measure(availableSize))

    val measuredSizePair = desiredSizeConstraints()
    var measuredColumnSizes = measuredSizePair._1
    var measuredRowSizes = measuredSizePair._2

    val minWidth = calculateMinSize(columnCount, columnSizes.toList, measuredColumnSizes)
    val minHeight = calculateMinSize(rowCount, rowSizes.toList, measuredRowSizes)

    return Size(minWidth, minHeight)
  }

  /**
   * Lane size (column or row) algorithm is quite complex. It is
   * described for columns, but rows work analogous.
   *
   * First, all constraints between columns are collected.
   * This takes into account the size of the visual,
   * absolute and fractional sizes.
   *
   * Then a shortest path like algorithm is applied to all
   * columns to find the most compact possible layout.
   *
   * The columns are iterated from left to right. For each
   * column, all constraints to the right are evaluated and
   * the positions of the constrained columns are moved to
   * the right if necessary. After one iteration, the layout
   * is computed.
   *
   * Finally, the remaining space is distributed among the
   * rubber size columns.
   */
  def calculateLaneSizes(
    availableSize: Double,
    laneCount: Int, laneGridSizes: List[Option[GridSize]],
    measuredSizeConstraints: Map[(Int, Int), Double]): List[Double] = {
    var constraints = measuredSizeConstraints

    // first add the constraints of the lane sizes
    var rubberSum = 0.0

    laneGridSizes.zipWithIndex.foreach { p =>
      val key = (p._2, p._2 + 1)
      p._1 match {
        case Some(FractionalSize(v)) => constraints += key -> (constraints.getOrElse(key, 0.0) max v * availableSize)
        case Some(AbsoluteSize(v)) => constraints += key -> (constraints.getOrElse(key, 0.0) max v)
        case Some(RubberSize(v)) => rubberSum += v
        case _ => // empty
      }
    }

    var lanePositions = ListBuffer[Double]()
    lanePositions.appendAll((0 to laneCount).map(_ => 0.0))

    // iterate over the lanes to calculate the positions
    (0 to laneCount).foreach { i =>
      val position = lanePositions(i)
      (i to laneCount).foreach { p =>
        constraints.get(i, p).foreach { c =>
          // move the lane position to the right if necessary
          if (lanePositions(p) < position + c) lanePositions(p) = position + c
        }
      }
    }

    // apply rubber requests
    if (rubberSum > 0) {
      val rubberRatio = (availableSize - lanePositions(laneCount)) / rubberSum
      var increment = 0.0
      for (i <- 0 until laneCount) {
        lanePositions(i) += increment
        if (i < laneGridSizes.size) laneGridSizes(i) match {
          case Some(RubberSize(v)) => increment += v * rubberRatio
          case _ => // empty
        }
      }
      lanePositions(laneCount) += increment
    }

    lanePositions.toList
  }


  private def desiredSizeConstraints(): (Map[(Int, Int), Double], Map[(Int, Int), Double]) = {
    var measuredColumnSizes = Map[(Int, Int), Double]()
    var measuredRowSizes = Map[(Int, Int), Double]()

    cellMap.foreach { p =>
      val cell = p._2
      val columnKey = (p._1._1, p._1._1 + cell.columnSpan)
      val rowKey = (p._1._2, p._1._2 + cell.rowSpan)

      measuredColumnSizes += columnKey ->
        (measuredColumnSizes.getOrElse(columnKey, 0.0) max cell.content.desiredSize.width)

      measuredRowSizes += rowKey ->
        (measuredRowSizes.getOrElse(rowKey, 0.0) max cell.content.desiredSize.height)

    }
    return (measuredColumnSizes, measuredRowSizes)
  }

  private var _columnPositions:Option[List[Double]]=None
  def columnPositions=_columnPositions
  private var _rowPositions:Option[List[Double]]=None
  def rowPositions=_rowPositions
  
  override def arrangeOverride(actualSize: Size) {
    val measuredSizePair = desiredSizeConstraints()
    var measuredColumnSizes = measuredSizePair._1
    var measuredRowSizes = measuredSizePair._2

    val columnPositions = calculateLaneSizes(actualSize.width, columnCount, columnSizes.toList, measuredColumnSizes)
    val rowPositions = calculateLaneSizes(actualSize.height, rowCount, rowSizes.toList, measuredRowSizes)

    _columnPositions=Some(columnPositions)
    _rowPositions=Some(rowPositions)
    
    cellMap.foreach { p =>
      val column = p._1._1
      val row = p._1._2
      val cell = p._2

      val columnPosition = columnPositions(column)
      val rowPosition = rowPositions(row)

      val columnSize = columnPositions(column + cell.columnSpan) - columnPosition
      val rowSize = rowPositions(row + cell.rowSpan) - rowPosition

      cell.content.arrange(Rectangle(Point(columnPosition, rowPosition), Size(columnSize, rowSize)))
    }
  }

  def getRowIndex(y: Double): Option[Int] =
  {
    _rowPositions.flatMap{v=>
      val index=v.filter(_<y).length-1
      if (index>0) Some(index) else None
    }
  }
  
  var painter: Option[GridPainter] =None
  def painter_=(p: GridPainter): Unit = painter=Some(p)
  
  override def paintOverride(g: Graphics): Unit = {
    painter.foreach(_.paintBefore(g))
    cellMap.values.foreach(_.content.paint(g))
    painter.foreach(_.paintAfter(g))
  }
}

object GridVisual extends VisualObject {
  type Self = GridVisual
  def apply() = new GridVisual()
}
