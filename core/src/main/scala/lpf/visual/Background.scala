/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual
import lpf.Graphics
import lpf.VisualObject
import java.awt.Color

class Background() extends SingleChildVisualLike {
  override type Self = Background
  override def self = this

  var color = Background.transparent

  override def paintOverride(g: Graphics): Unit = {
    g.setColor(color)
    g.fillRect(0, 0, actualSize.width, actualSize.height)

    super.paintOverride(g)
  }
}

object Background extends VisualObject {
  private val transparent = new Color(0, 0, 0, 0)
  type Self = Background
  override def apply() = new Background()
}
