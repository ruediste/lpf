/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual;

import lpf._
import java.awt.EventQueue

/**
 * Helper class to work with the Awt Event queue
 *
 * The LPF requires a prioritized handling of certain tasks:
 * - if any bindings are outdated, update them
 * - if no bindings are pending, perform the layout (if pending)
 * - finally do the paint (if pending)
 *
 * While performing these tasks, the AWT events should still be processed.
 *
 * This is achieved by putting an ActiveEvent into the queue. Whenever the
 * event is processed, the next pending task is performed. If there are more
 * pending tasks, the event is put into the AWT queue again.
 */
class AwtQueueHelper() extends Referencer {
  private val lock = new Object()
  @unobserved private var processNextQueued = false
  @unobserved private var pendingLayouts = List[() => Unit]()
  @unobserved private var pendingPaints = List[() => Unit]()
  @unobserved private var animationPending = false

  /**
   * Put a call to [[processNext()]] into the AWT event queue if it is not there already
   */
  private def queueProcessNext() {
    // atomically check processNextQueued, set it to true if it was false
    if (lock.synchronized {
      if (!processNextQueued) {
        processNextQueued = true
        true
      } else false
    }) {
      // if processNext was not in the queue, put it there
      EventQueue.invokeLater(new Runnable { def run = processNext() })
    }
  }

  /**
   * queue a call to doLayout
   */
  def queueLayout(f: () => Unit) {
    lock.synchronized(pendingLayouts = f :: pendingLayouts)
    queueProcessNext()
  }

  //def isLayoutPending = layoutPending

  /**
   * queue a call to doPaint
   */
  def queuePaint(f: () => Unit) {
    lock.synchronized(pendingPaints = f :: pendingPaints)
    queueProcessNext()
  }

  def queueAnimation() {
    lock.synchronized(animationPending = true)
    queueProcessNext
  }

  /**
   * process the next pending task
   */
  def processNext() {
    lock.synchronized(processNextQueued = false)

    def inner() {
      // check if an animation is pending
      if (lock.synchronized {
        val res = animationPending
        animationPending = false
        res
      }) {
        Animator.processFrame()
        return
      }

      // try to update a binding 
      if (Application.bindingDispatcher.tryExecuteSingle()) {
        // we did a task, so return
        return
      }

      // check if a layout is pending
      if (performLayoutIfPending()) return

      // check if a paint is pending
      lock.synchronized {
        val headOption = pendingPaints.headOption
        headOption.foreach(_ => pendingPaints = pendingPaints.tail)
        headOption
      }.foreach { doPaint =>
        doPaint()
        return
      }
    }

    // do the processing
    try {
      inner()
    } catch {
      case t: Throwable => t.printStackTrace()
    } finally {

      // put the event into the AWT queue again if necessary
      if (Application.bindingDispatcher.tasksPending ||
        lock.synchronized { (!pendingPaints.isEmpty) || (!pendingLayouts.isEmpty) || animationPending})
        queueProcessNext()
    }
  }

  private def performLayoutIfPending(): Boolean = {
    // check if a layout is pending
    lock.synchronized {
      val headOption = pendingLayouts.headOption
      headOption.foreach(_ => pendingLayouts = pendingLayouts.tail)
      headOption
    }.foreach { doLayout =>
      doLayout()
      return true
    }

    return false
  }

  // whenever a binding is added to the queue, make sure a processNext() call gets queued
  Application.bindingDispatcher.taskQueued += queueProcessNext()

  // do initial queuing
  queueProcessNext()
}
