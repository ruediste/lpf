/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual
import lpf._

class Margin extends SingleChildVisualLike with VisualWithThickness[Margin] {
  override type Self = Margin
  override def self = this

  override def measureOverride(availableSize: Size): Size = {
    super.measureOverride(availableSize) + (left + right, top + bottom)
  }

  override def arrangeOverride(actualSize: Size): Unit = {
   child.foreach(_.arrange(Rectangle(
      Point(left, top),
      actualSize - (left + right, top + bottom))))
  }
}

object Margin extends VisualObject {
  type Self = Margin
  override def apply() = new Margin()
}
