package lpf.visual

import javax.swing.JComponent
import lpf.Visual

class JLpfPane extends JLpfRootLike {
  def myPaintChildren(g: java.awt.Graphics): Unit = paintChildren(g)
}

object JLpfPane {
  def apply() = new JLpfPane()

  def apply(v: Visual) = {
    val result = new JLpfPane()
    result.visual = Some(v)
    result
  }
}