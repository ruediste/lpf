package lpf.visual

import lpf.Application
import java.util.Timer
import java.util.TimerTask
import lpf.util.MapMaker
import lpf.unobserved
import lpf.unobserved

abstract class Animator {

  protected def frame(elapsedTime: Double): Unit

  @unobserved private var _enabled = false
  def enabled=_enabled

  protected def enable() {
    Animator.animators.add(this)
    _enabled = true
  }

  protected def disable() {
    Animator.animators.remove(this)
    _enabled = false
  }
}

object Animator {
  private val animators = MapMaker().weakKeys.makeSet[Animator]()

  @unobserved private var lastFrameTime = System.currentTimeMillis()

  def processFrame() {
    val time = System.currentTimeMillis()
    val delta = time - lastFrameTime
    lastFrameTime = time

    animators.foreach(_.frame(delta.toDouble / 1000.0))
  }

  val timer = new Timer()
  timer.schedule(new TimerTask() {
    def run() {
      Application.awtQueueHelper.queueAnimation()
    }
  }, 0, 1000 / 60)

}