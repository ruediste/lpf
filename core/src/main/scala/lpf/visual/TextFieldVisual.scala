package lpf.visual

import lpf.VisualObject
import lpf.element.EditableString
import lpf.Graphics
import lpf.FunctionHandle
import java.awt.font.TextLayout
import java.awt.font.FontRenderContext
import java.awt.Graphics2D
import java.awt.Font
import lpf.Size
import lpf.Visual
import java.awt.Color
import lpf.Point
import lpf.dependency.Cache

class TextFieldVisual() extends Visual {
  type Self = TextFieldVisual
  def self = this

  val margin = 5

  private var _text: EditableString = null

  def text = _text
  def text_=(t: EditableString) = {
    if (_text != null)
      throw new Error("cannot change the text once it is set")

    _text = t
  }

  private var _layout: Option[TextLayout] = None

  private val frc = new FontRenderContext(null, true, true);
  private val layoutCache = Cache(new TextLayout(text.value, font, frc))
  
  def layout = layoutCache.value

  private val font = new Font("Dialog", Font.PLAIN, 12)

  override def paintOverride(g: Graphics) {
    if (text.value.length() == 0)
      return

    def inner(g: Graphics2D, setColor: Boolean) {
      // create the layout

      g.translate(margin, margin+layout.getAscent())

      // draw the selection
      text.selection.foreach(sel => {
        val selectionColor = Color.LIGHT_GRAY
        val selectionShape = layout.getLogicalHighlightShape(sel._1, sel._2);
        // selection may consist of disjoint areas
        if (setColor) g.setColor(selectionColor);
        g.fill(selectionShape);
      })

      // draw the text
      if (setColor) g.setColor(Color.BLACK)
      layout.draw(g, 0, 0)

      // draw the caret
      text.caretPosition.foreach(pos => {
        val shapes = layout.getCaretShapes(pos)
        if (setColor) g.setColor(Color.RED)
        shapes.filter(_ != null).foreach(g.draw(_))
      })
    }

    g.setFont(font)
    inner(g.graphics, true)
    g.visGraphics.foreach(inner(_, false))
  }

  override def measureOverride(availableSize: Size): Size = {
    val maxBounds = font.getMaxCharBounds(frc);
    val width = if (text.value.length() == 0) 0.0 else layout.getBounds().getWidth()
    Size(width + 2 * margin, maxBounds.getHeight() + 2 * margin);
  }

  def getCharIndex(p: Point): Int = {
    if (text.value.length()==0) 
      return 0
      
    val x = (p.x - margin).toFloat
    val y = (p.y - margin).toFloat
    layout.hitTestChar(x, y).getInsertionIndex()
  }

  override def arrangeOverride(actualSize: Size) = {
    // nothing to do
  }
}

object TextFieldVisual extends VisualObject {
  type Self = TextFieldVisual
  def apply() = new TextFieldVisual()
}