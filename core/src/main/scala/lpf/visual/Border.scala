/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visual
import java.awt.Color
import lpf._
import java.awt.BasicStroke

class Border() extends SingleChildVisualLike with VisualWithThickness[Border] {
  override type Self = Border
  override def self = this

  var color: Color = Color.BLACK

  override def measureOverride(availableSize: Size): Size = {
    super.measureOverride(availableSize) + Size(left + right, top + bottom)
  }

  override def arrangeOverride(actualSize: Size): Unit = {
    child.foreach(_.arrange(Rectangle(
      Point(left, top),
      actualSize - (left + right, top + bottom))))
  }

  override def paintOverride(g: Graphics): Unit = {
    g.setColor(color)

    // top side
    g.setStroke(new BasicStroke(top.toFloat))
    g.drawLine(
      left / 2,
      top / 2,
      actualSize.width - right / 2,
      0 + top / 2);

    // bottom side
    g.setStroke(new BasicStroke(bottom.toFloat))
    g.drawLine(
      left / 2,
      actualSize.height - bottom / 2,
      actualSize.width - right / 2,
      actualSize.height - bottom / 2);

    // left side
    g.setStroke(new BasicStroke(left.toFloat))
    g.drawLine(
      left / 2,
      top / 2,
      left / 2,
      actualSize.height - bottom / 2);

    // right side
    g.setStroke(new BasicStroke(right.toFloat))
    g.drawLine(
      actualSize.width - left / 2,
      top / 2,
      actualSize.width - left / 2,
      actualSize.height - bottom / 2);

    super.paintOverride(g)
  }
}

object Border extends VisualObject {
  type Self = Border
  override def apply() = new Border()
}
