/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.visual

trait VisualWithThickness[T] {self: T =>
	
	var top = 1.0
	var bottom = 1.0
	var left = 1.0
	var right = 1.0
	
	def thickness = {
		if (top == bottom && top == left && top == right)
			top
		else
			throw new Error("multiple border thicknesses found")
	}

	def thickness_=(d: Double):T  = {
		top = d
		bottom = d
		left = d
		right = d
		this
	}
}
