/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf
import scala.collection.mutable.Set

class StrongEvent[A] extends EventBase[A]{
	def +=(f: (A) => Unit) = addStrong(f)
	def +=(f: =>Unit) =addStrong{(arg: A)=>f}
	
	def -=(handle: FunctionHandle) = removeStrong(handle)
}

object StrongEvent{
	def apply[A]() = new StrongEvent[A]()
}

class StrongEventNoArgs extends EventBaseNoArgs(WeakEvent()) {
	def +=(f: =>Unit) = addStrong(f)
	def -=(handle: FunctionHandle)=removeStrong(handle)
}

object StrongEventNoArgs {
	def apply() = new StrongEventNoArgs()
}
