/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import scala.collection.mutable._
import scala.collection.immutable.List
import scala.collection.LinearSeq
import scala.collection.Seq
import scala.collection.Set
import scala.collection.Iterable
import scala.reflect.ClassTag

/**
 * Base trait for Tree Nodes
 *
 * TNode: Type all nodes in the tree have to be derived from
 * ParentNode: Base Type of the nodes which can have children
 */
abstract trait TreeNode[TNode <: TreeNode[TNode, ParentNode], ParentNode <: ParentTreeNodeLike[TNode, ParentNode]] { this: TNode =>
  /**
   * The actual type of the node
   */
  protected type Self

  /**
   * abstract method which returns this
   */
  protected def self: Self

  private var _parent: Option[ParentNode] = None

  /**
   * get the parent
   */
  def parent = _parent

  /**
   * get all children
   */
  final def children: Iterable[TNode] = childrenImp.view
  protected def childrenImp: Iterable[TNode] = List()

  /**
   * set the parent, without notifying the old parent or the new parent
   */
  def setParentOneWay(newParent: Option[ParentNode]): Unit = _parent = newParent

  /**
   * to be called when the node was added as child of a newParent
   */
  def addedAsChildOf(newParent: ParentNode): Unit = {
    // remove this node from the old parent
    parent.foreach(_.childHasBeenRemoved(this))

    // update the parent link
    _parent = Some(newParent)
  }

  /**
   * to be called when the node was removed from a parent
   */
  def removedAsChildOf(oldParent: ParentNode): Unit = {
    // clear the parent link
    _parent = None
  }

  /**
   * return this followed by all ancestors
   */
  def ancestors: LinearSeq[TNode] = {
    if (parent.isDefined)
      this.asInstanceOf[TNode] +: parent.get.ancestors
    else
      List(this)

  }

  /**
   * Return all children and subChildren. The order of the children is defined.
   * The iterable begins with the direct children of the node, and continues with
   * the transitive children of each child
   */
  def transitiveChildren: Iterable[TNode] = {
    children ++ children.flatMap(child => child.transitiveChildren)
  }

  /**
   * Return all visuals in the subtree starting at this node.
   */
  def subTree: List[TNode] = List[TNode](this) ++ transitiveChildren

  def subTreePre: List[TNode] = List[TNode](this) ++ children.flatMap(_.subTreePre)

  /**
   * return the path from the root, including this node
   */
  def path: Seq[TNode] = ancestors.reverse

  override def toString(): String = {
    super.toString() //+"{"+getChildren.mkString(",")+"}"
  }

  private def classTag[T](implicit tag: ClassTag[T]) = tag

  /**
   * get all children of the given type
   */
  def \*[T <: TNode: ClassTag]: Iterable[T] = children.filter(child => classTag[T].runtimeClass.isAssignableFrom(child.getClass())).map(_.asInstanceOf[T])

  /**
   * get the single child of a given type
   */
  def \[T <: TNode: ClassTag]: T = this.\*[T].head

  /**
   * get all nodes in the subtree which are of a given type
   */
  def \\*[T <: TNode: ClassTag]: Iterable[T] =
    subTree.filter(child => classTag[T].runtimeClass.isAssignableFrom(child.getClass())).map(_.asInstanceOf[T])

  /**
   * get the first node in the subtree which is of a given type
   */
  def \\[T <: TNode: ClassTag]: T = {
    val children = this.\\*[T]
    if (children.isEmpty)
      throw new Error("No children of type " + classTag[T].runtimeClass.getName())
    children.head
  }

}

/**
 * Trait Containing the implementation for TreeNodes which can have children
 */
abstract trait ParentTreeNodeLike[TNode <: TreeNode[TNode, ParentNode], ParentNode <: ParentTreeNodeLike[TNode, ParentNode]]
  extends TreeNode[TNode, ParentNode] { this: TNode with ParentNode =>
  type Self <: ParentTreeNodeLike[TNode, ParentNode]
  def childHasBeenRemoved(removedChild: TNode): Unit = childHasBeenRemovedImpl(removedChild)
  protected def childHasBeenRemovedImpl(removedChild: TNode)
}

/**
 * Trait fixing the type of the ParentNodes to ParentTreeNodeTrait
 */
abstract trait ParentTreeNode[TNode <: TreeNode[TNode, ParentTreeNode[TNode]]] extends ParentTreeNodeLike[TNode, ParentTreeNode[TNode]] { this: TNode => }

/**
 * Node with a single child
 */
abstract trait SingleChildNodeLike[TNode <: TreeNode[TNode, ParentNode], ParentNode <: ParentTreeNodeLike[TNode, ParentNode]]
  extends ParentTreeNodeLike[TNode, ParentNode] { this: TNode with ParentNode =>
  private var childOption: Option[TNode] = None
  def child = childOption

  def child_=(newChild: Option[TNode]): Unit = {
    //println("setChild this:" + SingleChildNodeLike.this + " child:" + newChild)
    childOption.foreach { (_.removedAsChildOf(SingleChildNodeLike.this)) }
    childOption = newChild
    childOption.foreach { (_.addedAsChildOf(SingleChildNodeLike.this)) }
  }

  def child_=(newChild: TNode): Unit = child = Some(newChild)

  override def childHasBeenRemovedImpl(removedChild: TNode): Unit = {
    childOption = None
  }

  override def childrenImp = if (childOption == null) None else childOption
}

/**
 * node with multiple children
 */
abstract trait MultiChildrenNodeLike[TNode <: TreeNode[TNode, ParentNode], ParentNode <: ParentTreeNodeLike[TNode, ParentNode]] extends ParentTreeNodeLike[TNode, ParentNode] { this: TNode with ParentNode =>
  protected val _children = LinkedHashSet[TNode]()

  /**
   * add a child, keeping the tree structure intact
   */
  def addChild(child: TNode): Unit = {
    //println("addedChild this:" + MultiChildrenNodeLike.this + " child:" + child)
    // add to the list of children
    _children += child

    // inform the child of the modified parent
    child.addedAsChildOf(MultiChildrenNodeLike.this)
  }

  def addChildren(newChildren: TNode*): Self = {
    newChildren foreach addChild
    self
  }

  override def childrenImp = _children

  /**
   * remove a child, keeping the tree structure intact
   */
  def removeChild(child: TNode): Unit = {
    // remove from children
    _children.remove(child);

    // inform the child of the changed parent
    child.removedAsChildOf(MultiChildrenNodeLike.this)
  }

  def removeAllChildren() = {
    _children.clone().foreach(removeChild(_))
  }

  def replaceChildren(newChildren: TraversableOnce[TNode]): Self = {
    removeAllChildren()
    newChildren foreach addChild
    self
  }

  override def childHasBeenRemovedImpl(removedChild: TNode): Unit = {
    _children -= removedChild
  }
}
