package lpf.hornet

import lpf.instrumentation._
import org.objectweb.asm._
import lpf.instrumentation.JoinPointKind._
import java.lang.reflect.InvocationTargetException

class HornetAdviceProvider extends AdviceProviderBase {
  private val readDispatcherName = "lpf.dependency.HornetReadEventDispatcher"
  private val writeDispatcherName = "lpf.dependency.HornetWrittenEventDispatcher"
  private val hornetEventEnablerName = "lpf.rt.ObjectHornetEventEnabler"

  override def needsInstrumentation(loader: ClassLoader, className: String, bytes: Array[Byte]): Option[Boolean] = {
    if (bytes != null)
      ClassInformationPool.getClassInformation(className, Some(bytes))
    Some(findPackageAdviceProvider(getClass().getClassLoader(), className)
      .flatMap(_.needsInstrumentation(loader, className, bytes))
      .getOrElse { ClassInformationPool.getClassInformation(className).needsInstrumentation })
  }

  override def getAdvice(jp: JoinPoint): Option[Advice] = {
    // try to get the advice from a package advice provider
    findPackageAdviceProvider(jp.location).flatMap(_.getAdvice(jp))
      // otherwise check field accesses
      .orElse(getFieldAccessAdvice(jp))
      // otherwise check array access
      .orElse(getArrayAccessAdvice(jp))
      .orElse(getFactoryMethodAdvice(jp))
      .orElse(getConstructorCallAdvice(jp))
  }

  def getConstructorCallAdvice(jp: JoinPoint): Option[Advice] = {
    if (jp.isInvokeSpecial && jp.member.memberName.equals("<init>") && jp.kind == AfterCall) {
      // check if the location instantiates object hornets
      if (!ClassInformationPool.getClassInformation(jp.location.className).instantiatesObjectHornets)
        return None

      val info = ClassInformationPool.getClassInformation(jp.member.className)
      if (info.isObjectHornet)
        return Some(InvokeStaticAdvice(hornetEventEnablerName, "enableHornetEventsVoid", OriginalArgumentMethodArgument(0)))
    }
    return None
  }

  def getFactoryMethodAdvice(jp: JoinPoint): Option[Advice] = {
    if (jp.kind != AfterCall)
      return None
    if (!ClassInformationPool.getClassInformation(jp.location.className).instantiatesObjectHornets)
      return None
    val info = ClassInformationPool.getClassInformation(jp.member.className)
    if (info.getMethods(jp.member.memberName, jp.member.desc).exists(_.isObjectHornetFactory))
      return Some(InvokeStaticAdviceDesc(hornetEventEnablerName, "enableHornetEvents", "(Ljava/lang/Object;)Ljava/lang/Object;", Some(Type.getMethodType(jp.member.desc).getReturnType())))
    return None
  }

  def getFieldAccessAdvice(jp: JoinPoint): Option[Advice] = {
    // check if the advice is handled by this method
    if (!List(BeforeGetField, AfterSetField).exists(_ == jp.kind)) return None

    val memberClassInfo = ClassInformationPool.getClassInformation(jp.member.className)

    // check if the class of the accessed field uses field hornets
    if (!memberClassInfo.usesFieldHornets) return None

    val fieldInfoT = memberClassInfo.getField(jp.member.memberName)
    if (fieldInfoT.isEmpty)
      println("getFieldAccessAdvice for " + jp.member.className + "::" + jp.member.memberName + " " + memberClassInfo)
    val fieldInfo = fieldInfoT.get

    // check if the field is unObserved
    if (fieldInfo.isUnobserved) return None

    // skip final fields
    if (fieldInfo.isFinal) return None

    //val methodInfos=memberClassInfo.getMethods.filter
    return jp.kind match {
      case BeforeGetField if (!jp.member.isStatic) =>
        Some(InvokeStaticAdvice(readDispatcherName, "hornetWillBeRead", OriginalArgumentMethodArgument(0), MemberNameMethodArgument()))
      case BeforeGetField if (jp.member.isStatic) =>
        Some(InvokeStaticAdvice(readDispatcherName, "hornetWillBeRead", MemberClassMethodArgument(), MemberNameMethodArgument()))
      case AfterSetField if (!jp.member.isStatic) =>
        Some(InvokeStaticAdvice(writeDispatcherName, "hornetWritten", OriginalArgumentMethodArgument(0), MemberNameMethodArgument()))
      case AfterSetField if (jp.member.isStatic) =>
        Some(InvokeStaticAdvice(writeDispatcherName, "hornetWritten", MemberClassMethodArgument(), MemberNameMethodArgument()))
    }
  }

  def getArrayAccessAdvice(jp: JoinPoint): Option[Advice] = {
    // check if the advice is handled by this method
    if (!List(BeforeArrayRead, AfterArrayWrite).exists(_ == jp.kind)) return None

    val info = ClassInformationPool.getClassInformation(jp.location.className)

    // check if the class the array is accessed in uses field hornets
    if (!info.usesFieldHornets) return None

    return jp.kind match {
      case BeforeArrayRead if (!jp.member.isStatic) =>
        Some(InvokeStaticAdvice(readDispatcherName, "hornetWillBeRead", OriginalArgumentMethodArgument(0)))
      case AfterArrayWrite if (!jp.member.isStatic) =>
        Some(InvokeStaticAdvice(writeDispatcherName, "hornetWritten", OriginalArgumentMethodArgument(0)))
      case _ => None
    }
  }

}

object HornetAdviceProvider {
}