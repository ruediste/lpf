/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf

import language.implicitConversions
import lpf.visual.SingleChildVisualLike
import lpf.binding.OneWayListBinding
import lpf.binding.OneWayBinding
import lpf.binding.OneWayCachingListBinding
import lpf.binding.ListBindingEndPoint
import lpf.visual.listener.ListenerBase

trait BuilderDSL {
  class TreeNodeWrapper[TNode <: TreeNode[TNode, ParentNode], ParentNode <: ParentTreeNodeLike[TNode, ParentNode], T <: TreeNode[TNode, ParentNode]](val node: T) {
    /**
     * Check if the parent satisfies a predicate. Allows stuff like
     */
    def hasParent(p: ParentNode => Boolean) = node.parent.exists(p)

    def apply(func: T => Unit): T = {
      func(node)
      node
    }
  }

  implicit def toVisualToTreeNodeWrapper[T <: Visual](node: T) = new TreeNodeWrapper[Visual, ParentVisualLike, T](node)

  class ListenerWrapper[T <: ListenerBase](listener: T) {
    def apply(func: T => Unit): T = {
      func(listener)
      listener
    }
  }

  implicit def toListenerWrapper[T <: ListenerBase](listener: T) = new ListenerWrapper[T](listener)

  implicit def toElementToTreeNodeWrapper[T <: Element](node: T) = new TreeNodeWrapper[Element, ParentElementLike, T](node)

  class MultiChildrenElementLikeWrapper[T <: MultiChildrenElementLike](val group: T) {
    def /(func: => Unit): T = {
      val elements = new scala.collection.mutable.ListBuffer[Element]()
      Element.observeInstantiations(elements += _, func)
      elements filter (_.parent.isEmpty) foreach { group addChild _ }
      group
    }
  }
  implicit def toMultiChildrenElementLikeWrapper[T <: MultiChildrenElementLike](group: T) = new MultiChildrenElementLikeWrapper[T](group)

  class MultiChildrenVisualLikeWrapper[T <: MultiChildrenVisualLike](val group: T) {
    def /(func: => Unit): T = {
      val visuals = new scala.collection.mutable.ListBuffer[Visual]()
      Visual.observeInstantiations(visuals += _, func)
      visuals filter (_.parent.isEmpty) foreach { group addChild _ }
      group
    }
  }
  implicit def toMultiChildrenVisualLikeWrapper[T <: MultiChildrenVisualLike](group: T) = new MultiChildrenVisualLikeWrapper[T](group)

  class VisualWrapper[T <: Visual](val visual: T) {
    import visual._
    def -:[A <: SingleChildVisualLike](parentVisual: A): A = { parentVisual.child = visual; parentVisual }

    def bind(func: T => Unit, showDependencies: Boolean = false)(implicit theVisualBuilder: VisualBuilder): T = {
      OneWayBinding(theVisualBuilder.dispatcher, showDependencies)(func(visual))(visual)
      visual
    }
  }

  implicit def toVisualWrapper[T <: Visual](a: T) = new VisualWrapper(a)

  class ElementWrapper[T <: Element](val element: T) {
    import visual._
    def -:[A <: SingleChildVisualLike](parentVisual: A)(implicit theVisualBuilder: VisualBuilder): A = {
      val tmp = theVisualBuilder.build(element, parentVisual);
      parentVisual.child = tmp;
      parentVisual
    }

    def -:[A <: SingleChildElementLike](parentElement: A): A = {
      parentElement.child = element;
      parentElement
    }

    def bind(func: T => Unit, showDependencies: Boolean = false)(implicit theElementBuilder: ElementBuilder): T = {
      OneWayBinding(theElementBuilder.dispatcher, showDependencies)(func(element))(element)
      element
    }
  }
  implicit def toElementWrapper[T <: Element](element: T) = new ElementWrapper(element)

  class TupleWrapper(val tuple: Tuple2[AnyRef, _root_.lpf.Element]) {
    import _root_.lpf.visual._

    def -:[A <: SingleChildVisualLike](parentVisual: A)(implicit theVisualBuilder: VisualBuilder, theElementBuilder: ElementBuilder): A = {
      parentVisual -: theElementBuilder.build(tuple._1, tuple._2)
    }

    def -:[A <: SingleChildElementLike](parentElement: A)(implicit theElementBuilder: ElementBuilder): A = {
      parentElement -: theElementBuilder.build(tuple._1, tuple._2)
    }
  }

  // disabled for the moment
  //implicit 
  def toTupleWrapper(tuple: Tuple2[AnyRef, _root_.lpf.Element]) = new TupleWrapper(tuple)

  class ElementSourceWrapper(val source: () => Element) {

    def ~:[A <: SingleChildVisualLike](parentVisual: A)(implicit theVisualBuilder: VisualBuilder): A = {
      OneWayBinding(theVisualBuilder.dispatcher)(parentVisual.child = theVisualBuilder.build(source(), parentVisual))(parentVisual)
      parentVisual
    }
  }

  implicit def toElementSourceWrapper(source: => Element) = new ElementSourceWrapper(() => source)

  class ElementIterableSourceWrapper(val source: () => Iterable[Element]) {

    def ~:[A <: MultiChildrenVisualLike](parentVisual: A)(implicit theVisualBuilder: VisualBuilder): A = {
      new OneWayCachingListBinding[Element, Visual](
        theVisualBuilder.dispatcher,
        ListBindingEndPoint.readOnly(source),
        theVisualBuilder.build(_, parentVisual),
        ListBindingEndPoint.writeOnly(parentVisual.replaceChildren(_)), parentVisual)
      parentVisual
    }
  }

  implicit def toElementIterableSourceWrapper(source: => Iterable[Element]) = new ElementIterableSourceWrapper(() => source)

  class ElementOptionSourceWrapper(val source: () => Option[Element]) {

    def ~:[A <: SingleChildVisualLike](parentVisual: A)(implicit theVisualBuilder: VisualBuilder): A = {
      OneWayBinding(theVisualBuilder.dispatcher)(parentVisual.child = source() map (theVisualBuilder.build(_, parentVisual)))(parentVisual)
      parentVisual
    }
  }

  implicit def toElementOptionSourceWrapper(source: => Option[Element]) = new ElementOptionSourceWrapper(() => source)

  class SourceWrapper(val source: () => AnyRef) {

    def ~:[A <: SingleChildElementLike](parentElement: A)(implicit theElementBuilder: ElementBuilder): A = {
      OneWayBinding(theElementBuilder.dispatcher)(parentElement.child = theElementBuilder.build(source(), parentElement))(parentElement)
      parentElement
    }

  }

  implicit def toSourceWrapper(source: => AnyRef) = new SourceWrapper(() => source)

}
