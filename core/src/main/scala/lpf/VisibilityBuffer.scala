/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import scala.collection.mutable.ListBuffer
import java.awt.Color
import java.awt.AlphaComposite
import lpf.util.MapMaker

class VisibilityBuffer(val width: Int, val height: Int) {

  val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)

  def graphics(v: Visual) = {
    if (!VisibilityBuffer.isHitTestVisible(v))
      None
    else {
      ids += v
      val color = new Color(ids.size, true)
      //printf("new visual with id %d %x\n", ids.size, color.getRGB())
      val g = image.createGraphics()
      g.setComposite(AlphaComposite.Src)
      g.setColor(color)
      Some(g)
    }
  }

  def dispose(): Unit = {
    image.flush()
  }

  /**
   * look up the image and the id list to find the visual at the specified position
   */
  def getVisual(x: Int, y: Int): Option[Visual] = {
    if (x < 0 || y < 0 || x >= image.getWidth() || y >= image.getHeight())
      return None
    val id = image.getRGB(x, y) - 1
    if (id < 0 || id >= ids.size) None
    else
      Some(ids(id))
  }
  var ids = ListBuffer[Visual]()

  def reset(): Unit = {
    ids.clear()
    val g = image.createGraphics()
    g.setColor(new Color(0, true));
    g.fillRect(0, 0, width, height)
  }

  // do an initial reset
  reset()
}

object VisibilityBuffer {
  val hitTestVisibilityMap = MapMaker().weakKeys().makeMap[Visual, Boolean]()

  def isHitTestVisible(v: Visual) = hitTestVisibilityMap.getOrElse(v, true)

  def setHitTestVisibility(v: Visual, value: Boolean) = if (value) hitTestVisibilityMap.remove(v) else hitTestVisibilityMap.put(v, false)
}
