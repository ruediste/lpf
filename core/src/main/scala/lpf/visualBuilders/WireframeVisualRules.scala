/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.visualBuilders
import lpf._
import _root_.lpf.visual._
import _root_.lpf.element._
import java.awt.Color
import listener.MouseOverListener
import lpf.visual.listener.ButtonLikeClickListener
import lpf.visual.listener.ButtonLikeClickListener
import lpf.visual.listener.MouseOverListener
import lpf.visual.listener.ClickListener
import javax.swing.PopupFactory
import javax.swing.JLabel
import javax.swing.SwingUtilities
import javax.swing.JWindow
import java.awt.event.WindowFocusListener
import java.awt.BorderLayout
import javax.swing.JTextField
import javax.swing.JPopupMenu
import lpf.event.EventModifier
import lpf.binding.OneWayListBinding
import java.awt.geom.AffineTransform
import lpf.visual.listener.MousePositionListener
import lpf.visual.listener.SelectionListener
import lpf.event.KeyEventDispatcher
import lpf.event.KeyEventType
import lpf.visual.listener.TextEnterListener
import lpf.visual.listener.TextMoveListener
import lpf.event.KeyboardFocusManager
import lpf.visual.listener.KeyboardFocusTraversalListener
import lpf.binding.Mapping


/**
 * A set of visually simple rules to generate visuals from elements
 */
object WireframeVisualRules extends VisualRuleList {

  
  rule2 {
    case (e: Element, g: Grid#Cell) => RubberMargin() -: continue()
  }

  rule {
    case g: Grid =>
      val mapping = new Mapping[Element, Visual]()
      GridVisual().bind { visual =>
        visual.columnSizes.clear()
        visual.columnSizes.appendAll(g.columnSizes)

        visual.rowSizes.clear()
        visual.rowSizes.appendAll(g.rowSizes)

        visual.clearCells()
        mapping.reDefine { m =>
          g.cellMap.foreach { p =>
            val element = p._2.content
            val cell = new visual.Cell(m.getOrElseDefine(element, build(element, p._2)))
            cell.columnSpan = p._2.columnSpan
            cell.rowSpan = p._2.rowSpan
            visual.putCell(p._1._1, p._1._2, cell)
          }
        }
      }
  }

  rule {
    case g: DataGridSingle[_] =>
      val mapping = new Mapping[Element, Visual]()
      val listener = ClickListener()
      var gridVisual: GridVisual = null
      var data: Option[List[List[Element]]] = None
      Background(listener.target = _)(_.color = Color.WHITE) -: GridVisual(gridVisual = _)(_.painter = new GridPainter {
        override def paintBefore(g2: Graphics) {
          // paint a blue background for the selected row
          g.selectedRow.foreach { row =>
            val index = g.rows.indexOf(row) + 1 // header row
            gridVisual.rowPositions
              .filter(_.length > index + 1) // check if the index is in bounds
              .foreach { pos =>
                g2.setColor(Color.BLUE)
                g2.fillRect(0, pos(index), gridVisual.actualSize.width, pos(index + 1) - pos(index))
              }
          }
        }
      }).bind { visual =>
        visual.columnSizes.clear()
        visual.columnSizes.appendAll(g.columns.map(_.columnSize))

        visual.clearCells()
        mapping.reDefine { m =>
          var column = 0
          var row = 0

          // create header row
          g.columns.foreach { c =>
            visual.putCell(column, row, new visual.Cell(m.getOrElseDefine(c.header, build(c.header, visual))))
            column += 1
          }

          column = 0
          row += 1

          // create rows for data
          data = Some(g.rows)

          g.rows.foreach { r =>
            r.foreach { e =>
              visual.putCell(column, row, new visual.Cell(m.getOrElseDefine(e, build(e, visual))))
              column += 1
            }
            column = 0
            row += 1
          }
        }
      }

      // make click select a row
      listener.clicked += { e =>
        gridVisual
          .getRowIndex(e.point.y) // get the row index
          .map(_ - 1) // header row
          .filter(_ >= 0) // check if still in bounds
          .foreach(g.trySelectRow(_))
      }
  }

  rule {
    case g: DataGrid[_] =>
      val mapping = new Mapping[Element, Visual]()
      GridVisual().bind { visual =>
        visual.columnSizes.clear()
        visual.columnSizes.appendAll(g.columns.map(_.columnSize))

        visual.clearCells()
        mapping.reDefine { m =>
          var column = 0
          var row = 0

          // create header row
          g.columns.foreach { c =>
            visual.putCell(column, row, new visual.Cell(m.getOrElseDefine(c.header, build(c.header, visual))))
            column += 1
          }

          column = 0
          row += 1

          // create rows for data
          g.rows.foreach { r =>
            r.foreach { e =>
              visual.putCell(column, row, new visual.Cell(m.getOrElseDefine(e, build(e, visual))))
              column += 1
            }
            column = 0
            row += 1
          }
        }
      }
  }
  rule {
    case b: Button =>
      val mouseOverListener = MouseOverListener()
      val clickedListener =
        ButtonLikeClickListener(_.clicked += b.raiseClicked())


      Background(_.color = new Color(0, 0, 0, 0))(mouseOverListener.target = _)(clickedListener.target = _) -:
        //        CrossPainter(posListener.target = _).bind(_.crossPosition = posListener.lastPosition) -:
        Border(_.thickness = 4)(_.bind(_.color = mouseOverListener.isMouseOver match {
          case true => Color.BLUE
          case false => Color.BLACK
        })) -:
        Background(_.color = new Color(0, 0, 0, 0))(_.bind(_.color = clickedListener.isDown match {
          case true => Color.YELLOW
          case false => Color.WHITE
        })) -:
        Margin(_.thickness = 10) ~: { b.child }

  }

  rule { case l: Label => Margin(_.thickness = 2) -: Text().bind(_.text = l.text) }

  rule { case g: VPanel => StackPanel(_.orientation = Orientation.vertical) ~: { g.children } }
  rule { case g: HPanel => StackPanel(_.orientation = Orientation.horizontal) ~: { g.children } }

  rule {
    case r: Root =>
      val bg = Background(_.color = Color.WHITE)  ~: {
        println("root changed")
        r.child
      }
    //bg.layoutTransform=Some(AffineTransform.getScaleInstance(2.0.toFloat, 2.0.toFloat))

  }

  rule {
    case t: TabPanel =>
      StackPanel(_.orientation = Orientation.vertical) / {
        def createHeadVisual(child: Element): Visual = {
          val childVisual = Background(_.color = Color.WHITE) -: Border() -: (child match {
            case h: HeaderedElement if (h.header.isDefined) =>
              build(h.header.get, HeaderedElement.HEADER)
            case o => build(o, HeaderedElement.HEADER)
          })

          val overListener = MouseOverListener(childVisual)
          childVisual.\[Border].bind(_.color = overListener.isMouseOver match {
            case true => Color.BLUE
            case false => Color.BLACK
          })

          childVisual.bind(_.color = t.selectedElement.exists(_ == child) match {
            case true => Color.YELLOW
            case false => Color.WHITE
          })

          val listener = ButtonLikeClickListener(childVisual)
          listener.clicked.addWeak({
            println("set selectedItem " + child)
            t.selectedElement = Some(child)
          })(childVisual)

          return childVisual
        }

        StackPanel(_.orientation = Orientation.horizontal).bind(_.replaceChildren(t.children.map(createHeadVisual(_))))

        SingleChildVisual().bind(_.child = t.selectedElement.map({
          case h: HeaderedElement if (h.child.isDefined) =>
            build(h.child.get, HeaderedElement.CHILD)
          case o => build(o, HeaderedElement.CHILD)
        }))
      }
  }

  rule {
    case t: ComboBox =>
      val listener = ClickListener()
      val root = Background(listener.target = _)(_.color = Color.YELLOW) ~: { t.selectedElement }
      listener.clicked += {
        val lpfRoot = JLpfRootLike.getRoot(root).get
        val popup = new JPopupMenu()

        val popupRoot = StackPanel()
        OneWayListBinding.bind(theVisualBuilder.dispatcher)(t.children)(element => {
          val clickListener = ClickListener(_.clicked += {
            t.selectedElement = Some(element)
            popup.setVisible(false)
          })
          val mouseOverListener = MouseOverListener()

          Background(clickListener.target = _)(mouseOverListener.target = _)(
            _.bind(_.color = mouseOverListener.isMouseOver match {
              case true => Color.BLUE
              case false => Color.WHITE
            })) ~: element

        })(popupRoot.replaceChildren(_))(root)
        val content = JLpfPane(MinSizer().bind(_.width = root.actualSize.width) -: popupRoot)

        popup.add(content)

        val rootPosition = root.pointInRootCoordinates(Point(0, root.actualSize.height))
        popup.show(lpfRoot, rootPosition.x.toInt, rootPosition.y.toInt)
      }
  }

  rule {
    case t: TextField =>

      var border: Border = null

      val b = Background(_.color = Color.WHITE) -:
        Border(border = _) -: TextFieldVisual(_.text = t.currentText)
      val selectionListener = SelectionListener(b)
      val textEnterListener = TextEnterListener(b)
      val textMoveListener = TextMoveListener(b)
      val keyboardFocusListener = KeyboardFocusTraversalListener(b)

      def commit() { t.text = t.currentText.value }

      // handle focus traversals out of the text field
      keyboardFocusListener.focusNext += {
        KeyboardFocusManager.focusNextVisual()
      }

      keyboardFocusListener.focusPrevious += {
        KeyboardFocusManager.focusPreviousVisual()
      }

      KeyboardFocusManager.focusChanged.addWeak({ args: KeyboardFocusManager.FocusChangeEventArgs =>
        println(args)
        if (args.visualGainedFocus(b) && t.currentText.caretPosition.isEmpty)
          t.currentText.placeCaretToEnd
        if (args.visualLostFocus(b))
          commit()
      })(b)

      border.bind(_.color = if (KeyboardFocusManager.isFocused(b)) Color.GREEN else Color.BLACK)

      val v = b.\\[TextFieldVisual]
      selectionListener.selectionStarted += { p =>
        t.currentText.caretPosition = Some(v.getCharIndex(p))
        t.currentText.selection = None
        KeyboardFocusManager.focus(b)
      }
      selectionListener.selectionChanged += { p =>
        val a = v.getCharIndex(p._1)
        val b = v.getCharIndex(p._2)

        println("selectionChanged " + a + " " + b)
        t.currentText.caretPosition = Some(b)
        t.currentText.selection = Some((a, b))
      }

      textEnterListener.charEntered += { ch => t.currentText.caretPosition.foreach(t.currentText.insert(_, ch.toString)) }
      textEnterListener.backspace += t.currentText.caretPosition.filter(_ > 0).foreach(pos => t.currentText.delete(pos - 1, 1))
      textEnterListener.delete += t.currentText.caretPosition.filter(_ < t.currentText.value.length()) foreach (pos => t.currentText.delete(pos, 1))
      textEnterListener.textCommited += commit()
      textMoveListener.left += {
        t.currentText.caretPosition.filter(_ > 0).foreach { pos =>
          t.currentText.caretPosition = Some(pos - 1)
        }
      }
      textMoveListener.right += {
        t.currentText.caretPosition.filter(_ < t.currentText.value.length()).foreach { pos =>
          t.currentText.caretPosition = Some(pos + 1)
        }
      }
  }

  rule {
    case t: ElementList =>
      Border(_.color = Color.BLUE) -:
        StackPanel(_.orientation = Orientation.vertical).bind(_.replaceChildren(t.children.map { child =>
          val container = Background() -: build(child, t)
          val listener = ButtonLikeClickListener(container)
          listener.clicked += { t.selectedElement = Some(child) }
          container.bind(_.color = if (t.selectedElement.exists(_.equals(child))) Color.BLUE else new Color(0, 0, 0, 0))
          container
        }))
  }
}

