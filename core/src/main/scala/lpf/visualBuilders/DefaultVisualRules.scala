package lpf.visualBuilders

import lpf.VisualRuleList
import lpf.element._
import lpf.visual._
import java.awt.Color
import lpf.visual.listener._
import lpf.visual.animator.TwoColorBlender
import lpf.visual.animator.LinearAnimator
import lpf.binding.OneWayBinding
import lpf.Application

class ColorScheme {
  var button = new Color(0.5f, 0.5f, 1f)
  var buttonMouseOver = new Color(0.8f, 0.8f, 0f)
}

object DefaultVisualRules extends VisualRuleList {
  val colors = new ColorScheme()
  rule {
    case r: Root => Background(_.color = Color.WHITE) ~: r.child
  }

  rule {
    case b: Button =>
      val blender = new TwoColorBlender(new LinearAnimator())
      blender.colorA = colors.button
      blender.colorB = colors.buttonMouseOver
      blender.animator.speed=1/0.2

      val clickListener =
        ButtonLikeClickListener(_.clicked += b.raiseClicked())
      val mouseOverListener = MouseOverListener()

      val root = Background(clickListener.target = _)(mouseOverListener.target = _)
        .bind(_.color = blender.color) ~: b.child

      OneWayBinding(Application.bindingDispatcher)(if (mouseOverListener.isMouseOver) blender.goToB else blender.goToA)(root)
  }
  include(WireframeVisualRules)
}