/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf

import java.awt.Dimension

class Point(val x: Double, val y: Double) {
  def +(p: Point) = Point(x + p.x, y + p.y)
  def +(x: Double, y: Double) = Point(this.x + x, this.y + y)
  def -(p: Point) = Point(x - p.x, y - p.y)
  def -(x: Double, y: Double) = Point(this.x - x, this.y + y)
  def scale(scaleX: Double, scaleY: Double) = Point(x * scaleX, y * scaleY)
  override def toString(): String = "[Point " + x + "," + y + "]"
  def toAwtPoint: java.awt.Point = new java.awt.Point(x.toInt, y.toInt)
  def toAwtPoint2D: java.awt.geom.Point2D = new java.awt.geom.Point2D.Double(x, y)
}

object Point {
  def apply(x: Double, y: Double): Point = new Point(x, y)
  def apply(): Point = apply(0, 0)
  def apply(p: java.awt.geom.Point2D) =new Point(p.getX(), p.getY())
}

class Size(val width: Double, val height: Double) {
  def +(s: Size) = Size(width + s.width, height + s.height)
  def +(width: Double, height: Double) = Size(this.width + width, this.height + height)
  def -(s: Size) = Size(width - s.width, height - s.height)
  def -(width: Double, height: Double) = Size(this.width - width, this.height - height)
  def scale(scaleWidth: Double, scaleHeight: Double) = Size(width * scaleWidth, height * scaleHeight)

  def max(s: Size) = Size(width.max(s.width), height.max(s.height))

  override def toString() = "[Size " + width + "x" + height + "]"

  def toDimension = new java.awt.Dimension(width.toInt, height.toInt)
}

object Size {
  def apply(width: Double, height: Double) = new Size(width, height)
  def apply(d: java.awt.Dimension) = new Size(d.getWidth(), d.getHeight())
  def apply(rect: java.awt.geom.Rectangle2D) = new Size(rect.getWidth(), rect.getHeight())
  def apply() = new Size(0, 0)
}

class Rectangle(val position: Point, val size: Size) {
  def topLeft = Point(left, top)
  def topRight = Point(right, top)
  def bottomLeft = Point(left, bottom)
  def bottomRight = Point(right, bottom)

  def top = position.y
  def bottom = position.y + height
  def left = position.x
  def right = position.x + width

  def width = size.width
  def height = size.height

  def toAwtRectangle: java.awt.Rectangle = new java.awt.Rectangle(
    position.x.asInstanceOf[Int],
    position.y.asInstanceOf[Int],
    width.asInstanceOf[Int],
    height.asInstanceOf[Int])
  override def toString(): String = "[Rectangle " + position + " " + size + "]"
}

object Rectangle {
  def apply(position: Point, size: Size) = new Rectangle(position, size)
  def apply() = new Rectangle(Point(), Size())
}
