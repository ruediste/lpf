/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import scala.collection.mutable.ListBuffer
import java.awt.Color
import java.awt.Stroke
import java.awt.geom.AffineTransform

case class GraphicsContext(val originalGraphics: Graphics2D, val visibilityBuffer: VisibilityBuffer) {}

class Graphics(val transform: AffineTransform, val graphics: Graphics2D, val visGraphics: Option[Graphics2D], ctx: GraphicsContext) {

  /**
   * create a new Graphics for the specified visual
   */
  def create(v: Visual) = {
    // create the new transformation
    val newTransform = new AffineTransform(transform)
    v.concatenateVisualTransformTo(newTransform)

    // create the graphics
    val graphics = ctx.originalGraphics.create().asInstanceOf[Graphics2D]
    graphics.transform(newTransform)

    // create the visibility graphics
    val visGraphics = ctx.visibilityBuffer.graphics(v)
    visGraphics.foreach(_.transform(newTransform))
    
    new Graphics(newTransform,graphics,visGraphics,ctx)
  }

  def setColor(c: Color): Unit = graphics.setColor(c)
  def setStroke(s: Stroke): Unit = {
    graphics.setStroke(s)
    visGraphics.foreach(_.setStroke(s))
  }

  def setFont(font: java.awt.Font): Unit = {
    graphics.setFont(font)
    visGraphics.foreach(_.setFont(font))
  }

  def drawString(text: String, position: Point) = {
    val maxAscent = graphics.getFontMetrics().getMaxAscent()
    graphics.drawString(text, position.x.asInstanceOf[Float], (position.y + maxAscent).asInstanceOf[Float]);
    visGraphics.foreach(_.drawString(text, position.x.asInstanceOf[Float], (position.y + maxAscent).asInstanceOf[Float]))
  }
  def fillRect(x: Double, y: Double, width: Double, height: Double) = {
    graphics.fillRect(x.toInt, y.toInt, width.toInt, height.toInt)
    visGraphics.foreach(_.fillRect(x.toInt, y.toInt, width.toInt, height.toInt))
  }

  def drawLine(x1: Double, y1: Double, x2: Double, y2: Double) = {
    graphics.drawLine(x1.toInt, y1.toInt, x2.toInt, y2.toInt)
    visGraphics.foreach(_.drawLine(x1.toInt, y1.toInt, x2.toInt, y2.toInt))
  }

  def dispose = {
    graphics.dispose()
    visGraphics.foreach(_.dispose())
  }
}
