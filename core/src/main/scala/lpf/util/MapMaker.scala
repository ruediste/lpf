/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.util
import scala.collection.JavaConversions._
import scala.collection.mutable.SynchronizedMap
import scala.collection.mutable.SynchronizedSet
import lpf.unobserved
/**
 * Wrapper around Guava's MapMaker, already omits all deprecated methods
 * 
 * A builder of ConcurrentMap instances having any combination of:
 * - keys or values automatically wrapped in weak references
 * - values automatically wrapped in soft references
 * 
 * By default, the returned map uses equality comparisons (the equals method) to determine 
 * equality for keys or values. However, if weakKeys() was specified, 
 * the map uses identity (==) comparisons instead for keys. Likewise, if weakValues() or 
 * softValues() was specified, the map uses identity comparisons for values.
 */
class MapMaker {
	val maker=new com.google.common.collect.MapMaker(); 
	def concurrencyLevel(concurrencyLevel: Int) = {maker.concurrencyLevel(concurrencyLevel); this }
	def initialCapacity(initialCapacity: Int) = {maker.initialCapacity(initialCapacity); this }
	def makeSynchronizedMap[K,V](): scala.collection.concurrent.Map[K,V] = {
		return new JConcurrentMapWrapper(maker.makeMap[K,V]()) with SynchronizedMap[K,V]
		}
	def makeMap[K,V](): scala.collection.concurrent.Map[K,V] = maker.makeMap[K,V]()
	def makeSynchronizedSet[K](): scala.collection.mutable.Set[K] = {
		return new JSetWrapper(
		java.util.Collections.newSetFromMap(maker.makeMap[K,java.lang.Boolean]))
		with SynchronizedSet[K]
	}
	def makeSet[K](): scala.collection.mutable.Set[K] = java.util.Collections.newSetFromMap(maker.makeMap[K,java.lang.Boolean])
	def softValues()={maker.softValues(); this}
	def weakKeys()={maker.weakKeys(); this}
	def weakValues()={maker.weakValues(); this}
}

object MapMaker {
	def apply() = new MapMaker()
}
