/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.util
import scala.collection.mutable.Map

/**
 * like a set, but can contain multiple values
 */
class Bag[A] {
	private val map = Map[A, Int]()

	def add(elem: A): Unit = map.put(elem, map.getOrElse(elem, 0) + 1)
	def remove(elem: A): Unit = map.get(elem) match {
		case Some(i: Int) if (i > 1) => map.put(elem, i - 1)
		case Some(i: Int) => map.remove(elem)
		case None => // nothing to do
	}

	def +=(elem: A): Unit = add(elem)
	def -=(elem: A): Unit = remove(elem)
	
	def count(elem: A): Int = map.getOrElse(elem, 0)
	def removeAll(elem: A) = map.remove(elem)
}

object Bag {
	def apply[A]() = new Bag[A]()
}
