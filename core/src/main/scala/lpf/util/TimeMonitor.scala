package lpf.util

import lpf.unobserved
import scala.collection.mutable.HashMap

class TimeMonitor(val name: String) {
  private val lock = new Object()
  @unobserved var time: Long = 0
  //@unobserved var depth = 0

  private val depth = new ThreadLocal[Int]() {
    override def initialValue: Int = 0
  }

  def time[R](block: => R): R = {
    val localDepth = depth.get()
    depth.set(localDepth + 1)
    val t0 = System.nanoTime()
    try {
      block // call-by-name
    } finally {
      val t1 = System.nanoTime()

      depth.set(localDepth)
      if (localDepth == 0)
        lock.synchronized {
          time += t1 - t0
        }
    }
  }

  def apply[R](block: => R): R = time(block)

  def print() {
    println(toString)
  }

  override def toString() = name + ": Elapsed " + time / 1e9 + "s"
}

object TimeMonitor {
  private val map = MapMaker().makeMap[String, TimeMonitor]()
  def apply(name: String) = map.getOrElseUpdate(name, new TimeMonitor(name))

  def printMonitors() {
    map.values.foreach(_.print)
  }

  @unobserved private var periodicPrintingEnabled = true

  def enablePeriodicPrinting(enable: Boolean) {
    periodicPrintingEnabled = enable
  }

  private val timerThread = new Thread() {
    override def run() {
      while (true) {
        if (periodicPrintingEnabled) printMonitors()
        Thread.sleep(2000)
      }
    }
  }

  timerThread.setName("PeriodicTTimeMonitorPrinting")
  timerThread.setDaemon(true)
  timerThread.start()
}