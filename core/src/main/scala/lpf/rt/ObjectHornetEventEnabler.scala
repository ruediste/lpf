package lpf.rt

import lpf.dependency.HornetReadEventDispatcher
import lpf.dependency.HornetWrittenEventDispatcher
import scala.collection.JavaConversions._

/**
 * Enables object hornets. This is the only place to enable object hornets.
 * This makes sure, the MethodCallForwarder is initialized before any object hornet is enabled
 */
object ObjectHornetEventEnabler {

  // set the receiver of the method call forwarder
  MethodCallForwarder.receiver = new MethodCallReceiver {
    def hornetWillBeRead(obj: AnyRef): Unit = {
      // println("hornet will be read")
      HornetReadEventDispatcher.hornetWillBeRead(obj)
    }
    def hornetWritten(obj: AnyRef): Unit = {
      // println("hornet will be written")
      HornetWrittenEventDispatcher.hornetWritten(obj)
    }
  }

  def enableHornetEvents(obj: AnyRef): AnyRef = {
    ObjectHornetEventEnablerHelper.enableHornetEvents(obj)
    return obj
  }

  def enableHornetEvents(obj: ObjectHornet): AnyRef = {
    ObjectHornetEventEnablerHelper.enableHornetEvents(obj)
    return obj
  }
  
  def enableHornetEventsVoid(obj: AnyRef) {
    ObjectHornetEventEnablerHelper.enableHornetEvents(obj)
  }
}