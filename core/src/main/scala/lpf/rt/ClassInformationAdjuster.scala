package lpf.rt

import lpf.instrumentation.ClassInformationAdjusterBase
import lpf.hornet.ClassInformation

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  override def adjustClassInformation(info: ClassInformation) {
	  info.setApplicationClass(false)
  }
}