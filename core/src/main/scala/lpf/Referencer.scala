/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf
import scala.collection.mutable.Set
import _root_.lpf.util._
import scala.collection.mutable.HashMap

/**
 * Key to access references within a [[Referencer]].
 * 
 * If a referencer contains a value for a reference key, the key is said
 * to be defined for the referencer.
 */
class ReferenceKey[A](val default: () => A) {
	/**
	 * Set the reference of the referencer for this key to value. The reference for this key is
	 * defined afterwards
	 */
	def set(referencer: Referencer, value: A) = {
		referencer.references.put(this, value)
	}
	
	/**
	 * Remove the reference of the referencer for this key. The reference for
	 * this key is undefined afterwards.
	 */
	def remove(referencer: Referencer) = referencer.references.remove(this)

	/**
	 * Get the reference of the referencer for this key. If the reference is undefined,
	 * [[default]] will be used to create a new value. The reference for this key is defined afterwards.
	 */
	def get(referencer: Referencer): A = referencer.references.getOrElseUpdate(this, default()).asInstanceOf[A]

	/**
	 * Get the reference of the referencer for this key. If the reference is undefined, return None
	 */
	def getIfDefined(referencer: Referencer): Option[A] = referencer.references.get(this).asInstanceOf[Option[A]]

	/**
	 * True if the reference of the referencer for this key is defined
	 */
	def isDefined(referencer: Referencer) = referencer.references.isDefinedAt(this)

	/**
	 * Return the reference of the referencer for this key. If the key is not defined, set it to the
	 * result of evaluating op. The reference is defined afterwards.
	 */
	def getOrElseUpdate(referencer: Referencer, op: => A): A = referencer.references.getOrElseUpdate(this, op).asInstanceOf[A]
}

object ReferenceKey {
	/**
	 * instantiate a reference key without default value generator
	 */
	def apply[A]() = new ReferenceKey[A](()=>{ throw new Exception("no default specified for Reference Key") })

	/**
	 * instantiate a reference key with a default value generator
	 */
	def apply[A](default: => A) = new ReferenceKey[A](() => default)
}

/**
 * References to other objects can be attached to a referencer using [[ReferenceKey]]s.
 */
trait Referencer {
	private[lpf] val references = MapMaker().makeSynchronizedMap[ReferenceKey[_], Any]()

	implicit protected val theReferencer = this

}
