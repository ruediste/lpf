/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.test.dependency

import lpf.dependency._
import lpf.test.testBase._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import java.util.concurrent.Semaphore
import lpf.Referencer

class HornetEventListenerTest extends FixtureBase {

  implicit val theReferencer = new Object() with Referencer

  @Test(timeout=1000) def previewAndEventRaised(): Unit = {
    val listener = new HornetEventListener();
    val events = ArrayBuffer[AnyRef]()
    val previewRaised = new Object();
    val eventRaised = new Object();

    // wire events
    listener.previewHornetWritten.addStrong(events += previewRaised)
    listener.hornetWritten.addStrong(events += eventRaised)

    listener.receiveHornetWritten();

    assert(events(0) == previewRaised)
    assert(events(1) == eventRaised)
  }

  private class TestDummy {
    def functionEnter: Unit = {}
    def functionLeave: Unit = {}
    def notification: Unit = {}
  }

  private def notifyOfChangesWithDummy(dummy: TestDummy, function: => Unit)(implicit theReferencer: Referencer) {
    val notifier = ChangeNotifier()

    notifier.notifyOfChanges(dummy.notification, {
      dummy.functionEnter
      function
      dummy.functionLeave
    })
  }

  @Test(timeout=100) def notifyOfChanges_functionIsExecuted(): Unit = {
    var executed = false
    val notifier = ChangeNotifier()
    val result = notifier.notifyOfChanges({}, { executed = true; 1 })
    assert(result == 1)
    assert(executed)
  }

  @Test(timeout=100) def notifyOfChanges_threadLocalChangesDuringFunctionExecutionAreIgnored(): Unit = {
    val dummy = context.mock(classOf[HornetEventListenerTest.this.TestDummy])
    val seq = context.sequence("seq")

    context.checking(new Expectations {
      one(dummy).functionEnter; inSequence(seq)
      one(dummy).functionLeave; inSequence(seq)
    })

    notifyOfChangesWithDummy(dummy, {
      HornetReadEventDispatcher.hornetWillBeRead(HornetEventListenerTest.this, "foo");
      HornetWrittenEventDispatcher.hornetWritten(HornetEventListenerTest.this, "foo");
    })

    context.assertIsSatisfied()
  }

  @Test(timeout=100) def notifyOfChanges_threadLocalChangesAreReported() {
    val dummy = context.mock(classOf[HornetEventListenerTest.this.TestDummy])
    val seq = context.sequence("seq")

    context.checking(new Expectations {
      one(dummy).functionEnter; inSequence(seq)
      one(dummy).functionLeave; inSequence(seq)
      one(dummy).notification; inSequence(seq)
    })

    notifyOfChangesWithDummy(dummy, {
      HornetReadEventDispatcher.hornetWillBeRead(HornetEventListenerTest.this, "foo");
      HornetReadEventDispatcher.hornetWillBeRead(HornetEventListenerTest.this, "foo");
    })

    HornetWrittenEventDispatcher.hornetWritten(HornetEventListenerTest.this, "foo");

    context.assertIsSatisfied()
  }

  @Test(timeout=100) def notifyOfChanges_repeatedThreadLocalChangesAreReportedOnce(): Unit = {
    val dummy = context.mock(classOf[HornetEventListenerTest.this.TestDummy])
    val seq = context.sequence("seq")

    context.checking(new Expectations {
      one(dummy).functionEnter; inSequence(seq)
      one(dummy).functionLeave; inSequence(seq)
      one(dummy).notification; inSequence(seq)
    })

    notifyOfChangesWithDummy(dummy, {
      HornetReadEventDispatcher.hornetWillBeRead(HornetEventListenerTest.this, "foo");
    })

    HornetWrittenEventDispatcher.hornetWritten(HornetEventListenerTest.this, "foo");
    HornetWrittenEventDispatcher.hornetWritten(HornetEventListenerTest.this, "foo");

    context.assertIsSatisfied()
  }

  @Test(timeout=1000) def notifyOfChanges_nonThreadLocalChangesDuringFunctionExecutionAreReported(): Unit = {
    val dummy = context.mock(classOf[HornetEventListenerTest.this.TestDummy])
    val seq = context.sequence("seq")
    val target = new Object()

    context.checking(new Expectations {
      one(dummy).functionEnter; inSequence(seq)
      one(dummy).notification; inSequence(seq)
      one(dummy).functionLeave; inSequence(seq)
    })

    val semaphore = new Semaphore(0)

    notifyOfChangesWithDummy(dummy, {
      val thread = new Thread {
        override def run() = {
          dummy.notification
          //println("sending field change")
          HornetWrittenEventDispatcher.hornetWritten(target, "foo");
          semaphore.release()
        }
      }

      thread.start();
      semaphore.acquire()
    })

    context.assertIsSatisfied()
  }

  @Test(timeout=100) def notifyOfChanges_nonThreadLocalChangesAreReported() {
    val dummy = context.mock(classOf[HornetEventListenerTest.this.TestDummy])
    val seq = context.sequence("seq")
    val target = new Object()

    context.checking(new Expectations {
      one(dummy).functionEnter; inSequence(seq)
      one(dummy).functionLeave; inSequence(seq)
      one(dummy).notification; inSequence(seq)
    })

    notifyOfChangesWithDummy(dummy, { HornetReadEventDispatcher.hornetWillBeRead(target, "foo"); })

    // change from different thread
    val semaphore = new Semaphore(0)
    val thread = new Thread {
      override def run() = {
        //println("sending field change")
        HornetWrittenEventDispatcher.hornetWritten(target, "foo");
        HornetWrittenEventDispatcher.hornetWritten(target, "foo");
        semaphore.release()
      }
    }

    thread.start();
    semaphore.acquire()

    context.assertIsSatisfied()
  }

  @Test(timeout=100) def notifyOfChanges_disableWorks() {
    val listener = new HornetEventListener();
    var failed = false

    // wire events
    listener.previewHornetWritten.addStrong(failed = true)
    listener.hornetWritten.addStrong(failed = true)

    listener.disable
    listener.receiveHornetWritten();

    assert(!failed)
  }
}
