/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.test.dependency
import lpf.dependency._
import lpf.test.testBase._

class HornetReadEventDispatcherTest extends FixtureBase {

  val target = new Object()

  @Test def capturingWithoutListener(): Unit = {
    HornetReadEventDispatcher.hornetWillBeRead(target)
  }

  @Test def capturingWithoutListenerFieldHornet(): Unit = {
    HornetReadEventDispatcher.hornetWillBeRead(target, "foo")
  }

  @Test def capturingWithListener(): Unit = {
    val listener = context.mock(classOf[HornetEventListener])
    context.checking(new Expectations {
      one(listener).hornetWillBeRead(target)
    })

    HornetReadEventDispatcher.withListener(listener) {
      HornetReadEventDispatcher.hornetWillBeRead(target);
    }

    context.assertIsSatisfied()
  }

  @Test def capturingWithListenerFieldHornet(): Unit = {
    val listener = context.mock(classOf[HornetEventListener])
    context.checking(new Expectations {
      one(listener).hornetWillBeRead(target, "foo")
    })

    HornetReadEventDispatcher.withListener(listener) {
      HornetReadEventDispatcher.hornetWillBeRead(target, "foo");
    }

    context.assertIsSatisfied()
  }

  @Test def capturingWithSubListener(): Unit = {
    val listener = context.mock(classOf[HornetEventListener], "listener")
    val subListener = context.mock(classOf[HornetEventListener], "subListener")
    context.checking(new Expectations {
      one(subListener).hornetWillBeRead(target)
    })

    HornetReadEventDispatcher.withListener(listener) {
      HornetReadEventDispatcher.withListener(subListener) {
        HornetReadEventDispatcher.hornetWillBeRead(target);
      }
    }

    context.assertIsSatisfied()
  }
  
  @Test def capturingWithSubListenerFieldHornet(): Unit = {
    val listener = context.mock(classOf[HornetEventListener], "listener")
    val subListener = context.mock(classOf[HornetEventListener], "subListener")
    context.checking(new Expectations {
      one(subListener).hornetWillBeRead(target, "foo")
    })

    HornetReadEventDispatcher.withListener(listener) {
      HornetReadEventDispatcher.withListener(subListener) {
        HornetReadEventDispatcher.hornetWillBeRead(target, "foo");
      }
    }

    context.assertIsSatisfied()
  }

  


  @Test def test_withListener(): Unit = {
    val listener = context.mock(classOf[HornetEventListener])
    val subListener = new HornetEventListener

    context.checking(new Expectations {
    })

    HornetReadEventDispatcher.withListener(listener) {
      HornetReadEventDispatcher.withListener(subListener) {
        HornetReadEventDispatcher.hornetWillBeRead(target, "foo");
      }
    }

    context.assertIsSatisfied()
  }
  
   @Test def test_withListenerFieldHornet(): Unit = {
    val listener = context.mock(classOf[HornetEventListener])
    val subListener = new HornetEventListener

    context.checking(new Expectations {
    })

    HornetReadEventDispatcher.withListener(listener) {
      HornetReadEventDispatcher.withListener(subListener) {
        HornetReadEventDispatcher.hornetWillBeRead(target, "foo");
      }
    }

    context.assertIsSatisfied()
  }
}
