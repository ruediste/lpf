/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.test.dependency

import lpf.dependency._
import lpf.test.testBase._
import scala.ref.WeakReference

@RunWith(classOf[JUnit4])
class HornetWrittenEventDispatcherTest extends FixtureBase {
	
	@After def after(): Unit ={
		HornetWrittenEventDispatcher.clear()
		System.gc();
	}
	
	var hornet=new Object();
	
	@Test def sendWithoutListeners(): Unit ={
		HornetWrittenEventDispatcher.hornetWritten(hornet)
	}
	
	@Test def sendWithoutListenersFieldHornet(): Unit ={
		HornetWrittenEventDispatcher.hornetWritten(hornet,"foo")
	}
	
	@Test def sendWithListeners(): Unit ={
		val listener=context.mock(classOf[HornetEventListener])
		
		context.checking(new Expectations{
			one(listener).receiveHornetWritten()
		})
		
		HornetWrittenEventDispatcher.register(listener,hornet);
		HornetWrittenEventDispatcher.hornetWritten(hornet)
		
		context.assertIsSatisfied()
	}
	
	@Test def sendWithListenersFieldHornet(): Unit ={
		val listener=context.mock(classOf[HornetEventListener])
		
		context.checking(new Expectations{
			one(listener).receiveHornetWritten()
		})
		
		HornetWrittenEventDispatcher.register(listener,hornet,"foo");
		HornetWrittenEventDispatcher.hornetWritten(hornet,"foo")
		
		context.assertIsSatisfied()
	}
	
	@Test def targetCanBeFreed(): Unit ={
		var listener=new HornetEventListener();
		var eventRaised=false;
		listener.hornetWritten.addStrong {eventRaised=true};
		
		// register listener
		HornetWrittenEventDispatcher.register(listener,hornet);
		
		// send event
		HornetWrittenEventDispatcher.hornetWritten(hornet)
		
		// check that listener received event
		assert(eventRaised); eventRaised=false
		
		val targetReference=WeakReference(hornet)
		// clear target
		hornet=null;
		
		// do a GC
		System.gc();
		/*System.runFinalization()
		System.gc();*/
		Thread.sleep(100)
		
		// check that the listener received the event of the disappeared target
		//assert(eventRaised); eventRaised=false
		assert(targetReference.get.isEmpty)
	}
	
	@Test def listenerCanBeFreed(): Unit ={
		var listener=new HornetEventListener();
		var eventRaised=false;
		listener.hornetWritten.addStrong {eventRaised=true};
		
		// register listener
		HornetWrittenEventDispatcher.register(listener,hornet);
		
		// keep weak reference to the listener
		val ref=new WeakReference(listener)
		// clear the listener
		listener=null
		
		// do a GC
		System.gc();
		System.runFinalization()
		System.gc();
		Thread.sleep(100)
		
		// check that the listener has been collected
		assert(ref.get.isEmpty)
	}
	
}
