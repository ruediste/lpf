/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.test

import lpf._
import lpf.test.testBase._
import scala.collection.mutable.ListBuffer
import lpf.FunctionBuildingRule2
import lpf.BuildingRuleList



@RunWith(classOf[JUnit4])
class BuildingRuleListIteratorTest extends FixtureBase {
	abstract class Dummy {
		def func(i: Int): Unit
	}

	@Test def iterateOverSimpleList() {
		val rules = List(
			new FunctionBuildingRule2[Int, Int, Int](null),
			new FunctionBuildingRule2[Int, Int, Int](null))

		val list = new BuildingRuleList[Int,Int, Int]();
		rules.foreach(list.addRule(_))

		assert(list.ruleIterator.toList.equals(rules))
	}

	@Test def diveIntoSubList() {
		val rules = List(
			new FunctionBuildingRule2[Int, Int, Int]({ case (0,_) => }),
			new FunctionBuildingRule2[Int, Int, Int]({ case (0,_) => }))
		val subRules = List(
			new FunctionBuildingRule2[Int, Int, Int]({ case (0,_) => }),
			new FunctionBuildingRule2[Int, Int, Int]({ case (0,_) => }))

		val list = new BuildingRuleList[Int, Int, Int] {
			addRule(rules(0))
			include({ case _ => }, new BuildingRuleList[Int, Int, Int] {
				subRules foreach addRule
			})
			addRule(rules(1))
		};

		val i = list.ruleIterator
		val result = ListBuffer[BuildingRule[Int, Int, Int]]()
		while (i.hasNext) {
			val r = i.next()
			result += r
			r.apply(1,null, i)
		}

		/*println("rules: " + rules)
		println("subRules: " + subRules)
		println("result: " + result)*/

		assert(result(0) == rules(0))
		assert(result(2) == subRules(0))
		assert(result(3) == subRules(1))
		assert(result(4) == rules(1))
	}
}
