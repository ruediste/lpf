package lpf.test.element

import lpf.element.EditableString
import lpf.test.testBase.FixtureBase
import org.junit.Test
import org.junit.Assert._

class EditableStringTest extends FixtureBase {

  @Test
  def testCaretPosition {
    val s = EditableString()

    s.value="Foo"
    s.caretPosition = Some(1)
    assert(s.caretPosition.isDefined)
    assert(s.caretPosition.get == 1)
  }

  @Test
  def testInsert {
    val s = EditableString()

    s.caretPosition = Some(0)
    s.insert(0, "Hello")

    assertEquals(5, s.caretPosition.get)
    assertEquals("Hello", s.value)
  }

  @Test
  def testInsert2 {
    val s = EditableString()
    s.value = "A"

    s.caretPosition = Some(0)
    s.insert(1, "Hello")

    assertEquals(0, s.caretPosition.get)
    assertEquals("AHello", s.value)
  }

  @Test
  def testInsert3 {
    val s = EditableString()
    s.value = "A"

    s.caretPosition = Some(0)
    s.insert(0, "Hello")

    assertEquals(5, s.caretPosition.get)
    assertEquals("HelloA", s.value)
  }

  @Test
  def testDelete {
    val s = EditableString()
    s.value = "Hello"

    s.caretPosition = Some(0)
    s.delete(1, 1)

    assertEquals(0, s.caretPosition.get)
    assertEquals("Hllo", s.value)
  }
  
  @Test
  def testDelete1 {
    val s = EditableString()
    s.value = "Hello"

    s.caretPosition = Some(1)
    s.delete(1, 1)

    assertEquals(1, s.caretPosition.get)
    assertEquals("Hllo", s.value)
  }
  @Test
  def testDelete2 {
    val s = EditableString()
    s.value = "Hello"

    s.caretPosition = Some(2)
    s.delete(1, 1)

    assertEquals(1, s.caretPosition.get)
    assertEquals("Hllo", s.value)
  }
}