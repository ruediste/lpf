package lpf.test.instrumentation

import org.junit.Test
import org.objectweb.asm._
import org.objectweb.asm.util.CheckClassAdapter
import java.io.PrintWriter
import lpf.hornet.HornetAdviceProvider
import java.io.StringWriter
import org.objectweb.asm.util.TraceClassVisitor
import java.security.ProtectionDomain
import lpf.instrumentation.InstrumentingClassVisitor
import lpf.test.binding.mutableList.MutableListChangesDetectionTest
import language.reflectiveCalls

/**
 * Test Class to verify an instrumented class
 */
class VerifyClassTest {
  def verifyClass() {
    val className = classOf[MutableListChangesDetectionTest].getName()

    // transform the class
    val adviceProvider = new HornetAdviceProvider()
    val reader = new ClassReader(className)
    val writer = new ClassWriter(reader, ClassWriter.COMPUTE_FRAMES)
    var cv: ClassVisitor = writer
    cv = new TraceClassVisitor(cv, new PrintWriter(System.out))
    cv = new InstrumentingClassVisitor(getClass().getClassLoader(), adviceProvider, cv)
    reader.accept(cv, ClassReader.EXPAND_FRAMES)

    // verify the transformed class
    val sw = new StringWriter();
    val pw = new PrintWriter(sw);
    val b=writer.toByteArray()
    CheckClassAdapter.verify(new ClassReader(b), false, pw);
    assert(sw.toString().length() == 0, sw.toString())

    val loader = new ClassLoader() {
      def publicDefine(name: String, b: Array[Byte], off: Int, len: Int, protectionDomain: ProtectionDomain) =
        defineClass(name, b, off, len, protectionDomain)
    }
    
    val clazz=loader.publicDefine(null, b, 0, b.length, null)
    val dummy=clazz.getConstructor().newInstance();
  }
}