package lpf.test.instrumentation;

public class StaticMethodContainer {
	public static StaticMethodLogger log;
	
	public static void staticMethod(Object obj, String name, String joinPoint){
		log.logMethod(obj,name,joinPoint);
	}
	
	public static void staticMethodString(String ownerClassName, String name, String joinPoint){
		log.logMethod(ownerClassName,name,joinPoint);
	}
	
	public static void staticMethodClass(Class<?> ownerClass, String name, String joinPoint){
		log.logMethod(ownerClass,name,joinPoint);
	}
	
	public static void staticMethodIntArg(Object obj, String name, int arg, String joinPoint){
		log.logMethodIntArg(obj,name,arg, joinPoint);
	}
	public static void staticMethodStringIntArg(String obj, String name, int arg, String joinPoint){
		log.logMethodIntArg(obj,name,arg, joinPoint);
	}

	public static void staticMethodClassIntArg(Class<?> obj, String name, int arg, String joinPoint){
		log.logMethodIntArg(obj,name,arg, joinPoint);
	}

}
