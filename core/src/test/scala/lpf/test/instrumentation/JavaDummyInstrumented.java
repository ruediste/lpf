package lpf.test.instrumentation;
import lpf.*;
import lpf.dependency.HornetReadEventDispatcher;
import lpf.dependency.HornetWrittenEventDispatcher;

public class JavaDummyInstrumented {
	public int i;
	public double d;
	public int[] intArray;
	
	public static int sI;
	public static double sD;
	public static int[] sIntArray;
	
	@Unobserved public int ai;
	@Unobserved public double ad;
	@Unobserved public int[] aIntArray;
	
	@Unobserved public static int asI;
	@Unobserved public static double asD;
	@Unobserved public static int[] asIntArray;
	
	public final int finalI=1;
	
	public static int getIField(JavaDummyInstrumented fieldOwner){
		HornetReadEventDispatcher.hornetWillBeRead(fieldOwner, "i");
		return fieldOwner.i;
	}
	
	public static void putIField(JavaDummyInstrumented fieldOwner, int newValue){
		fieldOwner.i=newValue;
		HornetWrittenEventDispatcher.hornetWritten(fieldOwner, "i");
	}
	public static int getSIField(){
		HornetReadEventDispatcher.hornetWillBeRead(JavaDummyInstrumented.class, "i");
		return sI;
	}
	
	public static void putSIField(int newValue){
		JavaDummyInstrumented.sI=newValue;
		HornetWrittenEventDispatcher.hornetWritten(JavaDummyInstrumented.class, "i");
	}
}
