package lpf.test.instrumentation;

public class DummyAccess {

	public void access(JavaDummyInstrumented dummy) {
		@SuppressWarnings("unused")
		int foo = 0;
		foo += dummy.i;
		dummy.i=3;
		
		foo+=JavaDummyInstrumented.sI;
		JavaDummyInstrumented.sI=4;
	}
}