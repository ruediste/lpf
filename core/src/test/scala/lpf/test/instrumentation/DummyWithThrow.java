package lpf.test.instrumentation;

public class DummyWithThrow {
	public void withThrow() {
		throw new Error("test");
	}

	public void callWithThrow() {
		withThrow();
	}

	public void normal() {

	}
}
