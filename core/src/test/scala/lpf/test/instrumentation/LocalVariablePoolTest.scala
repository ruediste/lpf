package lpf.test.instrumentation

import lpf.test.testBase._
import org.junit.Assert._
import org.objectweb.asm.commons.LocalVariablesSorter
import org.objectweb.asm.Type
import org.jmock.Expectations._
import lpf.instrumentation.LocalVariablePool
import org.scalacheck.Commands
import org.scalacheck.Gen
import scala.collection.mutable.ListBuffer

class LocalVariablePoolTest extends FixtureBase {
  @Test
  def cannotFreeNonManagedVariable() {
    val mv = context.mock(classOf[LocalVariablesSorter])
    val pool = new LocalVariablePool(mv)

    val err = intercept[Error] { pool.free(3) }
    assertEquals("Unmanaged variable: 3", err.getMessage())
    context.assertIsSatisfied()
  }

  @Test
  def canFreeManagedVariable() {
    val mv = context.mock(classOf[LocalVariablesSorter])
    val pool = new LocalVariablePool(mv)
    context.checking(new Expectations() {
      one(mv).newLocal(Type.BOOLEAN_TYPE); will(returnValue(3))
    })
    val local = pool.newLocal(Type.BOOLEAN_TYPE)
    pool.free(local)
    context.assertIsSatisfied()
  }

  @Test
  def cannotRepeatFreeManagedVariable() {
    val mv = context.mock(classOf[LocalVariablesSorter])
    val pool = new LocalVariablePool(mv)
    context.checking(new Expectations() {
      one(mv).newLocal(Type.BOOLEAN_TYPE); will(returnValue(3))
    })
    val local = pool.newLocal(Type.BOOLEAN_TYPE)
    pool.free(local)
    val err = intercept[Error] { pool.free(local) }
    assertEquals("Variable has been freed before: 3", err.getMessage())
    context.assertIsSatisfied()
  }

  @Test
  def variableIsReusedAndCanBeFreedAgain() {
    val mv = context.mock(classOf[LocalVariablesSorter])
    val pool = new LocalVariablePool(mv)
    context.checking(new Expectations() {
      one(mv).newLocal(Type.BOOLEAN_TYPE); will(returnValue(3))
    })
    assertEquals(3, pool.newLocal(Type.BOOLEAN_TYPE))
    pool.free(3)
    assertEquals(3, pool.newLocal(Type.BOOLEAN_TYPE))
    pool.free(3)
    context.assertIsSatisfied()
  }

  /**
   * This test replays the sequence of invocations
   * needed to instrument the ArrayDummy
   */
  @Test
  def arrayDummyPattern() {
    val mv = context.mock(classOf[LocalVariablesSorter])
    val localVariablePool = new LocalVariablePool(mv)
    context.checking(new Expectations() {
      one(mv).newLocal(Type.getObjectType("java/lang/Object")); will(returnValue(10))
      one(mv).newLocal(Type.INT_TYPE); will(returnValue(11))
      one(mv).newLocal(Type.FLOAT_TYPE); will(returnValue(12))
    })

    // store
    var storeArgVars = ListBuffer[Int]();
    {
      storeArgVars += localVariablePool.newLocal(Type.getObjectType("java/lang/Object"))
      storeArgVars += localVariablePool.newLocal(Type.INT_TYPE)
      storeArgVars += localVariablePool.newLocal(Type.FLOAT_TYPE)

      localVariablePool.free(storeArgVars(0))
      localVariablePool.free(storeArgVars(1))
      localVariablePool.free(storeArgVars(2))
    }

    // load
    var loadArgVars = ListBuffer[Int]();
    {
      loadArgVars += localVariablePool.newLocal(Type.getObjectType("java/lang/Object"))
      loadArgVars += localVariablePool.newLocal(Type.INT_TYPE)
      localVariablePool.free(loadArgVars(0))
      localVariablePool.free(loadArgVars(1))
    }
    
    assert(loadArgVars(0)==storeArgVars(0))
    assert(loadArgVars(1)==storeArgVars(1))
  }
}