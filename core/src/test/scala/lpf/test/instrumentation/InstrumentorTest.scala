package lpf.test.instrumentation

import lpf.test.testBase._
import org.objectweb.asm._
import lpf.instrumentation._
import lpf.instrumentation.JoinPointKind._
import org.hamcrest.Matchers._
import java.lang.reflect.InvocationTargetException
import org.junit.Ignore

class InstrumentorTest extends FixtureBase {
  class EmptyAdviceProvider extends AdviceProviderBase {
    override def getAdvice(jp: JoinPoint): Option[Advice] = None
    override def needsInstrumentation(loader: ClassLoader, className: String, bytes: Array[Byte])=Some(true)
  }

  @Test
  def joinPointEquality() {
    val log = context.mock(classOf[Logger])
    val accessMethod1 = ClassMember(classOf[DummyAccess].getName(), "access", "(Llpf/test/instrumentationJavaDummyInstrumented;)V", false)
    val accessMethod2 = ClassMember(classOf[DummyAccess].getName(), "access", "(Llpf/test/instrumentationJavaDummyInstrumented;)V", false)
    val jp1 = JoinPoint(accessMethod1, accessMethod1, MethodEnter)
    val jp2 = JoinPoint(accessMethod2, accessMethod2, MethodEnter)
    assert(jp1.equals(jp2))

    context.checking(new Expectations() {
      // constructor
      one(log).logJoinPoint(JoinPoint(accessMethod1, accessMethod1, MethodEnter))
    })

    log.logJoinPoint(JoinPoint(accessMethod2, accessMethod1, MethodEnter))

    context.assertIsSatisfied()
  }

  @Test
  def canInstantiateInstrumentor() {
    val instrumentor = new lpf.instrumentation.Instrumentor(new EmptyAdviceProvider())
  }

  abstract class Logger() {
    def logJoinPoint(jp: JoinPoint): Unit
  }

  class LoggingAdviceProvider(val log: Logger) extends AdviceProviderBase {
    override def getAdvice(jp: JoinPoint): Option[Advice] = {
      log.logJoinPoint(jp)
      None
    }
  }

  @Test
  def joinPointsFound() {
    val log = context.mock(classOf[Logger])
    val adviceContainer = new LoggingAdviceProvider(log)
    val instrumentor = new Instrumentor(adviceContainer)
    val accessMethod = ClassMember(classOf[DummyAccess].getName(), "access", "(Llpf/test/instrumentation/JavaDummyInstrumented;)V", false)
    val initMethod = ClassMember(classOf[DummyAccess].getName(), "<init>", "()V", false)

    context.checking(new Expectations() {
      // constructor
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), BeforeCall))
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), AfterCall))
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), AfterCallFinally))

      one(log).logJoinPoint(JoinPoint(accessMethod, accessMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(accessMethod, accessMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(accessMethod, accessMethod, MethodLeaveFinally))

      // instance
      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "i", "I", false),
        BeforeGetField))
      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "i", "I", false),
        AfterGetField))

      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "i", "I", false),
        BeforeSetField))
      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "i", "I", false),
        AfterSetField))

      // static
      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "sI", "I", true),
        BeforeGetField))
      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "sI", "I", true),
        AfterGetField))

      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "sI", "I", true),
        BeforeSetField))
      one(log).logJoinPoint(JoinPoint(accessMethod, ClassMember(classOf[JavaDummyInstrumented].getName(), "sI", "I", true),
        AfterSetField))

      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodLeaveFinally))
    })
    val className = classOf[DummyAccess].getName()
    val cr = new ClassReader(className)
    val cw = new ClassWriter(cr, 0)
    instrumentor.instrument(className, getClass().getClassLoader(), cr, cw)

    context.assertIsSatisfied()
  }

  @Test
  def arrayJoinPointsFound() {
    val log = context.mock(classOf[Logger])
    val adviceContainer = new LoggingAdviceProvider(log)
    val instrumentor = new Instrumentor(adviceContainer)
    val initMethod = ClassMember(classOf[ArrayDummy].getName(), "<init>", "()V", false)

    context.checking(new Expectations() {
      // constructor
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), BeforeCall))
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), AfterCall))
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), AfterCallFinally))

      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, BeforeArrayRead))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, AfterArrayRead))
      exactly(2).of(log).logJoinPoint(JoinPoint(initMethod, initMethod, BeforeArrayWrite))
      exactly(2).of(log).logJoinPoint(JoinPoint(initMethod, initMethod, AfterArrayWrite))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, BeforeArrayLength))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, AfterArrayLength))

      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodLeaveFinally))
    })
    val className = classOf[ArrayDummy].getName()

    val cr = new ClassReader(className)
    val cw = new ClassWriter(cr, 0)
    instrumentor.instrument(className, getClass().getClassLoader(), cr, cw)

    context.assertIsSatisfied()
  }

  @Test
  def methodInvocationJoinPointsFound() {
    val log = context.mock(classOf[Logger])
    val adviceContainer = new LoggingAdviceProvider(log)
    val instrumentor = new Instrumentor(adviceContainer)
    val invokingMethod = ClassMember(classOf[DummyMethodCalls].getName(), "invokingMethod", "(Llpf/test/instrumentation/IDummy;)V", false)
    val interfaceMethod = ClassMember(classOf[IDummy].getName(), "interfaceMethod", "()V", false)
    val interfaceMethodImplementation = ClassMember(classOf[DummyMethodCalls].getName(), "interfaceMethod", "()V", false)
    val staticMethod = ClassMember(classOf[DummyMethodCalls].getName(), "staticMethod", "()V", true)
    val normalMethod = ClassMember(classOf[DummyMethodCalls].getName(), "normalMethod", "()V", false)
    val privateMethod = ClassMember(classOf[DummyMethodCalls].getName(), "privateMethod", "()V", false)
    val initMethod = ClassMember(classOf[DummyMethodCalls].getName(), "<init>", "()V", false)

    context.checking(new Expectations() {
      // constructor
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), BeforeCall))
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), AfterCall))
      one(log).logJoinPoint(JoinPoint(initMethod, ClassMember(classOf[Object].getName(), "<init>", "()V", false), AfterCallFinally))

      // enter/leave requests
      one(log).logJoinPoint(JoinPoint(invokingMethod, invokingMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(invokingMethod, invokingMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(invokingMethod, invokingMethod, MethodLeaveFinally))

      one(log).logJoinPoint(JoinPoint(interfaceMethodImplementation, interfaceMethodImplementation, MethodEnter))
      one(log).logJoinPoint(JoinPoint(interfaceMethodImplementation, interfaceMethodImplementation, MethodLeave))
      one(log).logJoinPoint(JoinPoint(interfaceMethodImplementation, interfaceMethodImplementation, MethodLeaveFinally))

      one(log).logJoinPoint(JoinPoint(staticMethod, staticMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(staticMethod, staticMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(staticMethod, staticMethod, MethodLeaveFinally))

      one(log).logJoinPoint(JoinPoint(normalMethod, normalMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(normalMethod, normalMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(normalMethod, normalMethod, MethodLeaveFinally))

      one(log).logJoinPoint(JoinPoint(privateMethod, privateMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(privateMethod, privateMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(privateMethod, privateMethod, MethodLeaveFinally))

      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodEnter))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodLeave))
      one(log).logJoinPoint(JoinPoint(initMethod, initMethod, MethodLeaveFinally))

      // method calls
      one(log).logJoinPoint(JoinPoint(invokingMethod, interfaceMethod, BeforeCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, interfaceMethod, AfterCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, interfaceMethod, AfterCallFinally))

      one(log).logJoinPoint(JoinPoint(invokingMethod, staticMethod, BeforeCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, staticMethod, AfterCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, staticMethod, AfterCallFinally))

      one(log).logJoinPoint(JoinPoint(invokingMethod, normalMethod, BeforeCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, normalMethod, AfterCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, normalMethod, AfterCallFinally))

      one(log).logJoinPoint(JoinPoint(invokingMethod, privateMethod, BeforeCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, privateMethod, AfterCall))
      one(log).logJoinPoint(JoinPoint(invokingMethod, privateMethod, AfterCallFinally))

    })
    val className = classOf[DummyMethodCalls].getName()
    val cr = new ClassReader(className)
    val cw = new ClassWriter(cr, 0)
    instrumentor.instrument(className, getClass().getClassLoader(), cr, cw)

    context.assertIsSatisfied()
  }

  @Test
  def joinPointInstrumentationEnterLeaveWithThrow() {
    val log = context.mock(classOf[StaticMethodLogger])
    StaticMethodContainer.log = log

    val adviceContainer = new EmptyAdviceProvider {
      override def getAdvice(jp: JoinPoint): Option[Advice] = {
        jp match {
          case JoinPoint(_, ClassMember(_, "withThrow", _, _), MethodEnter) =>
            Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "withThrow", _, _), MethodLeave) =>
            Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "withThrow", _, _), MethodLeaveFinally) =>
            Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case _ => None
        }
      }
    }

    val instrumentor = new Instrumentor(adviceContainer)
    val loader = new TestInstrumentingLoader(instrumentor, classOf[DummyWithThrow].getName())
    val dummyClass = loader.loadClass(classOf[DummyWithThrow].getName())
    val dummy = dummyClass.getConstructor().newInstance()

    context.checking(new Expectations() {
      one(log).logMethod(dummy, "withThrow", MethodEnter.toString())
      one(log).logMethod(dummy, "withThrow", MethodLeaveFinally.toString())
    })

    val thrown = intercept[InvocationTargetException] {
      dummyClass.getMethod("withThrow").invoke(dummy)
    }
    assert(thrown.getTargetException().getClass == classOf[Error], thrown.getTargetException())
    assert("test".equals(thrown.getTargetException().getMessage()))

    context.assertIsSatisfied()
  }

  @Test
  def joinPointInstrumentationMethodCallWithThrow() {
    val log = context.mock(classOf[StaticMethodLogger])
    StaticMethodContainer.log = log

    val adviceProvider = new EmptyAdviceProvider {
      override def getAdvice(jp: JoinPoint): Option[Advice] = {
        if (!List(BeforeCall, AfterCall, AfterCallFinally).contains(jp.kind)) return None
        jp match {
          case JoinPoint(_, ClassMember(_, "withThrow", _, _), _) =>
            Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod",
              OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case _ => None
        }
      }
    }

    val instrumentor = new Instrumentor(adviceProvider)
    val loader = new TestInstrumentingLoader(instrumentor, classOf[DummyWithThrow].getName())
    val dummyClass = loader.loadClass(classOf[DummyWithThrow].getName())
    val dummy = dummyClass.getConstructor().newInstance()

    context.checking(new Expectations() {
      one(log).logMethod(dummy, "withThrow", BeforeCall.toString())
      one(log).logMethod(dummy, "withThrow", AfterCallFinally.toString())
    })

    val thrown = intercept[InvocationTargetException] {
      dummyClass.getMethod("callWithThrow").invoke(dummy)
    }
    assert(thrown.getTargetException().getClass == classOf[Error], thrown.getTargetException())
    assert("test".equals(thrown.getTargetException().getMessage()))

    context.assertIsSatisfied()
  }

  @Test
  def joinPointInstrumentationEnterLeaveWithNestedThrow() {
    val log = context.mock(classOf[StaticMethodLogger])
    StaticMethodContainer.log = log

    val adviceProvider = new EmptyAdviceProvider {
      override def getAdvice(jp: JoinPoint): Option[Advice] = {
        jp match {
          case JoinPoint(_, ClassMember(_, "callWithThrow", _, _), MethodEnter) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "callWithThrow", _, _), MethodLeave) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "callWithThrow", _, _), MethodLeaveFinally) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case _ => None
        }
      }
    }

    val instrumentor = new Instrumentor(adviceProvider)
    val loader = new TestInstrumentingLoader(instrumentor, classOf[DummyWithThrow].getName())
    val dummyClass = loader.loadClass(classOf[DummyWithThrow].getName())
    val dummy = dummyClass.getConstructor().newInstance()

    context.checking(new Expectations() {
      one(log).logMethod(dummy, "callWithThrow", MethodEnter.toString())
      one(log).logMethod(dummy, "callWithThrow", MethodLeaveFinally.toString())
    })

    val thrown = intercept[InvocationTargetException] {
      dummyClass.getMethod("callWithThrow").invoke(dummy)
    }
    assert(thrown.getTargetException().getClass == classOf[Error], thrown.getTargetException())
    assert("test".equals(thrown.getTargetException().getMessage()))

    context.assertIsSatisfied()
  }

  @Test
  def joinPointInstrumentationEnterLeaveWithoutThrow() {
    val log = context.mock(classOf[StaticMethodLogger])
    StaticMethodContainer.log = log

    val adviceProvider = new EmptyAdviceProvider {
      override def getAdvice(jp: JoinPoint): Option[Advice] = {
        jp match {
          case JoinPoint(_, ClassMember(_, "normal", _, _), MethodEnter) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "normal", _, _), MethodLeave) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "normal", _, _), MethodLeaveFinally) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod", ThisMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case _ => None
        }
      }
    }

    val instrumentor = new Instrumentor(adviceProvider)
    val loader = new TestInstrumentingLoader(instrumentor, classOf[DummyWithThrow].getName())
    val dummyClass = loader.loadClass(classOf[DummyWithThrow].getName())
    val dummy = dummyClass.getConstructor().newInstance()

    context.checking(new Expectations() {
      one(log).logMethod(dummy, "normal", MethodEnter.toString())
      one(log).logMethod(dummy, "normal", MethodLeave.toString())
      one(log).logMethod(dummy, "normal", MethodLeaveFinally.toString())
    })

    dummyClass.getMethod("normal").invoke(dummy)

    context.assertIsSatisfied()
  }

  @Test
  def joinPointInstrumentationFieldAccess() {
    val log = context.mock(classOf[StaticMethodLogger])
    StaticMethodContainer.log = log
    val dummy = new JavaDummyInstrumented()

    val adviceProvider = new EmptyAdviceProvider {
      override def getAdvice(jp: JoinPoint): Option[Advice] = {
        jp match {
          case JoinPoint(_, ClassMember(_, "i", _, _), BeforeGetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod",
            OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "i", _, _), AfterGetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod",
            OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), JoinPointNameMethodArgument()))

          case JoinPoint(_, ClassMember(_, "i", _, _), BeforeSetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethodIntArg",
            OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), OriginalArgumentMethodArgument(1), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "i", _, _), AfterSetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethodIntArg",
            OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), OriginalArgumentMethodArgument(1), JoinPointNameMethodArgument()))

          // static
          case JoinPoint(_, ClassMember(_, "sI", _, _), BeforeGetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethodString",
            MemberClassNameMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "sI", _, _), AfterGetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethodClass",
            MemberClassMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))

          case JoinPoint(_, ClassMember(_, "sI", _, _), BeforeSetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethodStringIntArg",
            MemberClassNameMethodArgument(), MemberNameMethodArgument(), OriginalArgumentMethodArgument(0), JoinPointNameMethodArgument()))
          case JoinPoint(_, ClassMember(_, "sI", _, _), AfterSetField) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethodClassIntArg",
            MemberClassMethodArgument(), MemberNameMethodArgument(), OriginalArgumentMethodArgument(0), JoinPointNameMethodArgument()))

          case _ => None
        }
      }
    }

    context.checking(new Expectations() {
      one(log).logMethod(dummy, "i", BeforeGetField.toString())
      one(log).logMethod(dummy, "i", AfterGetField.toString())
      one(log).logMethodIntArg(dummy, "i", 3, BeforeSetField.toString())
      one(log).logMethodIntArg(dummy, "i", 3, AfterSetField.toString())

      one(log).logMethod(classOf[JavaDummyInstrumented].getName(), "sI", BeforeGetField.toString())
      one(log).logMethod(classOf[JavaDummyInstrumented], "sI", AfterGetField.toString())
      one(log).logMethodIntArg(classOf[JavaDummyInstrumented].getName(), "sI", 4, BeforeSetField.toString())
      one(log).logMethodIntArg(classOf[JavaDummyInstrumented], "sI", 4, AfterSetField.toString())
    })

    val instrumentor = new Instrumentor(adviceProvider)
    val loader = new TestInstrumentingLoader(instrumentor, classOf[DummyAccess].getName())
    val dummyAccessClass = loader.loadClass(classOf[DummyAccess].getName())
    val dummyAccess = dummyAccessClass.getConstructor().newInstance()
    dummyAccessClass.getMethod("access", classOf[JavaDummyInstrumented]).invoke(dummyAccess, dummy)

    context.assertIsSatisfied()
  }

  @Test
  def joinPointInstrumentationMethodCalls() {
    val log = context.mock(classOf[StaticMethodLogger])
    StaticMethodContainer.log = log

    val adviceProvider = new EmptyAdviceProvider {
      override def getAdvice(jp: JoinPoint): Option[Advice] = {
        if (!List(BeforeCall, AfterCall, AfterCallFinally).contains(jp.kind)) return None
        jp match {
          case JoinPoint(ClassMember(_, "invokingMethod", _, _), ClassMember(_, _, _, false), _) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethod",
            OriginalArgumentMethodArgument(0), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case JoinPoint(ClassMember(_, "invokingMethod", _, _), ClassMember(_, _, _, true), _) => Some(InvokeStaticAdvice(classOf[StaticMethodContainer].getName(), "staticMethodClass",
            MemberClassMethodArgument(), MemberNameMethodArgument(), JoinPointNameMethodArgument()))
          case _ => None
        }
      }
    }

    val iDummy: IDummy = new DummyMethodCalls();
    val instrumentor = new Instrumentor(adviceProvider)
    val loader = new TestInstrumentingLoader(instrumentor, classOf[DummyMethodCalls].getName())
    val dummyMethodCallsClass = loader.loadClass(classOf[DummyMethodCalls].getName())
    val dummyMethodCalls = dummyMethodCallsClass.getConstructor().newInstance()

    context.checking(new Expectations() {
      one(log).logMethod(iDummy, "interfaceMethod", BeforeCall.toString())
      one(log).logMethod(iDummy, "interfaceMethod", AfterCall.toString())
      one(log).logMethod(iDummy, "interfaceMethod", AfterCallFinally.toString())
      one(log).logMethod(dummyMethodCalls, "privateMethod", BeforeCall.toString())
      one(log).logMethod(dummyMethodCalls, "privateMethod", AfterCall.toString())
      one(log).logMethod(dummyMethodCalls, "privateMethod", AfterCallFinally.toString())
      one(log).logMethod(dummyMethodCalls, "normalMethod", BeforeCall.toString())
      one(log).logMethod(dummyMethodCalls, "normalMethod", AfterCall.toString())
      one(log).logMethod(dummyMethodCalls, "normalMethod", AfterCallFinally.toString())
      one(log).logMethod(dummyMethodCallsClass, "staticMethod", BeforeCall.toString())
      one(log).logMethod(dummyMethodCallsClass, "staticMethod", AfterCall.toString())
      one(log).logMethod(dummyMethodCallsClass, "staticMethod", AfterCallFinally.toString())

    })

    dummyMethodCallsClass.getMethod("invokingMethod", classOf[IDummy]).invoke(dummyMethodCalls, iDummy)

    context.assertIsSatisfied()
  }

}