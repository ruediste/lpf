package lpf.test.instrumentation;

public class DerivedDummy extends BaseDummy{

	@Override
	public void foo() {
		super.foo();
	}
	
	public void bar(){
		@SuppressWarnings("unused")
		DerivedDummy d=new DerivedDummy();
	}
	
}
