package lpf.test.instrumentation;

public class DummyMethodCalls implements IDummy{

	@Override
	public void interfaceMethod() {
		
	}
	
	public static void staticMethod(){
		
	}
	
	public void normalMethod(){}
	private void privateMethod(){}
	
	public void invokingMethod(IDummy dummy){
		
		dummy.interfaceMethod();
		privateMethod();
		staticMethod();
		normalMethod();
	}
	
}
