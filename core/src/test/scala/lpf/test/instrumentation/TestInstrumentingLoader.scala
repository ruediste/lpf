package lpf.test.instrumentation

import lpf.test.testBase._
import org.objectweb.asm._
import lpf.instrumentation._
import lpf.instrumentation.JoinPointKind._
import org.hamcrest.Matchers._
import java.lang.reflect.InvocationTargetException
import org.objectweb.asm.util.CheckClassAdapter
import lpf.hornet.ClassInformationPool

class TestInstrumentingLoader(val instrumentor: Instrumentor, val classToInstrument: String) extends ClassLoader(classOf[JavaDummyInstrumented].getClassLoader()) {

  override def loadClass(name: String): java.lang.Class[_] = TestInstrumentingLoader.this.synchronized {
    if (!name.equals(classToInstrument))
      return super.loadClass(name);

    // First, check if the class has already been loaded
    var c = findLoadedClass(name);

    //var c: Class[_] = null;
    if (c == null) {
      // use the base implementation if the class does not need to be instrumented
      if (!instrumentor.needsInstrumentation(this, name, null)) {
        println("skipped class " + name + " due to instrumentor")
        c = super.loadClass(name)
      } else {
        val cr = new ClassReader(name)
        val cw = new ClassWriter(cr, ClassWriter.COMPUTE_FRAMES)

        instrumentor.instrument(name, getClass().getClassLoader(), cr, cw)
        val bytes = cw.toByteArray()

        if (true) {
          val ccr = new ClassReader(cw.toByteArray())
          val ccw = new ClassWriter(ccr, 0)
          val ccv = new CheckClassAdapter(ccw)
          ccr.accept(ccv, ClassReader.EXPAND_FRAMES)
        }

        c = defineClass(name, bytes, 0, bytes.length)
        resolveClass(c)
      }
    }
    c
  }
}