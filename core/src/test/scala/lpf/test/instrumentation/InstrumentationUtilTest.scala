package lpf.test.instrumentation

import lpf.test.testBase.FixtureBase
import org.junit.Test
import lpf.instrumentation.InstrumentationUtil
import lpf.instrumentation.ClassInformationAdjusterBase
import meta.java.lang.ClassInformationAdjuster

class InstrumentationUtilTest extends FixtureBase {

  @Test
  def searchClass() {
    val adjuster = InstrumentationUtil.searchClass("java.lang.foo.bar", "ClassInformationAdjuster", classOf[ClassInformationAdjusterBase]).headOption
    assert(adjuster.isDefined)
    assert(adjuster.get.getClassLoader() == classOf[ClassInformationAdjuster].getClassLoader(), "ClassLoaders equal")
    assert(adjuster.get == classOf[ClassInformationAdjuster], "Classes equal")
  }
}