package lpf.test.instrumentation

import lpf.test.testBase._
import org.junit.Assert._
import org.objectweb.asm.commons.LocalVariablesSorter
import org.objectweb.asm.Type
import org.jmock.Expectations._
import lpf.instrumentation.LocalVariablePool
import org.scalacheck.Commands
import org.scalacheck.Gen

class Counter {
  var count = 0
  def inc = count += (if (count >10) 2 else 1)
  def dec = count -= 2
  def get = count
}
class LocalVariablePoolTestNN extends Commands {
  val counter = new Counter
  case class State(n: Int)
  def initialState() = {
    State(counter.get)
  }

  case object Inc extends Command {
    def run(s: State) = counter.inc
    def nextState(s: State) = State(s.n + 1)
    preConditions += (s => true)
  }

  case object Dec extends Command {
    def run(s: State) = counter.dec
    def nextState(s: State) = State(s.n - 1)
  }

  case object Get extends Command {
    def run(s: State) = {
      assert(counter.get == s.n)
    }
    def nextState(s: State) = s
  }
  def genCommand(s: State): Gen[Command] = Gen.oneOf(Inc, Dec, Get)
}