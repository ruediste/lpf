package lpf.test

import lpf.test.testBase.FixtureBase
import org.junit.Test
import lpf.visual.JLpfPane
import lpf.visual.Border

class LpfRootPaneLikeTest extends FixtureBase {

  @Test
  def visualInMultipleRootPanes() {
    val visual = Border()
    val pane1 = JLpfPane(visual)
    val t=intercept[Error] {
      val pane2 = JLpfPane(visual)
    }
    assert(t.getMessage().equals("Visual already in another root pane"))
  }
}