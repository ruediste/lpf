package lpf.test.binding

class BindingDummy(var i: Int) {
  def this() = this(0)
  var p: Int = 0
  def setI(value: Int) = i = value
  var source = Iterable.empty[Int]
  var target = Iterable.empty[Int]

  var dummyList = Iterable.empty[BindingDummy]
}
