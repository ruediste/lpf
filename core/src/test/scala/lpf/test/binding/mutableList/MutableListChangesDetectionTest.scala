package lpf.test.binding.mutableList

import lpf.test.testBase.FixtureBase
import org.junit.Test
import scala.collection.mutable.ListBuffer
import lpf.dependency.HornetEventListener
import lpf.Referencer
import lpf.hornet.ClassInformationPool
import scala.collection.generic.GenericCompanion
import lpf.dependency.HornetReadEventDispatcher
import scala.collection.generic.Growable
import scala.collection.GenTraversableOnce
import scala.collection.mutable.ArrayBuffer
import lpf.dependency.ChangeNotifier

class MutableListChangesDetectionTest extends FixtureBase with Referencer {

  @Test
  def ListBufferAddDetectedWithFactory() = {
    testBuffer(ListBuffer[Int]())
  }

  @Test
  def ListBufferAddDetectedWithNew() = {
    testBuffer(new ListBuffer[Int]())
  }

  @Test
  def ArrayBufferAddDetectedWithFactory() = {
    testBuffer(ArrayBuffer[Int]())
  }

  @Test
  def ArrayBufferAddDetectedWithNew() = {
    testBuffer(new ArrayBuffer[Int]())
  }

  def testBuffer(buffer: Growable[Int] with GenTraversableOnce[Int]) {
    var notified = false

    val notifier = ChangeNotifier()

    notifier.notifyOfChanges(notified = true, buffer.size)

    buffer += 3

    assert(notified)
  }

  @Test
  def javaListAddDetected() {
    val list = new java.util.LinkedList[Int]()
    var notified = false

    val notifier = ChangeNotifier()
    notifier.notifyOfChanges(notified = true, list.size)

    list.add(3)

    assert(notified)
  }

  @Test
  def genericCompanion_Apply_isObjectHornetFactory() = {
    val info = ClassInformationPool.getClassInformation(classOf[GenericCompanion[List]].getName())
    assert(info.getMethods().filter(pair => pair._1.name.equals("apply")).values.head.exists(_.isObjectHornetFactory))
  }
}