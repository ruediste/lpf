package lpf.test.binding.twoWayBinding

import lpf.test.testBase.FixtureBase
import org.junit.Test
import lpf.binding.TwoWayBinding
import lpf.Referencer
import lpf.dependency.HornetReadEventDispatcher
import lpf.dependency.HornetWrittenEventDispatcher
import lpf.test.testBase.Expectations
import org.hamcrest.Matchers._
import lpf.binding.BindingBase
import org.junit.Ignore
import lpf.StandaloneDispatcher

class TwoWayBindingTest extends FixtureBase {
  val referencer = new Object() with Referencer

  @Test
  def testInitialization() {
    val log = context.mock(classOf[Logger])
    val hornetX = new Object()
    val hornetY = new Object()
    val queue = StandaloneDispatcher()

    context.checking(new Expectations {
      one(log)("getAtoB")
      one(log)("setAtoB")
      one(log)("getBtoA")
    })
    val binding = new TwoWayBinding[Unit, Unit](queue, queue, false, referencer,
      () => { HornetReadEventDispatcher.hornetWillBeRead(hornetX); log("getAtoB") },
      (_) => { HornetWrittenEventDispatcher.hornetWritten(hornetY); log("setAtoB") },
      () => { HornetReadEventDispatcher.hornetWillBeRead(hornetY); log("getBtoA") },
      (_) => { HornetWrittenEventDispatcher.hornetWritten(hornetX); log("setBtoA") })
    context.assertIsSatisfied()
  }

  @Test
  def testUpdateWithQueue() {
    val log = context.mock(classOf[Logger])
    val hornetA = new Object()
    val hornetB = new Object()
    val queue = StandaloneDispatcher()

    context.checking(new Expectations {
      // initial
      one(log)("getAtoB")
      one(log)("setAtoB")
      one(log)("getBtoA")

      // update of hornetA
      one(log)("getAtoB")
      one(log)("setAtoB")
      one(log)("getBtoA")
    })
    val binding = new TwoWayBinding[Unit, Unit](queue, queue, false, referencer,
      () => { HornetReadEventDispatcher.hornetWillBeRead(hornetA); log("getAtoB") },
      (_) => { HornetWrittenEventDispatcher.hornetWritten(hornetB); log("setAtoB") },
      () => { HornetReadEventDispatcher.hornetWillBeRead(hornetB); log("getBtoA") },
      (_) => { HornetWrittenEventDispatcher.hornetWritten(hornetA); log("setBtoA") })
    HornetWrittenEventDispatcher.hornetWritten(hornetA)
    queue.tryExecuteSingle
    context.assertIsSatisfied()
  }
}