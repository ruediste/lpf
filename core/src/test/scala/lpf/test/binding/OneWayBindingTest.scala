/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.test.binding

import lpf.binding._
import lpf.test.testBase._
import scala.ref.WeakReference
import lpf.Referencer
import org.junit.Assert._
import org.jmock.Expectations._
import lpf.dependency._
import language.implicitConversions
import lpf.StandaloneDispatcher
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class OneWayBindingTest2 extends FunSuite with ShouldMatchers {
  implicit var theReferencer = new Object() with Referencer
  val dispatcher = StandaloneDispatcher()
  var ia = 1;
  var ib = 1;
  test("loop detected") {
    OneWayBinding(dispatcher)(ia = ib)
    OneWayBinding(dispatcher)(ib = ia)

    ia = 2;

    dispatcher.tryExecuteSingle() should equal(true)
    dispatcher.tryExecuteSingle() should equal(true)
    val t = intercept[Error](dispatcher.tryExecuteSingle())
  }
}

class OneWayBindingTest extends FixtureBase {
  private class BindingDummy {
    var i, p: Int = 0
    def setI(value: Int) = i = value
    var otherDummy: Option[BindingDummy] = None
  }

  val queue = StandaloneDispatcher()

  @volatile implicit var theReferencer = new Object() with Referencer

  @Test def BindingIsReferenced(): Unit = {
    val source = new BindingDummy()
    val target = new BindingDummy()

    val ref = new WeakReference(OneWayBinding(queue)(target.i = source.i))
    System.gc()
    assert(ref.get.isDefined, "binding was collected during GC")
  }

  @Test def twoBindingsAreReferenced(): Unit = {
    val source = new BindingDummy()
    val target = new BindingDummy()
    val refI = new WeakReference(OneWayBinding(queue)(target.i = source.i))
    val refP = new WeakReference(OneWayBinding(queue)(target.p = source.p))

    System.gc()

    assert(refI.get.isDefined, "first binding was collected")
    assert(refP.get.isDefined, "second binding was collected")
  }

  @Test def BindingCanBeFreed(): Unit = {
    val source = new BindingDummy()
    val target = new BindingDummy()
    val ref = new WeakReference(OneWayBinding(queue)(target.i = source.i))
    theReferencer = null
    System.gc()
    assert(!ref.get.isDefined, "binding was not freed")
  }

  @Test def BindingCanBeDisposed(): Unit = {
    val source = new BindingDummy()
    val target = new BindingDummy()
    val ref = new WeakReference(OneWayBinding(queue)(target.i = source.i))
    ref.get.get.dispose()
    System.gc()
    assert(!ref.get.isDefined, "binding was not freed")
  }

  @Test def dummyIsInstrumented() {
    var notified = false
    val dummy = new BindingDummy();

    val notifier = ChangeNotifier()
    notifier.notifyOfChanges(notified = true, dummy.i)
    dummy.i = 2
    assertTrue(notified)
  }

  @Test def bindingWorks(): Unit = {
    val source = new BindingDummy()
    val target = context.mock(classOf[BindingDummy])

    context.checking(new Expectations {
      one(target).setI(0) // for the initial update
      one(target).setI(1) // for the change
    })

    OneWayBinding(queue)(target.setI(source.i))

    source.i = 1;

    queue.executeAll

    context.assertIsSatisfied()
  }

  @Test def targetDependencyChangeIsHandled(): Unit = {
    val source = new BindingDummy()
    val dummy = new BindingDummy()
    val target = context.mock(classOf[BindingDummy])

    OneWayBinding(queue)(dummy.otherDummy.foreach(_.setI(source.i)))
    context.assertIsSatisfied()

    source.i = 1;

    context.checking(new Expectations {
      one(target).setI(1) // for the change
    })

    dummy.otherDummy = Some(target)

    queue.executeAll

    context.assertIsSatisfied()
  }

  @Test def bindingIsDisabledAfterBeeingDisposed(): Unit = {
    val source = new BindingDummy()
    val target = context.mock(classOf[BindingDummy])

    context.checking(new Expectations {
      one(target).setI(0) // for the initial update
    })

    // don't forget to simulate the instrumentation
    val binding = OneWayBinding(queue)(target.setI(source.i))

    binding.dispose()

    source.i = 1;

    queue.executeAll

    context.assertIsSatisfied()
  }

  @Test def bindingWorksTwice(): Unit = {
    val source = new BindingDummy()
    val target = context.mock(classOf[BindingDummy])

    context.checking(new Expectations {
      one(target).setI(0) // for the initial update
      one(target).setI(1) // for the first change
      one(target).setI(2) // for the second change
    })

    OneWayBinding(queue)(target.setI(source.i))

    source.i = 1;

    queue.executeAll

    source.i = 2;

    queue.executeAll

    context.assertIsSatisfied()
  }

  @Test def repeatedSourceChangesCauseUpdateOnce {
    val source = new BindingDummy()
    val target = context.mock(classOf[BindingDummy])

    // initial update
    context.checking(new Expectations {
      one(target).setI(0) // for the initial update
    })

    OneWayBinding(queue)(target.setI(source.i))

    // do two updates
    source.i = 1
    source.i = 2

    // now perform the processing, such that an update should happen
    context.checking(new Expectations {
      one(target).setI(2)
    })
    queue.executeAll()
    context.assertIsSatisfied()
  }

  @Test def bindingWithBindingQueue(): Unit = {
    val source = new BindingDummy()
    val target = context.mock(classOf[BindingDummy])

    // initial update
    context.checking(new Expectations {
      one(target).setI(0) // for the initial update
    })

    OneWayBinding(queue)(target.setI(source.i))

    // check that no update has been performed yet
    context.assertIsSatisfied()

    // repeat processing, nothing should happen
    queue.executeAll()
    context.assertIsSatisfied()

    // change the source, such that the binding enters the queue
    source.i = 1;

    // simulate the instrumentation
    context.assertIsSatisfied() // nothing should have happend

    // now perform the processing, such that an update should happen
    context.checking(new Expectations {
      one(target).setI(1) // for the initial update
    })
    queue.executeAll()
    context.assertIsSatisfied()
  }
}
