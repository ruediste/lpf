package lpf.test.binding

import lpf.test.testBase.FixtureBase
import lpf.binding.Mapping
import org.junit.Test

class MappingTest extends FixtureBase {

  @Test
  def integerMapping(){
    val mapping=new Mapping[Int, AnyRef]()
    val obj1=new Object()
    val obj2=new Object()
    val obj3=new Object()
    mapping.reDefine({definer=>
      definer.getOrElseDefine(1, obj1)
      definer.getOrElseDefine(2, obj2)
      definer.getOrElseDefine(3, obj3)
    })
    
    assert(mapping.inverseGet(obj1).get==1, "should be 1")
  }
}