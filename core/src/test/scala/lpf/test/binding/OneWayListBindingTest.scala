/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.test.binding

import lpf.binding._
import lpf.test.testBase._
import lpf.Referencer
import org.junit.Assert._
import lpf.dependency.HornetEventListener
import language.implicitConversions
import lpf.dependency.HornetWrittenEventDispatcher
import lpf.dependency.ChangeNotifier
import lpf.StandaloneDispatcher

class OneWayListBindingTest extends FixtureBase {

  implicit var theReferencer = new Object() with Referencer

  @Test def BindingDummyGetsInstrumented(): Unit = {
    var gotNotified = false

    var target = new BindingDummy()

    val notifier = ChangeNotifier()

    val theResult = notifier.notifyOfChanges(gotNotified = true, target.i)
    target.i = 2
    assert(gotNotified, "notification not sent")
  }

  @Test def immutableDirectBinding(): Unit = {
    val dummy = new BindingDummy();
    val queue = StandaloneDispatcher()

    dummy.source = List[Int](1, 2, 3)
    dummy.target = List()

    OneWayBinding(queue)(dummy.target = dummy.source)
    queue.executeAll

    val newList = List[Int](4, 7)
    dummy.source = newList
    queue.executeAll

    assert(dummy.target == newList)
  }

  @Test def listBindingRepeatedUpdates() {
    val dummy = new BindingDummy();
    val queue = StandaloneDispatcher()

    dummy.source = List[Int](1, 2, 3)
    var updates = 0

    OneWayListBinding.bind(queue)(dummy.source)(new BindingDummy(_))(_ => updates += 1)

    updates=0
    dummy.source = List[Int](4, 7)
    dummy.source = List[Int](5, 6)

    queue.executeAll

    assertEquals(1, updates) 
  }

  @Test def cachingListBindingRepeatedUpdates() {
    val dummy = new BindingDummy();
    val queue = StandaloneDispatcher()

    dummy.source = List[Int](1, 2, 3)
    var updates = 0

    OneWayListBinding.cache(queue)(dummy.source)(new BindingDummy(_))(_ => updates += 1)

    updates=0
    dummy.source = List[Int](4, 7)
    dummy.source = List[Int](5, 6)

    queue.executeAll

    assertEquals(1, updates) 
  }
  @Test def bindingDummySourceIsInstrumented() {
    val dummy = new BindingDummy();
    val notifier = ChangeNotifier()
    var notified = false
    notifier.notifyOfChanges(notified = true, dummy.source, true)
    assert(!notified)
    dummy.source = List()
    assert(notified)
  }

  /**
   * check a simple mapping binding
   */
  @Test def immutableMappingBinding(): Unit = {
    val dummy = new BindingDummy();
    val queue = StandaloneDispatcher()

    dummy.source = List[Int](1, 2, 3)
    dummy.target = List()

    OneWayListBinding.bind(queue)(dummy.source)(new BindingDummy(_))(dummy.dummyList = _)

    assertEquals(3, dummy.dummyList.toList.length)
    assertEquals(dummy.source, dummy.dummyList map (_.i))

    val newList = List[Int](4, 7)
    dummy.source = newList

    queue.executeAll

    assertEquals(2, dummy.dummyList.toList.length)
    assertEquals(newList, dummy.dummyList map (_.i))
  }

  /**
   * check a simple caching binding
   */
  @Test def immutableMappingChagedSimple(): Unit = {
    val dummy = new BindingDummy();
    val queue = StandaloneDispatcher()

    dummy.source = List[Int](1, 2, 3)
    dummy.target = List()

    val binding = OneWayListBinding.cache(queue)(dummy.source)(new BindingDummy(_))(dummy.dummyList = _)

    val newList = List[Int](4, 7)
    dummy.source = newList

    queue.executeAll()

    assertEquals(2, dummy.dummyList.toList.length)
    assertEquals(newList, dummy.dummyList map (_.i))
    //println(binding.mapping)
    assertEquals(newList, dummy.dummyList.map(binding.mapping.inverseGet(_).get))
  }

  /**
   * check if caching works
   */
  @Test def immutableMappingCached(): Unit = {
    val dummy = new BindingDummy();
    val queue = StandaloneDispatcher()

    dummy.source = List[Int](1, 2, 3)
    // dummy.target = List()

    OneWayListBinding.cache(queue)(dummy.source)(new BindingDummy(_))(dummy.dummyList = _)

    dummy.source = List[Int](4, 7)

    queue.executeAll

    val firstList = dummy.dummyList

    dummy.source = List[Int](7, 4)

    queue.executeAll

    assertTrue(firstList.toList(0) == dummy.dummyList.toList(1))
    assertTrue(firstList.toList(1) == dummy.dummyList.toList(0))
  }

  /**
   * check if duplicate items get different cache values
   */
  @Test def immutableMappingCachingDuplicateItems(): Unit = {
    val dummy = new BindingDummy();
    val queue = StandaloneDispatcher()

    dummy.source = List[Int](1, 2, 3)
    dummy.target = List()

    OneWayListBinding.cache(queue)(dummy.source)(new BindingDummy(_))(dummy.dummyList = _)

    dummy.source = List[Int](4, 4)
    queue.executeAll

    val firstList = dummy.dummyList
    assertTrue("elements must be different", firstList.toList(0) != firstList.toList(1))

    dummy.source = List[Int](4, 4)
    queue.executeAll

    val secondList = dummy.dummyList

    assertTrue(firstList.toList(0) == secondList.toList(0))
    assertTrue(firstList.toList(1) == secondList.toList(1))
  }
}

