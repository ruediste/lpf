package lpf.test.event

import lpf.event._
import lpf.test.testBase._
import lpf.Referencer
import lpf.Element
import lpf.element.Label
import lpf.event._
import lpf.Visual
import lpf.visual.Border
import scala.ref.WeakReference
import org.junit.Ignore
import org.junit.rules.Timeout

class VisualEventDispatcherTest {
  var theReferencer=new Object() with Referencer
    
  @Test def testFunctionIsFreedWithVisual(){
    var visual=Border()
    val eventType=MouseEventType.pressed
    var func= (e: MouseEvent)=>println("pressed")
    VisualEventDispatcher.register(visual, eventType)(func)
    
    val ref=WeakReference(func)
    
    assert(!ref.get.isEmpty)
    
    func=null
    
    System.gc()
    assert(!ref.get.isEmpty)
    
    visual=null
    
    System.gc()
    assert(ref.get.isEmpty, "ref should be empty now")
  }
}