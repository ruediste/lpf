/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.test.event

import lpf.event._
import lpf.test.testBase._
import lpf.StrongEvent
import lpf.WeakEvent
import lpf.Referencer
import lpf.StrongEventNoArgs
import lpf.WeakEventNoArgs
import scala.ref.WeakReference
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(classOf[JUnit4])
class EventTest extends FixtureBase {

	private class EventReceptor{
		def receive(i: Int)(id: String): Unit={}
		def receive2(id: String): Unit ={}
	}
	
	@Test def strongEventGetsFired(): Unit ={
		val event=StrongEvent[Int]
		val receptor=context.mock(classOf[EventReceptor])
		event+= (i=>receptor.receive(i)("first"))
		event+= receptor.receive(5)("second")
		
		// check that nothing happend yet
		context.assertIsSatisfied()
		
		context.checking(new Expectations{
			one(receptor).receive(3)("first")
			one(receptor).receive(5)("second")
		})
		
		event(3)
		context.assertIsSatisfied()
	}
	
	@Test def strongEventRemoval(): Unit ={
		val event=StrongEvent[Int]
		val receptor=context.mock(classOf[EventReceptor])
		val handle=event+= (i=>receptor.receive(i)("first"))
		
		context.checking(new Expectations{
			one(receptor).receive(3)("first")
		})
		
		event(3)
		event-=handle
		event(4)
		
		context.assertIsSatisfied()
	}
	
	@Test def strongNoArgsEventGetsFired(): Unit ={
		val event=StrongEventNoArgs()
		
		val receptor=context.mock(classOf[EventReceptor])
		event.addStrongF(()=>receptor.receive2("first"))
		event.addStrongF((x: Unit)=>receptor.receive2("third"))
		event+= receptor.receive2("second")
		event+= {
			receptor.receive2("fourth")
			receptor.receive2("fifth")
			
		}
		
		// check that nothing happend yet
		context.assertIsSatisfied()
		
		context.checking(new Expectations{
			one(receptor).receive2("first")
			one(receptor).receive2("third")
			one(receptor).receive2("second")
			one(receptor).receive2("fourth")
			one(receptor).receive2("fifth")
		})
		
		event()
		
		context.assertIsSatisfied()
	}
	
	@Test def strongNoArgsRemoval(): Unit ={
		val event=StrongEventNoArgs()
		
		val receptor=context.mock(classOf[EventReceptor])
		val handle= event+=receptor.receive2("first")
	
		
		// check that nothing happend yet
		context.assertIsSatisfied()
		
		context.checking(new Expectations{
			one(receptor).receive2("first")
		})
		
		event()
		event-=handle
		event()
		
		context.assertIsSatisfied()
	}
	
	
	def equalityOfEventHelper(): String=>Unit ={return (x=>println(x))}
	
	@Test def equalityOfEvents(): Unit ={
		val receptor=new EventReceptor();
		val first=receptor.receive _
		val second=receptor.receive _
		
		// the same object is equal and identical
		assert(first==first)
		assert(first.equals(first))
		
		// two different functions, which have the same definition
		// are neither identical nor equal
		assert(first!=second)
		assert(!first.equals(second))
		
		// even if the same code creates the two function instances,
		// they are neither identical nor equal
		assert(equalityOfEventHelper()!=equalityOfEventHelper())
		assert(!equalityOfEventHelper().equals(equalityOfEventHelper()))
	}

	
	@Test def weakEventGetsFired(): Unit ={
		val event=WeakEvent[Int]
		val receptor=context.mock(classOf[EventReceptor])
		implicit val theReferencer=new Object() with Referencer
		
		event+= (i=>receptor.receive(i)("first"))
		event+= receptor.receive(5)("second")

		// check that nothing happend yet
		context.assertIsSatisfied()

		context.checking(new Expectations{
			one(receptor).receive(3)("first")
			one(receptor).receive(5)("second")
		})
		
		System.gc();
		
		event(3)
		context.assertIsSatisfied()
	}
	
	@Test def weakEventGetsGarbageCollected(): Unit ={
		val event=WeakEvent[Int]
		val receptor=context.mock(classOf[EventReceptor])
		var plainReceptor=new EventReceptor()
		
		implicit var theReferencer=new Object() with Referencer
		
		event+= (i=>receptor.receive(i)("first"))
		val handle=event+= receptor.receive(5)("second")
		event+=plainReceptor.receive2("third")
		
		val plainReceptorRef=new WeakReference(plainReceptor)
		plainReceptor=null;

		// check that nothing happend yet
		context.assertIsSatisfied()

		// keep a weak reference to the referencer
		val weakRef=new WeakReference(theReferencer)
		
		// set the referencer to null, such that the function handles can be collected
		theReferencer=null
		
		System.gc()
		
		event(3)
		
		// nothing should have happend
		context.assertIsSatisfied()
		
		// the plainReceptor should have been collected
		assert(plainReceptorRef.get.isEmpty)
		
		// the referencer should have been collected
		assert(weakRef.get.isEmpty)
		
		// the handle should be invalid
		val thrown=intercept[Exception]{event-=handle}
		assert(thrown.getMessage().equals("Handle not defined"))
	}
	
	
	@Test def weakEventRemoval(): Unit ={
		val event=WeakEvent[Int]
		val receptor=context.mock(classOf[EventReceptor])
		implicit val theReferencer=new Object() with Referencer
		
		val handle=event+= (i=>receptor.receive(i)("first"))

		// check that nothing happend yet
		context.assertIsSatisfied()

		context.checking(new Expectations{
			one(receptor).receive(3)("first")
		})
		
		event(3)
		event-=handle
		event(4)
		context.assertIsSatisfied()
	}
	
	@Test def weakEventNoArgsGetsFired(): Unit ={
		val event=WeakEventNoArgs()
		val receptor=context.mock(classOf[EventReceptor])
		implicit val theReferencer=new Object() with Referencer
		
		event+= {receptor.receive2("first")}
		event+= receptor.receive2("second")
		
		// check that nothing happend yet
		context.assertIsSatisfied()

		
		context.checking(new Expectations{
			one(receptor).receive2("first")
			one(receptor).receive2("second")
		})
		
		event()
		context.assertIsSatisfied()
	}
	
	@Test def weakEventNoArgsRemoval(): Unit ={
		val event=WeakEventNoArgs()
		val receptor=context.mock(classOf[EventReceptor])
		implicit val theReferencer=new Object() with Referencer
		
		val handle=event+= {receptor.receive2("first")}
		
		// check that nothing happend yet
		context.assertIsSatisfied()

		
		context.checking(new Expectations{
			one(receptor).receive2("first")
		})
		
		event()
		event-=handle
		event()
		context.assertIsSatisfied()
	}
	
}
