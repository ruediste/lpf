package lpf.test

import lpf.Dispatcher
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.time.Span
import org.scalatest.time.Millis
import lpf.StandaloneDispatcher

class DispatcherTest extends FunSuite with ShouldMatchers with TimeLimitedTests {

  val timeLimit = Span(2000, Millis)

  test("invoke") {
    val dispatcher = StandaloneDispatcher(Thread.currentThread())
    var invoked = false
    dispatcher.invoke(invoked = true)
    invoked should equal(false)
    dispatcher.tryExecuteSingle()
    invoked should equal(true)
  }
  
  test("apply") {
    val dispatcher = StandaloneDispatcher(Thread.currentThread())
    var invoked = false
    dispatcher(invoked = true)
    invoked should equal(false)
    dispatcher.tryExecuteSingle()
    invoked should equal(true)
  }

  test("executeAll") {
    val dispatcher = StandaloneDispatcher(Thread.currentThread())
    var invoked1 = false
    var invoked2 = false
    dispatcher.invoke(invoked1 = true)
    dispatcher.invoke(invoked2 = true)
    assert(!invoked1)
    assert(!invoked2)
    dispatcher.executeAll()
    assert(invoked1)
    assert(invoked2)
  }

  test("invokeAndWait") {
    var dispatcher: StandaloneDispatcher = null
    val t = new Thread { override def run { dispatcher.run } }
    dispatcher = StandaloneDispatcher(t)
    t.start()

    var invoked = false
    dispatcher.invokeAndWait(invoked = true)
    assert(invoked === true)
  }

  test("invokeAndWait on same thread") {
    var invoked = false
    val dispatcher = StandaloneDispatcher(Thread.currentThread())
    intercept[Error](dispatcher.invokeAndWait(invoked = true))
  }

  test("verifyAccess() fails from different thread") {
    val t = new Thread()
    val dispatcher = StandaloneDispatcher(t)
    intercept[AssertionError](dispatcher.verifyAccess)
  }

  test("verifyAccess() works from same thread") {
    val dispatcher = StandaloneDispatcher(Thread.currentThread())
    dispatcher.verifyAccess
  }

  test("cannot run dispatcher on wrong thread") {
    val t = new Thread()
    val dispatcher = StandaloneDispatcher(t)
    intercept[AssertionError](dispatcher.run())
  }

  test("dispatcher can be stopped") {
    val d = StandaloneDispatcher(Thread.currentThread())
    var b1 = false
    var b2 = false
    d.invoke(b1 = true)
    d.invoke(d.stop())
    d.invoke(b2 = true)

    d.run()

    b1 should equal(true)
    b2 should equal(false)
  }

  test("invocation order maintained") {
    val d = StandaloneDispatcher(Thread.currentThread())
    var nr = 1;
    (1 until 100) foreach { i => d.invoke { i should equal(nr); nr += 1 } }
    d.executeAll()
    nr should equal(100)
  }

  test("try execute single") {
    val d = StandaloneDispatcher(Thread.currentThread())
    d.tryExecuteSingle should equal(false)
    d.invoke()
    d.tryExecuteSingle should equal(true)
    d.tryExecuteSingle should equal(false)
  }
  
  test("tasks pending") {
    val d = StandaloneDispatcher(Thread.currentThread())
    d.tasksPending should equal(false)
    d.invoke()
    d.tasksPending should equal(true)
    d.tryExecuteSingle
    d.tasksPending should equal(false)
  }
}