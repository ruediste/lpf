/**
 * *****************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 *
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 *
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 * ****************************************************************************
 */
package lpf.test

import lpf._
import lpf.test.testBase._
import org.junit.Test
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

abstract class TestNode extends TreeNode[TestNode, ParentTestNode] {

}

class LeafTestNode extends TestNode {
  type Self = LeafTestNode
  def self = this
}

trait ParentTestNode extends TestNode with ParentTreeNodeLike[TestNode, ParentTestNode] {}

class SingleChildTestNode extends TestNode
  with ParentTestNode
  with SingleChildNodeLike[TestNode, ParentTestNode] {

  override type Self = SingleChildTestNode
  override def self = this
}

class MultiChildrenTestNode extends TestNode
  with ParentTestNode
  with MultiChildrenNodeLike[TestNode, ParentTestNode] {

  override type Self = MultiChildrenTestNode
  override def self = this
}

class TreeNodeTest2 extends FunSuite with ShouldMatchers {
  test("subTreePre no child") {
    val root = new MultiChildrenTestNode()
    
    root.subTreePre should equal(List(root))
  }
  
  test("subTreePre single child") {
    val root = new MultiChildrenTestNode()
    val l = new SingleChildTestNode
    root.addChild(l)
    
    root.subTreePre should equal(List(root,l))
  }
  
  test("subTreePre two children") {
    val root = new MultiChildrenTestNode()
    val l1 = new SingleChildTestNode
    root.addChild(l1)
    
    val l2 = new SingleChildTestNode
    root.addChild(l2)
    
    root.subTreePre should equal(List(root,l1,l2))
  }
  
  test("subTreePre sub children") {
    val root = new MultiChildrenTestNode()
    val l1 = new MultiChildrenTestNode
    root.addChild(l1)

    val l11 = new MultiChildrenTestNode
    l1.addChild(l11)

    val l2 = new MultiChildrenTestNode
    root.addChild(l2)

    val l22 = new SingleChildTestNode
    l2.addChild(l22)
    
    root.subTreePre should equal(List(root,l1,l11,l2,l22))
  }
}

class TreeNodeTest extends FixtureBase {

  @Test def singleChild_addedAsChildOf_isCalledOnNewChild(): Unit = {
    val leaf = context.mock(classOf[LeafTestNode])
    val parent = new SingleChildTestNode()

    context.checking(new Expectations {
      one(leaf).addedAsChildOf(parent)
    })

    parent.child = leaf

    context.assertIsSatisfied()
  }

  @Test def singleChild_ParentLinkIsSet(): Unit = {
    val leaf = new LeafTestNode()
    val parent = new SingleChildTestNode()

    assert(leaf.parent.isEmpty)
    parent.child = leaf
    assert(leaf.parent.isDefined, "Parent not defined")
    assert(leaf.parent.get == parent)
  }

  @Test def singleChild_transitiveChildren(): Unit = {
    val leaf = new LeafTestNode()
    val parent = new SingleChildTestNode()
    val child = new SingleChildTestNode()

    parent.child = child
    child.child = leaf

    assert(leaf.transitiveChildren.isEmpty)
    assert(child.transitiveChildren.head == leaf)
    assert(parent.transitiveChildren.size == 2)
  }

  @Test def singleChild_nodeIsRemovedFromOldParent(): Unit = {
    val leaf = new LeafTestNode()
    val newParent = new SingleChildTestNode()
    val oldParent = new SingleChildTestNode()

    oldParent.child = leaf
    assert(oldParent.child.isDefined)
    assert(oldParent.child.get == leaf)
    assert(leaf.parent.get == oldParent, "parent of the child is NOT set initially")
    assert(newParent.child.isEmpty)

    newParent.child = leaf

    assert(newParent.child.get == leaf)
    assert(leaf.parent.isDefined, "parent not defined")
    assert(leaf.parent.get == newParent)

    assert(oldParent.child.isEmpty)
  }

  @Test def singleChild_getChildren(): Unit = {
    val leaf = new LeafTestNode()
    val parent = new SingleChildTestNode()

    assert(parent.children.isEmpty)

    parent.child = leaf

    assert(parent.children.head == leaf)
  }

  @Test def multiChildren_addedAsChildOf_isCalledOnNewChild(): Unit = {
    val leaf = context.mock(classOf[LeafTestNode])
    val parent = new MultiChildrenTestNode()

    context.checking(new Expectations {
      one(leaf).addedAsChildOf(parent)
    })

    parent.addChild(leaf)

    context.assertIsSatisfied()
  }

  @Test def multiChildren_parentLinkIsSet(): Unit = {
    val leaf = new LeafTestNode()
    val parent = new MultiChildrenTestNode()

    assert(leaf.parent.isEmpty)
    parent.addChild(leaf)
    assert(leaf.parent.isDefined, "Parent not defined")
    assert(leaf.parent.get == parent)
  }

  @Test def multiChildren_nodeIsRemovedFromOldParent(): Unit = {
    val leaf = new LeafTestNode()
    val newParent = new MultiChildrenTestNode()
    val oldParent = new MultiChildrenTestNode()

    oldParent.addChild(leaf)
    assert(oldParent.children.head == leaf)
    assert(leaf.parent.get == oldParent, "parent of the child is NOT set initially")
    assert(newParent.children.isEmpty)

    newParent.addChild(leaf)

    assert(newParent.children.head == leaf)
    assert(leaf.parent.isDefined, "parent not defined")
    assert(leaf.parent.get == newParent)

    assert(oldParent.children.isEmpty)
  }

}
