package lpf.test.hornet

import lpf.hornet._
import lpf.test.testBase.FixtureBase
import lpf.test.instrumentation.TestInstrumentingLoader
import lpf.instrumentation.Instrumentor
import org.junit.Test

class ListInstrumentationTest extends FixtureBase {

  @Test
  def addTest(){
    val adviceProvider=new HornetAdviceProvider();
    val instrumentor=new Instrumentor(adviceProvider);
    val loader=new TestInstrumentingLoader(instrumentor, classOf[Dummy].getName());
    val dummyClass=loader.loadClass(classOf[Dummy].getName());
    val dummy=dummyClass.newInstance();
    dummyClass.getMethod("readList").invoke(dummy);
  }
}