package lpf.test.hornet;

import java.util.LinkedList;
import java.util.List;

public class Dummy {

	public List<Integer> aList = new LinkedList<Integer>();
	
	public int readList(){
		return aList.size();
	}
	
	public void writeList(){
		aList.add(2);
	}
}
