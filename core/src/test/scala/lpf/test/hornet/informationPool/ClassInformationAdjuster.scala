package lpf.test.hornet.informationPool

import lpf.instrumentation._
import lpf.hornet._

class ClassInformationAdjuster extends ClassInformationAdjusterBase{
 def adjustClassInformation(info: ClassInformation) {
    if (info.name.equals(classOf[DerivedDummy].getName())) {
      for (m <- info.declaredMethods)
        m match {
          case MethodInfo("derivedMethod", _, _) => m.readsObjectHornet = true
          case _ =>
        }
    }
  }
}