package lpf.test.hornet.informationPool

import lpf.test.testBase.FixtureBase
import org.junit.Test
import org.junit.Assert._
import java.io.IOException
import lpf.hornet._
import org.objectweb.asm.Type

class ClassInformationPoolTest extends FixtureBase {

  @Test
  def inexistentClass() {
    val thrown = intercept[Error](ClassInformationPool.getClassInformation(classOf[BaseDummy].getName() + "$$fooBar"))
    assert(thrown.getMessage().startsWith("could not "))
  }

  @Test
  def baseInformation() {
    val info = ClassInformationPool.getClassInformation(classOf[BaseDummy].getName())

    assertEquals(classOf[BaseDummy].getName(), info.name)
    assertSetEquals[MethodInfo](
      Set(
        MethodInfo("<init>", Type.VOID_TYPE, Seq()),
        MethodInfo("publicMethod", Type.VOID_TYPE, Seq()),
        MethodInfo("publicMethod", Type.VOID_TYPE, Seq(Type.INT_TYPE)),
        MethodInfo("interfaceMethod", Type.VOID_TYPE, Seq())),
      info.declaredMethods)

    assert(ClassInformationPool.getClassInformation(classOf[Object].getName()) == info.superClass.get)
    assertSetEquals(Set(
      ClassInformationPool.getClassInformation(classOf[DummyInterface].getName())), info.interfaces)
  }

  @Test
  def derivedInformation() {
    val info = ClassInformationPool.getClassInformation(classOf[DerivedDummy].getName())

    assertEquals(classOf[DerivedDummy].getName(), info.name)
    assertSetEquals[MethodInfo](
      Set(
        MethodInfo("<init>", Type.VOID_TYPE, Seq()),
        MethodInfo("publicMethod", Type.VOID_TYPE, Seq()),
        MethodInfo("derivedMethod", Type.VOID_TYPE, Seq())),
      info.declaredMethods)

    info.declaredMethods.foreach {
      _ match {
        case m @ MethodInfo("derivedMethod", _, _) => assert(m.readsObjectHornet,"derivedMethod classInfoAdjuster kicked in")
        case _ =>
      }
    }
    assert(ClassInformationPool.getClassInformation(classOf[BaseDummy].getName()) == info.superClass.get)
    assertSetEquals(Set(
      ClassInformationPool.getClassInformation(classOf[DummyInterface].getName())), info.interfaces)
  }

  @Test
  def checkOverrideableMethods() {
    val info = ClassInformationPool.getClassInformation(classOf[BaseDummy].getName())

    assertSetEquals[MethodInfo](
      Set(
        MethodInfo("clone", Type.getObjectType("java/lang/Object"), Seq()),
        MethodInfo("equals", Type.BOOLEAN_TYPE, Seq(Type.getObjectType("java/lang/Object"))),
        MethodInfo("finalize", Type.VOID_TYPE, Seq()),
        MethodInfo("hashCode", Type.INT_TYPE, Seq()),
        MethodInfo("toString", Type.getObjectType("java/lang/String"), Seq()),

        MethodInfo("publicMethod", Type.VOID_TYPE, Seq()),
        MethodInfo("publicMethod", Type.VOID_TYPE, Seq(Type.INT_TYPE)),
        MethodInfo("interfaceMethod", Type.VOID_TYPE, Seq())),
      info.getOverrideableMethods(classOf[DerivedDummy].getPackage().getName()))
  }

  @Test
  def checkOverrideableConstructors() {
    val info = ClassInformationPool.getClassInformation(classOf[BaseDummy].getName())

    assertSetEquals[MethodInfo](
      Set(
        MethodInfo("<init>", Type.VOID_TYPE, Seq())),
      info.getOverrideableConstructors(classOf[DerivedDummy].getPackage().getName()))
  }

  @Test
  def objectCloneOverrideable() {
    val info = ClassInformationPool.getClassInformation(classOf[Object].getName())
    val cloneInfo = info.declaredMethods.find(_.name.equals("clone")).get
    assert(cloneInfo.isProtected)
    assert(cloneInfo.isOverrideable("foo"))
  }

  def assertSetEquals[A](a: scala.collection.Set[A], b: scala.collection.Set[A]) {
    for (t <- a) {
      assertTrue("b should contain " + t+" b= "+b, b.contains(t))
    }

    for (t <- b) {
      assertTrue("a should contain " + t, a.contains(t))
    }
  }
}