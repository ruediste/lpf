package lpf.test.visual

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import lpf.element.GridCursor

class GridCursorTest extends FunSuite with ShouldMatchers {

  test("occupy cell multiple times"){
    val cursor=new GridCursor()
    cursor.occupyCells(1, 1)
    intercept[Exception](cursor.occupyCells(1, 1))
  }
  
  test("initial position"){
    val cursor=new GridCursor()
    cursor.column should equal(0)
    cursor.row should equal(0)
  }
  
  test("go right"){
    val cursor=new GridCursor()
    cursor.goRight()
    cursor.column should equal(1)
  }
  
  test("go right over single occupied cell"){
    val cursor=new GridCursor()
    cursor.occupyCells(1, 1)
    cursor.goRight()
    cursor.column should equal(1)
  }
  
  test("go right over two occupied cells"){
    val cursor=new GridCursor()
    cursor.occupyCells(2, 1)
    cursor.goRight()
    cursor.column should equal(2)
  }
  
  test("newLine"){
    val cursor=new GridCursor()
    cursor.goRight()
    cursor.newLine()
    cursor.column should equal(0)
    cursor.row should equal(1)
  }
  
  test("newLine, occupied cell"){
    val cursor=new GridCursor()
    cursor.occupyCells(1,2)
    cursor.goRight()
    cursor.newLine()
    cursor.column should equal(1)
    cursor.row should equal(1)
  }
  
}