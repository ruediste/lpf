package lpf.test.visual

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import lpf.visual.GridVisual
import lpf.element.Grid
import lpf.element.AbsoluteSize
import lpf.element.FractionalSize
import lpf.element.RubberSize
import lpf.element.RubberSize
import lpf.element.RubberSize

class GridVisualTest extends FunSuite with ShouldMatchers {

  val element = Grid()
  val visual = GridVisual()

  test("empty lanes") {
    val sizes = visual.calculateLaneSizes(100, 0, List(), Map())
    sizes should equal(List(0.0))
  }

  test("empty lanes, zero size") {
    val sizes = visual.calculateLaneSizes(0, 0, List(), Map())
    sizes should equal(List(0.0))
  }

  test("single lane, no size") {
    val sizes = visual.calculateLaneSizes(100, 1, List(None), Map((0, 1) -> 10))
    sizes should equal(List(0.0, 10))
  }

  test("two lanes, no size") {
    val sizes = visual.calculateLaneSizes(100, 2, List(None, None), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 10, 21))
  }

  test("single lane, absolute size") {
    val sizes = visual.calculateLaneSizes(100, 1, List(Some(AbsoluteSize(25))), Map((0, 1) -> 10))
    sizes should equal(List(0.0, 25))
  }

  test("two lanes, absolute size") {
    val sizes = visual.calculateLaneSizes(100, 2, List(Some(AbsoluteSize(25)), Some(AbsoluteSize(27))), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 25, 52))
  }

  test("two lanes, mix no size and absolute size") {
    val sizes = visual.calculateLaneSizes(100, 2, List(None, Some(AbsoluteSize(27))), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 10, 37))
  }

  test("single lane, fractional size") {
    val sizes = visual.calculateLaneSizes(100, 1, List(Some(FractionalSize(0.3))), Map((0, 1) -> 10))
    sizes should equal(List(0.0, 30))
  }

  test("two lanes, fractional size") {
    val sizes = visual.calculateLaneSizes(100, 2, List(Some(FractionalSize(0.25)), Some(FractionalSize(0.27))), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 25, 52))
  }

  test("two lanes, mix absolute and fractional") {
    val sizes = visual.calculateLaneSizes(100, 2, List(Some(AbsoluteSize(15)), Some(FractionalSize(0.8))), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 15, 95))
  }

  test("two lanes, mix no size and fractional") {
    val sizes = visual.calculateLaneSizes(100, 2, List(None, Some(FractionalSize(0.8))), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 10, 90))
  }

  test("single lane, rubber size") {
    val sizes = visual.calculateLaneSizes(100, 1, List(Some(RubberSize(0.4))), Map((0, 1) -> 10))
    sizes should equal(List(0.0, 100))
  }

  test("two lanes, rubber sizes") {
    val sizes = visual.calculateLaneSizes(100, 2, List(Some(RubberSize(4)), Some(RubberSize(6))), Map((0, 1) -> 10, (1, 2) -> 12))
    sizes should equal(List(0.0, 10 + 0.4 * 78, 41.2 + 12.0 + 0.6 * 78))
  }

  test("two lanes, mix no size and rubber size") {
    val sizes = visual.calculateLaneSizes(100, 2, List(None, Some(RubberSize(6))), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 10, 100))
  }

  test("three lanes, multicolumn") {
    val sizes = visual.calculateLaneSizes(100, 3, List(None, None, None), Map((0, 2) -> 10, (1, 3) -> 11))
    sizes should equal(List(0.0, 0.0, 10, 11))
  }

  test("more lanes than lanesizes") {
    val sizes = visual.calculateLaneSizes(100, 2, List(None, None), Map((0, 1) -> 10, (1, 2) -> 11))
    sizes should equal(List(0.0, 10, 21))
  }

  test("colspan") {
    val sizes = visual.calculateLaneSizes(100, 2, List(None, None), Map((0, 1) -> 10, (1, 2) -> 11, (0, 2) -> 30))
    sizes should equal(List(0.0, 10, 30))
  }
}