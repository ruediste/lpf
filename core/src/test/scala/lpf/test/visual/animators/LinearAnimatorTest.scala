package lpf.test.visual.animators

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import lpf.visual.animator.LinearAnimator
import lpf.visual.animator.LinearAnimator

class LinearAnimatorTest extends FunSuite with ShouldMatchers {
  private class TestLinearAnimator extends LinearAnimator {
    override def frame(elapsedTime: Double) = super.frame(elapsedTime)
  }

  test("update  +delta") {
    val anim = new TestLinearAnimator()
    anim.target = 10
    anim.frame(1)
    anim.value should equal(1)
  }

  test("update -delta") {
    val anim = new TestLinearAnimator()
    anim.target = -10.0
    anim.frame(1)
    anim.value should equal(-1)
  }

  test("negative speed rejected") {
    val anim = new TestLinearAnimator()
    intercept[IllegalArgumentException](anim.speed = -1.0)
  }

  test("zero speed rejected") {
    val anim = new TestLinearAnimator()
    intercept[IllegalArgumentException](anim.speed = 0)
  }

  test("-zero speed rejected") {
    val anim = new TestLinearAnimator()
    intercept[IllegalArgumentException](anim.speed = -0.0)
  }

  test("animator initially disabled") {
    val anim = new TestLinearAnimator()
    anim.enabled should equal(false)
  }

  test("animator is enabled") {
    val anim = new TestLinearAnimator()
    anim.target = 0.5
    anim.enabled should equal(true)
  }
  test("animator stops") {
    val anim = new TestLinearAnimator()

    anim.target = 0.5
    anim.frame(1)
    anim.value should equal(0.5)
    anim.enabled should equal(false)
  }
}