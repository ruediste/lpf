package lpf.test

import org.junit.Test
import org.hamcrest.Matcher
import org.jmock.Expectations

package object testBase {
  type RunWith=org.junit.runner.RunWith
  
  type Expectations=org.jmock.Expectations
  type JUnit4= org.junit.runners.JUnit4
  type Assert=org.junit.Assert
  type Test=org.junit.Test
  type After=org.junit.After
  type Before=org.junit.Before
}