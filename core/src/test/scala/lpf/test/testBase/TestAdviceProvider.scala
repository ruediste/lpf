package lpf.test.testBase

import lpf.instrumentation.AdviceProviderBase
import lpf.instrumentation.JoinPoint
import lpf.instrumentation.Advice
import lpf.instrumentation.ClassMember

class TestAdviceProvider extends AdviceProviderBase {
 override def getAdvice(jp: JoinPoint): Option[Advice] = None
}