/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.test.testBase
import org.jmock.integration.junit4.JUnit4Mockery
import org.jmock.lib.legacy.ClassImposteriser
import org.junit.Ignore
import scala.reflect.ClassTag

//@NoDependencyWeaving
//@RunWith(classOf[InstrumentingRunner])
//@RunWith(classOf[BlockJUnit4ClassRunner])
@Ignore
class FixtureBase{
	val context = new JUnit4Mockery() {
    {
      // we want to be able to mock classes
      setImposteriser(ClassImposteriser.INSTANCE);
    }
	};
	
	/**
	 * Run a function, expecting an exception of type A to be thrown. The thrown exception is returned.
	 */
	def intercept[A<:Throwable](func: =>Unit)(implicit tag: ClassTag[A]):A={
		var thrown: Option[A]=None
		try{
			func
		}
		catch{
			case t: Throwable => {
				if (tag.runtimeClass.isAssignableFrom(t.getClass()))
					thrown=Some(t.asInstanceOf[A])
				else
				{
					throw t
				}
			}
		}
		assert(!thrown.isEmpty,"expected exception of type "+tag.runtimeClass.getName());
		return thrown.get
	}
}
