import sbt._
import Keys._
import com.typesafe.sbteclipse.plugin.EclipsePlugin.EclipseKeys

object LpfBuildBuild extends Build {
  private def createT() = {
    import com.typesafe.sbteclipse.plugin.EclipsePlugin.EclipseTransformerFactory
    import com.typesafe.sbteclipse.core._
    import scala.xml.transform.RewriteRule
    import scala.xml.Node
    import scala.xml.Elem

    EclipseKeys.classpathTransformerFactories := Seq(new EclipseTransformerFactory[RewriteRule] {
      override def createTransformer(project: sbt.ProjectRef, state: sbt.State): Validation[RewriteRule] = {
        scalaz.Success(new RewriteRule {
          var rootEmitted = false
          override def transform(n: Node): Seq[Node] = {
            //println(n)
	    val result = n match {
		case e:Elem if(e.label == "classpathentry") => 
		  if ((n \ "@kind" text) == "src") {
                    if ((n \ "@path" text).startsWith("src/"))
                      Seq()
                    else
                      Seq(n)
                  } else
                    Seq(n)

                case e:Elem if(e.label == "classpath") =>
			<classpath>
			{ e.child}
			<classpathentry including="*.scala" kind="src" path=""/>
			</classpath>
		}
             
	    //println("=> "+result)
	    result
          }
        })
      }
    })
  }

  lazy val lpfBuild = Project(
    id = "lpf-build",
    base = file(".") , 
    settings = Defaults.defaultSettings ++ Seq(createT(), EclipseKeys.withSource := true)
  )
}
