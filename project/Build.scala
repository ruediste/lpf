
/*
val Mklauncher = config("mklauncher") extend(Compile)

  val mklauncher = TaskKey[Unit]("mklauncher")
  val mklauncherTask = mklauncher <<= (target, fullClasspath in Runtime) map { (target, cp) =>
    def writeFile(file: File, str: String) {
      val writer = new PrintWriter(file)
      writer.println(str)
      writer.close()
    }
    val cpString = cp.map(_.data).mkString(":")
    val launchString = """
CLASSPATH="%s"
scala -usejavacp -Djava.class.path="${CLASSPATH}" "$@"
""".format(cpString)
    val targetFile = (target / "scala-sbt").asFile
    writeFile(targetFile, launchString)
    targetFile.setExecutable(true)
  }
*/

import sbt._
import Keys._
import com.typesafe.sbteclipse.plugin.EclipsePlugin.EclipseKeys

object BuildSettings {
  val buildOrganization = "RuediSteinmann"
  val buildVersion = "0.1.2"
  val buildScalaVersion = "2.10.0"

  val buildSettings = Defaults.defaultSettings ++ Seq(
    organization := buildOrganization,
    version := buildVersion,
    scalaVersion := buildScalaVersion,
    libraryDependencies ++= Dependencies.unitTesting,
    EclipseKeys.withSource := true,
    scalacOptions += "-deprecation",
    javaOptions in Test <<= (
      packageBin in (LocalProject("lpf-agent"), Compile),
      LpfBuild.generateInstrumentedRt in (LocalProject("lpf-rtInstrument"), Compile)) map {
        (agent: File, newrt: File) =>
          import scala.sys.process._
          Seq("-javaagent:" + agent.getAbsolutePath + "=lpf.hornet.HornetAdviceProvider",
            "-Xbootclasspath/p:" + newrt.getAbsolutePath)
      },
    fork in Test := true
    )
}

object Dependencies {
  val asmVer = "4.1"
  val joglVer = "2.0-rc11"
  val jmockVer = "2.5.1"

  val asmUtil = "org.ow2.asm" % "asm-util" % asmVer
  val asmTree = "org.ow2.asm" % "asm-tree" % asmVer
  val asmAnalysis = "org.ow2.asm" % "asm-analysis" % asmVer
  val asmCommons = "org.ow2.asm" % "asm-commons" % asmVer

  val guava = "com.google.guava" % "guava" % "13.0.1"

  val jogampJogl = "org.jogamp.jogl" % "jogl-all" % joglVer
  val jogampGluegen = "org.jogamp.gluegen" % "gluegen-rt" % joglVer

  //val junit = "junit" % "junit" % "4.11" % "test"
  val jmock = "org.jmock" % "jmock" % jmockVer % "test"
  val jmockJunit4 = "org.jmock" % "jmock-junit4" % jmockVer % "test"
  val jmockLegacy = "org.jmock" % "jmock-legacy" % jmockVer % "test"
  val scalaCheck = "org.scalacheck" % "scalacheck_2.10" % "1.10.0" % "test"
  val scalaTest = "org.scalatest" % "scalatest_2.10.0" % "1.8" % "test"
  val junit = "com.novocode" % "junit-interface" % "0.8" % "test->default"
  val unitTesting = Seq(junit, jmock, jmockJunit4, jmockLegacy, scalaTest, scalaCheck)

  // Sub-project specific dependencies

  val rtInstrumentDeps = Seq(
    asmCommons,
    guava)

  val coreDeps = Seq(
    asmUtil,
    asmTree,
    asmAnalysis)

  val exampleDeps = Seq(
    jogampJogl,
    jogampGluegen)
}

object LpfBuild extends Build {
  import BuildSettings.buildSettings

  // rtInstrument task
  val generateInstrumentedRt = TaskKey[File]("generate-instrumented-rt", "Generates an instrumented version of the java RT library")

  val generateInstrumentedRtTask = generateInstrumentedRt <<= (cacheDirectory, dependencyClasspath in Compile, packageBin in Compile, baseDirectory in LocalProject("lpf-rtInstrument")) map {
    (cache: File, cp: Seq[Attributed[File]], jar: File, dir: File) =>
      import scala.sys.process._

      val cachedFun = FileFunction.cached(cache / "generate-instrumented-rt", FilesInfo.lastModified, FilesInfo.exists) { (in: Set[File]) =>
        val command = Seq("java", "-cp", (jar.getAbsolutePath +: cp.map(_.data.getAbsolutePath())).mkString(":"), "lpf.rt.Main")
        Process(command, dir) !;
        Set(dir / "newrt.jar")
      }

      cachedFun(Set(jar)).head
  }

  val runLpf = TaskKey[Unit]("run-lpf", "Runs a program")

  val runLpfTask = runLpf <<= (
    fullClasspath in Compile,
    mainClass in Compile,
    packageBin in (LocalProject("lpf-agent"), Compile),
    generateInstrumentedRt in (LocalProject("lpf-rtInstrument"), Compile)) map { (cp: Seq[Attributed[File]], main: Option[String], agent: File, newrt: File) =>
      import scala.sys.process._
      val command = Seq(
        "java",
        "-cp", cp.map(_.data.getAbsolutePath()).mkString(":"),
        "-javaagent:" + agent.getAbsolutePath + "=lpf.hornet.HornetAdviceProvider",
        "-Xbootclasspath/p:" + newrt.getAbsolutePath,
        main.get)
      command !
    }

  // Project Definitions

  lazy val lpf = Project(id = "lpf",
    base = file("."),
    settings = buildSettings) aggregate (core, example, agent, rtInstrument)

  lazy val rtInstrument = Project(id = "lpf-rtInstrument",
    base = file("rtInstrument"),
    settings = buildSettings ++ Seq(
      libraryDependencies ++= Dependencies.rtInstrumentDeps,
      generateInstrumentedRtTask))

  lazy val agent = Project(id = "lpf-agent",
    base = file("agent"),
    settings = buildSettings ++ Seq(
      packageOptions in (Compile, packageBin) +=
        Package.ManifestAttributes("Premain-Class" -> "lpf.instrumentation.InstrumentationAgent") //,artifactPath in (Compile, packageBin) <<= target(_ / "agent.zip")
        ))

  lazy val core = Project(id = "lpf-core",
    base = file("core"),
    settings = buildSettings ++ Seq(libraryDependencies ++= Dependencies.coreDeps)) dependsOn (rtInstrument % "compile->compile")

  lazy val example = Project(id = "lpf-example",
    base = file("example"),
    settings = buildSettings ++ Seq(
      libraryDependencies ++= Dependencies.exampleDeps,
      mainClass in Compile := Some("lpfExample.Main"),
      runLpfTask)) dependsOn (core % "compile->compile")
}