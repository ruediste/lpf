The goal of Light Presentation Framework (LPF) is to create a presentation framework which is distilled to the parts strictly necessary. However, it should stay flexible and powerful.

Many GUI frameworks try to provide a solution for every potential problem. They grow very complex. But special requirements often cannot be met directly and the framework has to be customized. This is very hard due to the sheer size of these frameworks.

The LPF chose a different approach. It should be possible to solve the problem at hand through custimization and extension. Due to the manageable size of the framework, we hope this to be more effective than finding the right solution with a large framework.

For more information, see the `wiki <http://bitbucket.org/ruediste/lpf/wiki/>`_

Features
--------

Scala DSL
  While XML is a popular choice for GUI definitions at the moment, it comes with some drawbacks. Therefore a scala DSL is used: ::

    rule { case r: Root => Window() -: Background(Color.WHITE) -: RubberMargin() -: { r.child }	}

Innovative Binding
  Bytecode instrumentation is used to detect which fields code depends on. Whenever one of these fields changes, the GUI is updated. No need to worry about notification anymore.

Custom Appearance
  By separating GUI logic from visual appearance deep in it's design, LPF makes it very easy to create a custom look for your application.

Licence
-------

The LPF is licensed under GPLv3. Commercial licenses are available upon request. See `License 
<http://bitbucket.org/ruediste/lpf/wiki/License>`_
