package lpf.instrumentation

import scala.collection.JavaConversions._

class RtInstrumentationUtilLike {
  def packageName(className: String) = className.split('.').dropRight(1).mkString(".")

  def searchClass(startPackage: String, simpleClassName: String, baseClass: Class[_]): List[Class[_]] =
    searchClass(getClass().getClassLoader(), startPackage, simpleClassName, baseClass)

  /**
   * Searches for a class, starting in the startPackage, probing all packages towards the root
   */
  def searchClass(loader: ClassLoader, startPackage: String, simpleClassName: String, baseClass: Class[_]): List[Class[_]] = {
    var result = List[Class[_]]()

    // iterate over packages
    var currentPackage = new StringBuffer()
    for (part <- startPackage.split('.')) {
      // append the package part to the current package 
      if (currentPackage.length() != 0)
        currentPackage.append(".")
      currentPackage.append(part)

      // build the class name to attempt to load from the current package
      // and the simple class name given
      var className = currentPackage.toString()
      if (className.length() != 0)
        className += "."
      className += simpleClassName

      // try to load the class
      val clazz = tryLoadClass(loader, className, baseClass, true)
      clazz.foreach(result ::= _)
    }

    result
  }

  /**
   * attempt to load a class, which has to derive from a base class
   */
  def tryLoadClass(className: String, baseClass: Class[_], tryMeta: Boolean): Option[Class[_]] =
    tryLoadClass(getClass.getClassLoader(), className, baseClass, tryMeta)

  private val loadClassCache: scala.collection.concurrent.Map[(ClassLoader, String), Option[Class[_]]] 
  = (new com.google.common.collect.MapMaker()).makeMap[(ClassLoader, String), Option[Class[_]]]()

  /**
   * attempt to load a class, which has to derive from a base class
   */
  def tryLoadClass(loader: ClassLoader, className: String, baseClass: Class[_], tryMeta: Boolean): Option[Class[_]] = {
    if (tryMeta) {
      var clazz = tryLoadClass(loader, className, baseClass, false)
      if (clazz.isEmpty)
        clazz = tryLoadClass(loader, ("meta" :: (className.split('.').toList)).mkString("."), baseClass, false)
      return clazz
    }

    // try the cache
    val clazz = loadClassCache.get((loader, className))
    if (clazz.isDefined) {
      // check if there was no class for the (loader,className)
      if (clazz.get.isEmpty) return None

      if (baseClass.isAssignableFrom(clazz.get.get))
        return clazz.get
      else
        // although there was a class for the (loader,className), 
        // the class was not assignable to the base class
        return None
    }

    // handle the base case
    try {
      val clazz = (if (loader != null) loader else ClassLoader.getSystemClassLoader())
        .loadClass(className)

      // put the clazz into the cache
      loadClassCache.put((loader, className), if (clazz == null) None else Some(clazz))

      if (clazz != null && baseClass.isAssignableFrom(clazz))
        return Some(clazz)
    } catch {
      case t: ClassNotFoundException =>
        // just ignore, but remember in the cache
        loadClassCache.put((loader, className), None)
    }
    None
  }
}

object RtInstrumentationUtil extends RtInstrumentationUtilLike{}