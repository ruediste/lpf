package lpf.instrumentation;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.AdviceAdapter;

/**
 * A method visitor to create method enter, exit and finally advice.
 *
 * onMethodEnter() is called at the start of the methdo
 * 
 * onMethodExitImpl() is called on every normal method return
 * 
 * onFinally() is called on every normal method return, and when the method is exited with an exception
 */
public class FinallyAdapter extends AdviceAdapter {
	private Label startFinally = new Label();

	public FinallyAdapter(MethodVisitor mv, int acc, String name, String desc) {
		super(ASM4, mv, acc, name, desc);
	}

	/**
	 * Check if the finally instrumentation is required. Always true.
	 * To be overridden in subclasses.
	 */
	protected boolean isFinallyRequired(){
		return true;
	}
	
	public void visitCode() {
		super.visitCode();
		if (isFinallyRequired())
			mv.visitLabel(startFinally);
	}

	public void visitMaxs(int maxStack, int maxLocals) {
		if (isFinallyRequired()){
			Label endFinally = new Label();
			mv.visitTryCatchBlock(startFinally, endFinally, endFinally, null);
			mv.visitLabel(endFinally);
			onFinally(ATHROW);
			mv.visitInsn(ATHROW);
		}

		mv.visitMaxs(maxStack, maxLocals);
	}

	final protected void onMethodExit(int opcode) {
		if (isFinallyRequired()){
			// if we exit with a throw, the finally block will be visited anyways
			if (opcode != ATHROW) 
			{
				// at a normal exit location, first to the normal exit actions,
				// followed by the finally actions
				onMethodExitImpl(opcode);
				onFinally(opcode);
			}
		}
		else 
		{
			// the methodExit advice is not desired before a throw instruction
			if (opcode !=ATHROW)
				onMethodExitImpl(opcode);
		}
	}

	/**
	 * called on normal method exits
	 */
	protected void onMethodExitImpl(int opcode) {
	}

	/**
	 * called when an exception is thrown
	 * @param opcode
	 */
	protected void onFinally(int opcode) {
	}
}
