package lpf.hornet

import lpf.instrumentation.ClassInformationAdjusterBase

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  override def adjustClassInformation(info: ClassInformation) {
	  info.setApplicationClass(false)
  }
}