package lpf.hornet

import scala.collection.mutable.HashMap
import org.objectweb.asm._
import lpf.Unobserved
import scala.{ collection => co }
import scala.collection.{ mutable => mu }
import org.objectweb.asm.MethodVisitor
import scala.collection.mutable.ArrayBuffer
import lpf.instrumentation._
import scala.collection.{ mutable => mu }
import scala.Predef.Set.apply
import scala.Some.apply
import scala.collection.immutable.List.apply
import scala.collection.{ mutable => mu }
import scala.collection.mutable.ArrayBuffer.apply
import scala.collection.mutable.HashMap.apply
import scala.collection.mutable.Set.apply
import lpf.instrumentation.RtInstrumentationUtil
import scala.reflect.ClassTag

abstract class InfoWithAccess {
  var access: Int
  def isStatic = (access & Opcodes.ACC_STATIC) != 0
  def isFinal = (access & Opcodes.ACC_FINAL) != 0
  def isPublic = (access & Opcodes.ACC_PUBLIC) != 0
  def isProtected = (access & Opcodes.ACC_PROTECTED) != 0
  def isPrivate = (access & Opcodes.ACC_PRIVATE) != 0
  def isInterface = (access & Opcodes.ACC_INTERFACE) != 0
  def isAbstract = (access & Opcodes.ACC_ABSTRACT) != 0
  def isDefaultVisible = (!isPublic && !isPrivate && !isProtected)
}

abstract class MemberInfo extends InfoWithAccess {
  val name: String

  var clazz: ClassInformation

}
case class FieldInfo(name: String, t: Type, isUnobserved: Boolean) extends MemberInfo {
  var access: Int = 0
  var clazz: ClassInformation = null
}

/**
 * Represents a method.
 *
 * All elements of the signature, which has to be unique within a class, are in
 * the constructor arguments. Since it is a case class, they will also be included
 * in the equals() and the hashCode() method. (useful for sets)
 */
case class MethodInfo(name: String, retType: Type, argTypes: Seq[Type]) extends MemberInfo {
  var clazz: ClassInformation = null

  var access: Int = 0
  var desc: String = null

  var readsObjectHornet: Boolean = false
  var writesObjectHornet: Boolean = false
  var isObjectHornetFactory: Boolean = false

  def isConstructor() = name.equals("<init>")
  def isStaticConstructor() = name.equals("<clinit>")
  def accessesObjectHornet: Boolean = readsObjectHornet || writesObjectHornet

  def isOverrideable(fromPackage: String) =
    (isPublic || isProtected || (isDefaultVisible && clazz.packageName.equals(fromPackage))) &&
      !isStatic && !isFinal
}

case class ClassInformation(name: String, superClass: Option[ClassInformation], interfaces: Set[ClassInformation], declaredMethods: Set[MethodInfo], declaredFileds: Set[FieldInfo]) extends InfoWithAccess {

  var access = 0

  // link the methods with the class information
  declaredMethods.foreach(_.clazz = this)
  declaredFileds.foreach(_.clazz = this)

  var isObjectHornet = false
  var usesFieldHornets = false
  var accessesFieldHornets = false
  var instantiatesObjectHornets = false
  private var _isAlreadyInstrumented = false
  def isAlreadyInstrumented = _isAlreadyInstrumented
  private[hornet] def isAlreadyInstrumented_=(b: Boolean) = _isAlreadyInstrumented = b

  def needsInstrumentation: Boolean =
    (!isAlreadyInstrumented) && 
    (instantiatesObjectHornets || usesFieldHornets || accessesFieldHornets || needsObjectHornetInstrumentation)

  private var applicationClassDefined = false

  /**
   * Set the default settings for a normal application class. Only the first invocation
   * of this method on an instance has an effect, further calls are ignored.
   */
  def setApplicationClass(isApplicationClass: Boolean) {
    if (applicationClassDefined) return

    applicationClassDefined = true
    usesFieldHornets = isApplicationClass
    instantiatesObjectHornets = isApplicationClass
    accessesFieldHornets = isApplicationClass
  }
  /**
   * package of the class
   */
  def packageName = RtInstrumentationUtil.packageName(name)

  /**
   * Get the methods which are overrideable from within the given package.
   *
   * For each method signature, the list of definitions is retrieved. The first
   * definition, which is the one furthest down in the inheritance tree, has to
   * be overrideable
   */
  def getOverrideableMethods(fromPackage: String): co.Set[MethodInfo] =
    getOverrideableMethodsWithOverriddenMethods(fromPackage).map(_.head).toSet

  /**
   * Get the methods which are overrideable from within the given package, along with all overridden versions
   * and definitions from iterfaces
   */
  def getOverrideableMethodsWithOverriddenMethods(fromPackage: String): Iterable[Seq[MethodInfo]] =
    getMethods().values.filter(l => l.head.isOverrideable(fromPackage) && !l.head.isConstructor())

  /**
   * Get a map from each method signature in the class hierarchy to all declared method infos.
   * The resulting lists are ordered in such a way, that method declarations of a subclass is always
   * before the declarations of the superclass. (or an interface)
   */
  def getMethods(): HashMap[MethodInfo, ArrayBuffer[MethodInfo]] = {
    val res = HashMap[MethodInfo, ArrayBuffer[MethodInfo]]()
    getClassesInTree.flatMap(_.declaredMethods).foreach(m => res.getOrElseUpdate(m, ArrayBuffer()).append(m))
    res
  }

  /**
   * return all methods for the given name and description
   */
  def getMethods(name: String, desc: String): ArrayBuffer[MethodInfo] = {
    val t = Type.getMethodType(desc)
    val mi = MethodInfo(name, t.getReturnType(), t.getArgumentTypes())
    return getMethods.get(mi).getOrElse(ArrayBuffer.empty)
  }

  def getDeclaredMethod(name: String, desc: String): Option[MethodInfo] =
    declaredMethods.find(m => m.name.equals(name) && m.desc.equals(desc))

  def getImplementor(name: String, desc: String): Option[MethodInfo] = {
    getMethods(name, desc).find(!_.isAbstract)
  }

  def getOverrideableConstructors(fromPackage: String): co.Set[MethodInfo] = {
    declaredMethods
      .filter(m => m.isConstructor && m.isOverrideable(fromPackage))
  }

  def isOf[T: ClassTag](implicit tag: ClassTag[T]): Boolean = tag.runtimeClass.getName().equals(name)

  /**
   * Get this class and all it's ancestors. The iterable is ordered in such a way that subclasses are always
   * located before their parent classes. The interfaces have no specific order
   */
  def getClassesInTree(): Iterable[ClassInformation] = getClassesInTree(mu.Set())

  /**
   * Test if this class needs any instrumentation regarding object hornet instrumentation.
   * Instrumentation is required as soon as any implemented method accesses the object hornet.
   *
   * If any superclass is an object hornet, this is obviously required. If a subclass is ever
   * an object hornet, this is required too. So we just need to check the existence of a declared
   * method which accesses the object hornet.
   */
  def needsObjectHornetInstrumentation: Boolean = {
    isObjectHornet || declaredMethods.exists(m => !m.isAbstract && m.accessesObjectHornet)
  }

  /**
   * true if this class needs to contain the enabled flag
   */
  def containsHornetEnabledFlag: Boolean = needsObjectHornetInstrumentation && !getSuperClasses.exists(_.needsObjectHornetInstrumentation)

  /**
   * Get the superclasses, ignoring the interfaces
   */
  def getSuperClasses: Iterable[ClassInformation] = superClass.toList ::: superClass.toList.flatMap(_.getSuperClasses)

  def getField(name: String) = getClassesInTree.flatMap(_.declaredFileds).find(_.name.equals(name))

  /**
   * Get this class and all it's ancestors. The iterable is ordered in such a way that subclasses are always
   * located before their parent classes. The interfaces have no specific order
   */
  def getClassesInTree(visitedClasses: mu.Set[String]): Iterable[ClassInformation] = {
    // don't repeatedly process classes
    if (visitedClasses.contains(name)) return List()
    visitedClasses.add(name)

    this :: ((superClass.toList ::: interfaces.toList).flatMap(_.getClassesInTree(visitedClasses)))
  }

  override def toString(): String = name + "|" + superClass.map(_.name).toString() + "|" + interfaces.map(_.name).mkString(",") + "|" + declaredMethods.map(_.name + "()").mkString(",") + "|" + declaredFileds.map(_.name).mkString(",")
}

object ClassInformationPool {

  // maps class names to sets of fields which are annotated
  private val cache = HashMap[String, ClassInformation]()

  /**
   * if None is returned, the class does not exist
   */
  def getClassInformation(className: String, bytes: Option[Array[Byte]] = None): ClassInformation = this synchronized {
    if (bytes.isDefined) {
      val info = readClass(className, bytes)
      cache.put(className, info)
      info
    } else
      cache.getOrElseUpdate(className, readClass(className, bytes))
  }

  /**
   * read the given class and return the set of annotated methods
   */
  private def readClass(className: String, bytes: Option[Array[Byte]]): ClassInformation = {
    val cl = bytes.map(new ClassReader(_)).getOrElse(try { new ClassReader(className) } catch {
      case t: Throwable => throw new Error("could not load " + className, t)
    })

    var fields = Set[FieldInfo]()
    var methods = Set[MethodInfo]()
    var superName = ""
    var interfaceNames: Seq[String] = null
    var access: Int = 0
    var isAlreadyInstrumented = false

    cl.accept(new ClassVisitor(Opcodes.ASM4) {
      override def visit(version: Int,
        myAccess: Int,
        name: String,
        signature: String,
        superName1: String,
        interfaces: Array[String]) {
        superName = superName1
        interfaceNames = interfaces.toList
        access = myAccess
      }

      override def visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor = {
        // check if the annotation is the unobserved annotation
        if (desc.equals("Llpf/rt/$lpf$Instrumented;"))
          isAlreadyInstrumented = true
        return null;
      }

      // visit all fields
      override def visitField(access: Int, name: String, desc: String, signature: String, value: Any): FieldVisitor = {
        return new FieldVisitor(Opcodes.ASM4) {
          var isUnobserved = false
          // visit the annotations of the field
          override def visitAnnotation(desc: String, visible: Boolean): AnnotationVisitor = {
            // check if the annotation is the unobserved annotation
            if (desc.equals(Type.getDescriptor(classOf[Unobserved])))
              isUnobserved = true

            return null;
          }

          // at the end of visiting the field, put the field information into the map
          override def visitEnd() {
            val info = FieldInfo(name, Type.getObjectType(desc), isUnobserved)
            info.access = access
            fields += info
          }
        };
      }

      override def visitMethod(access: Int, name: String, desc: String, signature: String, exceptions: Array[String]): MethodVisitor = {
        val t = Type.getMethodType(desc)
        val mi = MethodInfo(name, t.getReturnType(), t.getArgumentTypes())
        mi.access = access
        mi.desc = desc
        methods += mi
        return null;
      }

    }, Opcodes.ASM4)

    val interfaces: Set[ClassInformation] = interfaceNames.toSet.map { name: String =>
      this.getClassInformation(Type.getObjectType(name).getClassName())
    }

    val info = ClassInformation(
      className,
      if (superName == null) None else Some(getClassInformation(Type.getObjectType(superName).getClassName())),
      interfaces,
      methods, fields)
    info.access = access
    info.isAlreadyInstrumented = isAlreadyInstrumented

    // adjust the information
    var adjusterClasses = RtInstrumentationUtil.searchClass(
      RtInstrumentationUtil.packageName(className), "ClassInformationAdjuster",
      classOf[ClassInformationAdjusterBase])

    adjusterClasses.foreach { adjusterClass =>
      try {
        val adjuster: ClassInformationAdjusterBase = adjusterClass.newInstance().asInstanceOf[ClassInformationAdjusterBase]
        adjuster.adjustClassInformation(info)
      } catch {
        case t: Throwable => throw new Error("Error while instantiating " + adjusterClass, t)
      }
    }

    return info
  }
}