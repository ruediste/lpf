package lpf.rt

import org.objectweb.asm._
import lpf.hornet.ClassInformation
import lpf.instrumentation.FinallyAdapter
import scala.collection.mutable.ListBuffer
import org.objectweb.asm.commons.JSRInlinerAdapter

class ObjectHornetClassVisitor(info: ClassInformation, cv: ClassVisitor) extends ClassVisitor(Opcodes.ASM4, cv) {
  var className = ""
  override def visit(version: Int,
    access: Int,
    name: String,
    signature: String,
    superName: String,
    interfaces: Array[String]) {
    this.className = name
    val interfacesBuffer = interfaces.toBuffer
    
    interfacesBuffer.append("lpf/rt/ObjectHornet")
    super.visit(version, access, name, signature, superName, interfacesBuffer.toArray);
  }

  override def visitMethod(methodAccess: Int, methodName: String, methodDesc: String, methodSignature: String, methodExceptions: Array[String]): MethodVisitor = {
    // get the method visitor to forward to
    var mv = super.visitMethod(methodAccess, methodName, methodDesc, methodSignature, methodExceptions)

    if ((methodAccess & Opcodes.ACC_ABSTRACT) == 0) {
      val methodInfo = info.getDeclaredMethod(methodName, methodDesc).get

      val isReading = methodInfo.readsObjectHornet
      val isWriting = methodInfo.writesObjectHornet

      mv = new FinallyAdapter(mv, methodAccess, methodName, methodDesc) {
        override def isFinallyRequired(): Boolean = isWriting
        override def onMethodEnter() {
          if (isReading) {
            //println("reading Object Hornet: " + methodName + " " + methodDesc)
            visitVarInsn(Opcodes.ALOAD, 0);
            visitFieldInsn(Opcodes.GETFIELD, className, "$lpf$hornetEventsEnabled", "Z");
            val done = new Label();
            visitJumpInsn(Opcodes.IFEQ, done);
            visitVarInsn(Opcodes.ALOAD, 0);
            visitMethodInsn(Opcodes.INVOKESTATIC, "lpf/rt/MethodCallForwarder", "hornetWillBeRead", "(Ljava/lang/Object;)V");
            visitLabel(done);
          }
        }
        var firstOnFinallyCall = true
        override def onFinally(opcode: Int) {
          if (isWriting) {
            if (firstOnFinallyCall) {
              firstOnFinallyCall = false
              //println("writing Object Hornet: " + methodName + " " + methodDesc)
            }
            visitVarInsn(Opcodes.ALOAD, 0);
            visitFieldInsn(Opcodes.GETFIELD, className, "$lpf$hornetEventsEnabled", "Z");
            val done = new Label();
            visitJumpInsn(Opcodes.IFEQ, done);
            visitVarInsn(Opcodes.ALOAD, 0);
            visitMethodInsn(Opcodes.INVOKESTATIC, "lpf/rt/MethodCallForwarder", "hornetWritten", "(Ljava/lang/Object;)V");
            visitLabel(done);
          }
        }
      }
    }
    mv = new JSRInlinerAdapter(mv, methodAccess, methodName, methodDesc, methodSignature, methodExceptions)
    return mv;
  }

  override def visitEnd() {
    // check if this class is the highest object hornet in the class hierarchy
    if (info.containsHornetEnabledFlag) {
      //rintln("adding HornetEnabled Flag to " + className)
      // if so, add the hornetEventsEnabledField
      val fv = super.visitField(Opcodes.ACC_PROTECTED, "$lpf$hornetEventsEnabled", "Z", null, null)

      // and implement the enableHornetEvents() method
      {
        val mv = super.visitMethod(Opcodes.ACC_PUBLIC, "$lpf$enableHornetEvents", "()V", null, null);
        mv.visitCode();
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitInsn(Opcodes.ICONST_1);
        mv.visitFieldInsn(Opcodes.PUTFIELD, className, "$lpf$hornetEventsEnabled", "Z");
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(2, 1);
        mv.visitEnd();
      }

      fv.visitEnd()
    }

    super.visitEnd()
  }

}