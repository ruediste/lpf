package lpf.rt;


@DummyAnnotation
public class Dummy implements ObjectHornet{
	protected boolean enableHornetEvents=false;
	
	public int readHornet(){
		if (enableHornetEvents)
			MethodCallForwarder.hornetWillBeRead(this);
		return 1;
	}
	
	public void writeHornet(){
		try{
			System.out.println("Hello World");
		}
		finally{
			if (enableHornetEvents)
				MethodCallForwarder.hornetWritten(this);
		}
	}

	@Override
	public void enableHornetEvents() {
		enableHornetEvents=true;
	}
}
