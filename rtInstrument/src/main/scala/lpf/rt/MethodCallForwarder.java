package lpf.rt;

/**
 * Object hornet classes are instrument to invoke the static methods of the
 * method call forwarder. The ObjectHornetEventEnabler is the only place the
 * object hornets can be enabled. Upon loading of the enabler, it will set the
 * receiver of the forwarder.
 */
public class MethodCallForwarder {

	public static MethodCallReceiver receiver = null;

	/**
	 * called before an object hornet is read 
	 */
	static public void hornetWillBeRead(Object obj) {
		//System.out.println("forwarder: will be read");
		receiver.hornetWillBeRead(obj);
	}

	/**
	 * called after an object hornet has been written
	 */
	static public void hornetWritten(Object obj) {
		//System.out.println("forwarder: will be written");
		receiver.hornetWritten(obj);
	}

}
