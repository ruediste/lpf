package lpf.rt;

abstract public class MethodCallReceiver {
	public abstract void hornetWillBeRead(Object obj);
	public abstract void hornetWritten(Object obj);
}
