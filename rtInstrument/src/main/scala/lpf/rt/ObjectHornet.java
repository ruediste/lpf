package lpf.rt;

/**
 * Interface implemented by all object hornets. This implementation will be replaced 
 * in the generated jar with an interface of the same name, but the method will be renamed.
 */
public interface ObjectHornet {
	// will be included in generated version
	// as $lpf$enableHornetEvents
	void enableHornetEvents();
}
