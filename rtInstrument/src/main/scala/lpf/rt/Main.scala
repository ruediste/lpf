package lpf.rt

import scala.collection.JavaConversions._
import java.util.zip.ZipFile
import lpf.hornet.ClassInformationPool
import org.objectweb.asm._
import java.util.zip.ZipOutputStream
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import scala.collection.mutable.ListBuffer
import org.objectweb.asm.commons.JSRInlinerAdapter

class Main {

}

object Main {
  def main(args: Array[String]) {
    val outputStream = new ZipOutputStream(new FileOutputStream("newrt.jar"))

    instrumentJarContainingClass(outputStream, classOf[Object])
    //instrumentJarContainingClass(outputStream, classOf[ListBuffer[_]])

    createObjectHornetInterface(outputStream);
    createObjectHornetEnablerHelper(outputStream);
    createInstrumentedAnnotation(outputStream)

    copyClass(outputStream, classOf[MethodCallForwarder].getName())
    copyClass(outputStream, classOf[MethodCallReceiver].getName())

    outputStream.close()
  }

  private def instrumentJarContainingClass(outputStream: ZipOutputStream, clazz: Class[_]) {
    val url = getClass().getClassLoader().getResource(clazz.getName().replace(".", "/") + ".class");
    var s = url.toExternalForm().substring(0, url.toExternalForm().lastIndexOf(clazz.getPackage().getName().replace(".", "/")));
    println(s);

    // remove pre- and postfix
    s = s.substring("jar:file:".length());
    s = s.substring(0, s.length() - 2);

    // iterate over all entries in the JAR
    val inputZip = new ZipFile(s).entries();
    while (inputZip.hasMoreElements()) {
      val entry = inputZip.nextElement();

      // skip directories and non-class files
      if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
        // load the class information
        val className = entry.getName().substring(0, entry.getName().length() - ".class".length()).replace('/', '.');
        val info = try {
          ClassInformationPool.getClassInformation(className)
        } catch {
          case t: Throwable => throw new Error("error while processing " + className, t)
        }

        // check if the class is an object hornet
        if (info.needsObjectHornetInstrumentation) {
          println("instrumenting " + className);
          val reader = new ClassReader(className)

          val writer = new ClassWriter(reader, ClassWriter.COMPUTE_FRAMES)

          var cv: ClassVisitor = writer
          cv = new ObjectHornetClassVisitor(info, cv)

          // add the modified class to the output
          reader.accept(cv, ClassReader.EXPAND_FRAMES)
          val bytes = writer.toByteArray()
          outputStream.putNextEntry(new ZipEntry(entry.getName()))
          outputStream.write(bytes, 0, bytes.length)
        }

      }
    }
  }

  private def copyClass(outputStream: ZipOutputStream, className: String) {
    println(className);
    val reader = new ClassReader(className)

    val writer = new ClassWriter(reader, ClassWriter.COMPUTE_FRAMES)
    // add the modified class to the output
    reader.accept(writer, ClassReader.EXPAND_FRAMES)
    val bytes = writer.toByteArray()
    outputStream.putNextEntry(new ZipEntry(className.replace('.', '/') + ".class"))
    outputStream.write(bytes, 0, bytes.length)
  }

  private def createObjectHornetInterface(outputStream: ZipOutputStream) {
    val cw = new ClassWriter(0);

    cw.visit(Opcodes.V1_6, Opcodes.ACC_PUBLIC + Opcodes.ACC_ABSTRACT + Opcodes.ACC_INTERFACE, "lpf/rt/ObjectHornet", null, "java/lang/Object", null);

    {
      val mv = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_ABSTRACT, "$lpf$enableHornetEvents", "()V", null, null);
      mv.visitEnd();
    }
    cw.visitEnd();

    val bytes = cw.toByteArray()
    outputStream.putNextEntry(new ZipEntry("lpf/rt/ObjectHornet.class"))
    outputStream.write(bytes, 0, bytes.length)
  }

  private def createInstrumentedAnnotation(outputStream: ZipOutputStream) {
    val cw = new ClassWriter(0);

    cw.visit(Opcodes.V1_6, Opcodes.ACC_PUBLIC + Opcodes.ACC_ANNOTATION + Opcodes.ACC_ABSTRACT + Opcodes.ACC_INTERFACE,
      "lpf/rt/$lpf$Instrumented", null, "java/lang/Object", Array[String]("java/lang/annotation/Annotation"));
    cw.visitEnd();

    val bytes = cw.toByteArray()
    outputStream.putNextEntry(new ZipEntry("lpf/rt/$lpf$Instrumented.class"))
    outputStream.write(bytes, 0, bytes.length)
  }

  private def createObjectHornetEnablerHelper(outputStream: ZipOutputStream) {
    val cw = new ClassWriter(0);
    cw.visit(Opcodes.V1_6, Opcodes.ACC_PUBLIC + Opcodes.ACC_SUPER, "lpf/rt/ObjectHornetEventEnablerHelper", null, "java/lang/Object", null);

    {
      val mv = cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
      mv.visitCode();
      mv.visitVarInsn(Opcodes.ALOAD, 0);
      mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
      mv.visitInsn(Opcodes.RETURN);
      mv.visitMaxs(1, 1);
      mv.visitEnd();
    }
    {
      val mv = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "enableHornetEvents", "(Llpf/rt/ObjectHornet;)V", null, null);
      mv.visitCode();
      mv.visitVarInsn(Opcodes.ALOAD, 0);
      mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, "lpf/rt/ObjectHornet", "$lpf$enableHornetEvents", "()V");
      mv.visitInsn(Opcodes.RETURN);
      mv.visitMaxs(1, 1);
      mv.visitEnd();
    }

    {
      val mv = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "enableHornetEvents", "(Ljava/lang/Object;)V", null, null);
      mv.visitCode();

      mv.visitVarInsn(Opcodes.ALOAD, 0);
      val l1 = new Label();
      mv.visitJumpInsn(Opcodes.IFNULL, l1);
      mv.visitVarInsn(Opcodes.ALOAD, 0);
      mv.visitTypeInsn(Opcodes.INSTANCEOF, "lpf/rt/ObjectHornet");
      mv.visitJumpInsn(Opcodes.IFEQ, l1);

      mv.visitVarInsn(Opcodes.ALOAD, 0);
      mv.visitTypeInsn(Opcodes.CHECKCAST, "lpf/rt/ObjectHornet");
      mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, "lpf/rt/ObjectHornet", "$lpf$enableHornetEvents", "()V");
      mv.visitLabel(l1);
      mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
      mv.visitInsn(Opcodes.RETURN);
      mv.visitMaxs(1, 1);
      mv.visitEnd();
    }
    cw.visitEnd();

    val bytes = cw.toByteArray()
    outputStream.putNextEntry(new ZipEntry("lpf/rt/ObjectHornetEventEnablerHelper.class"))
    outputStream.write(bytes, 0, bytes.length)
  }
}