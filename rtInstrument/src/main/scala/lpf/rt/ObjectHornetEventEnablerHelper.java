package lpf.rt;

/**
 * Helper class for enabling object hornets. It will be replaced by a generated class,
 * since it has to call methods with special names. Should not be used directly, only
 * through the ObjectHornetEventEnabler.
 */
public class ObjectHornetEventEnablerHelper {

	public static void enableHornetEvents(Object obj) {
		// will be replaced by generated method
		System.out.println("original Invoked");
		if (obj != null && obj instanceof ObjectHornet)
			((ObjectHornet) obj).enableHornetEvents();
	}

	// will be replaced by generated method
	public static void enableHornetEvents(ObjectHornet obj) {
		// will be replaced by generated method
		obj.enableHornetEvents();
	}
}
