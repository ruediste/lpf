package meta.java.awt.geom
import lpf.hornet.ClassInformation
import lpf.hornet.MethodInfo
import lpf.instrumentation.ClassInformationAdjusterBase

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  def adjustClassInformation(info: ClassInformation) {
    if (info.isOf[java.awt.geom.AffineTransform]) {
      for (m <- info.declaredMethods) {
        if (m.isStatic && !m.isStaticConstructor && !m.isPrivate)
          m.isObjectHornetFactory = true
        else if (!m.isPrivate && !m.isConstructor && !m.isStatic)
          m match {
            case MethodInfo("createInverse", _, _) => m.readsObjectHornet = true
            case MethodInfo("transform", _, _) => m.readsObjectHornet = true
            case MethodInfo("inverseTransform", _, _) => m.readsObjectHornet = true
            case MethodInfo("deltaTransform", _, _) => m.readsObjectHornet = true
            case MethodInfo("createTransformedShape", _, _) => m.readsObjectHornet = true
            case MethodInfo("toString", _, _) => m.readsObjectHornet = true
            case MethodInfo("isIdentity", _, _) => m.readsObjectHornet = true
            case MethodInfo("clone", _, _) => m.readsObjectHornet = true
            case MethodInfo("hashCode", _, _) => m.readsObjectHornet = true
            case MethodInfo("equals", _, _) => m.readsObjectHornet = true
            case _ =>
              if (m.name.startsWith("get")) {
                //println("reading hornet " + m)
                m.readsObjectHornet = true
              } else {
                //println("writing hornet " + m)
                m.writesObjectHornet = true
              }
          }
      }
    }
  }
}