package meta.java.lang
import lpf.hornet.ClassInformation
import lpf.hornet.MethodInfo
import lpf.instrumentation.ClassInformationAdjusterBase

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  def adjustClassInformation(info: ClassInformation) {
    if (info.name.equals("java.lang.Object")) {
      /*
       * nothing to do here since
       * * hashCode(): const
       * * equals(): identity => const
       * * toString(): className+hashCode() => const
       * * clone(): hard to handle => not handled
       */
    }
  }
}