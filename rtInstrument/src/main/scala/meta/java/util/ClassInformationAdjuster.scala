package meta.java.util
import lpf.hornet.ClassInformation
import lpf.hornet.MethodInfo
import lpf.instrumentation.ClassInformationAdjusterBase
import java.util._
import org.objectweb.asm.Type

class ClassInformationAdjuster extends ClassInformationAdjusterBase {
  def adjustClassInformation(info: ClassInformation) {
    if (info.isOf[java.util.List[_]]) {
      for (m <- info.declaredMethods)
        m match {
          case MethodInfo("size", _, _) => m.readsObjectHornet = true
          case MethodInfo("add", _, _) => m.writesObjectHornet = true
          case _ =>
        }
    }

    if (info.isOf[java.util.AbstractCollection[_]]) {
      info.isObjectHornet = true;
      for (m <- info.declaredMethods)
        m match {
          case MethodInfo("isEmpty", _, _) => m.readsObjectHornet = true
          case MethodInfo("contains", _, _) => m.readsObjectHornet = true
          case MethodInfo("toArray", _, _) => m.readsObjectHornet = true
          case MethodInfo("remove", _, _) => m.readsObjectHornet; m.writesObjectHornet = true
          case MethodInfo("containsAll", _, _) => m.readsObjectHornet = true
          case MethodInfo("addAll", _, _) => m.readsObjectHornet; m.writesObjectHornet = true
          case MethodInfo("removeAll", _, _) => m.readsObjectHornet; m.writesObjectHornet = true
          case MethodInfo("retainAll", _, _) => m.readsObjectHornet; m.writesObjectHornet = true
          case MethodInfo("clear", _, _) => m.writesObjectHornet = true
          case MethodInfo("toString", _, _) => m.readsObjectHornet = true
          case _ =>
        }
    }

    if (info.isOf[java.util.AbstractList[_]]) {
      info.isObjectHornet = true;
      for (m <- info.declaredMethods)
        m match {
          case MethodInfo("indexOf", _, _) => m.readsObjectHornet = true
          case MethodInfo("lastIndexOf", _, _) => m.readsObjectHornet = true
          case MethodInfo("addAll", _, _) => m.readsObjectHornet = true; m.writesObjectHornet = true
          case MethodInfo("equals", _, _) => m.readsObjectHornet = true
          case MethodInfo("hashCode", _, _) => m.readsObjectHornet = true
          case MethodInfo("removeRange", _, _) => m.writesObjectHornet = true
          case _ =>
        }
    }

    // abstractSequentialList needs no adjustments

    if (info.isOf[java.util.LinkedList[_]]) {
      info.isObjectHornet = true;
      for (m <- info.declaredMethods)
        m match {
          case MethodInfo("getFirst", _, _) => m.readsObjectHornet = true
          case MethodInfo("getLast", _, _) => m.readsObjectHornet = true
          case MethodInfo("size", _, _) => m.readsObjectHornet = true
          case MethodInfo("addAll", _, _) => m.readsObjectHornet = true; m.writesObjectHornet = true
          case MethodInfo("clear", _, _) => m.writesObjectHornet = true

          case MethodInfo("set", _, _) => m.writesObjectHornet = true
          case MethodInfo("entry", _, _) => m.readsObjectHornet = true
          case MethodInfo("indexOf", _, _) => m.readsObjectHornet = true
          case MethodInfo("lastIndexOf", _, _) => m.readsObjectHornet = true
          case MethodInfo("peek", _, _) => m.readsObjectHornet = true
          case MethodInfo("peekFirst", _, _) => m.readsObjectHornet = true
          case MethodInfo("peekLast", _, _) => m.readsObjectHornet = true
          case MethodInfo("poll", _, _) => m.readsObjectHornet = true
          case MethodInfo("pollFirst", _, _) => m.readsObjectHornet = true
          case MethodInfo("pollLast", _, _) => m.readsObjectHornet = true
          case MethodInfo("removeLastOccurrence", _, _) => m.readsObjectHornet = true
          case MethodInfo("clone", _, _) => m.readsObjectHornet = true
          case MethodInfo("toArray", _, _) => m.readsObjectHornet = true
          case MethodInfo("writeObject", _, _) => m.readsObjectHornet = true
          case MethodInfo("readObject", _, _) => m.writesObjectHornet = true

          case MethodInfo("addBefore", _, _) if (m.desc.equals("(Ljava/lang/Object;Ljava/util/LinkedList$Entry;)Ljava/util/LinkedList$Entry;")) =>
            m.readsObjectHornet = true; m.writesObjectHornet = true

          case MethodInfo("remove", _, _) if (m.desc.equals("(Ljava/util/LinkedList$Entry;)Ljava/lang/Object;")) =>
            m.readsObjectHornet = true; m.writesObjectHornet = true

          case MethodInfo("remove", _, _) => m.readsObjectHornet = true // write will be hadled by remove() method above
          case _ =>
        }
    }
  }
}