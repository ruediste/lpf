package lpf.instrumentation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * class loader loading as much as possible by itself, such that a transformer
 * does not lead to untransformed classes
 */
class TransformerClassLoader extends ClassLoader {

	@Override
	protected synchronized Class<?> loadClass(String name, boolean resolve)
			throws ClassNotFoundException {

		// skip classes in java., we can't define them anyways
		if (name.startsWith("java.") || 
				name.startsWith("sun.") // stuff in the sun package could brake reflection, and we're instrumenting it anyways by handling the rt classes
				)
			return super.loadClass(name, resolve);

		// skip the AdviceProviderBase

		// First, check if the class has already been loaded
		Class<?> c = findLoadedClass(name);

		if (c == null) {
			c = findClass(name);
		}

		if (resolve) {
			resolveClass(c);
		}
		return c;
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		String resourceName = name.replace('.', '/') + ".class";

		// try to load the class from all class loaders
		InputStream is = getResourceAsStream(resourceName);

		if (is == null)
			throw new ClassNotFoundException("No resource found for class "
					+ name);

		byte[] bb;
		try {
			bb = toByteArray(is);
		} catch (Throwable e) {
			throw new Error("Error while loading " + name, e);
		}
		return defineClass(name, bb, 0, bb.length);
	}

	  public static byte[] toByteArray(InputStream in) throws IOException {
		    ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[4096];
			while (true) {
			  int r = in.read(buf);
			  if (r == -1) {
			    break;
			  }
			  out.write(buf, 0, r);
			}
		    return out.toByteArray();
		  }

}
