/*******************************************************************************
 * Light Presentation Framework (<http://bitbucket.org/ruediste/lpf>)
 * Copyright (C) 2012 Ruedi Steinmann
 * 
 * Light Presentation Framework commercial licenses may be obtained
 * at <http://bitbucket.org/ruediste/lpf/wiki/License>. If you do
 * not own a commercial license, this file shall be governed by the
 * GNU General Public License (GPL) version 3.
 * 
 * You should have received a copy of the GPL along with this program.
 * If not, or for further information concerning the GPL,
 * see <http://www.gnu.org/copyleft/gpl.html>.
 ******************************************************************************/
package lpf.instrumentation;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.Vector;

public class InstrumentationAgent {
	private static TransformerClassLoader loader = new TransformerClassLoader();
	
	private static class MyClassFileTransformer implements ClassFileTransformer{

		private final ClassFileTransformer transformer;

		public MyClassFileTransformer(ClassFileTransformer transformer) {
			this.transformer = transformer;
		}
		
		@Override
		synchronized public byte[] transform(ClassLoader loader, String className,
				Class<?> classBeingRedefined,
				ProtectionDomain protectionDomain, byte[] classfileBuffer)
				throws IllegalClassFormatException {
			// skip classes of the TransformerClassLoader
			if (loader==InstrumentationAgent.loader)
				return classfileBuffer;
			return transformer.transform(loader, className, classBeingRedefined, protectionDomain, classfileBuffer);
		}	
	}
	
	
	@SuppressWarnings("unused")
	public static void premain(String agentArgument,
			Instrumentation instrumentation) {
		//ClassLoader loader = ClassLoader.getSystemClassLoader(); // WeavingAgent.class.getClassLoader();
		try {
			// load the transformer
			Class<?> transformerClass = loader
					.loadClass("lpf.instrumentation.InstrumentingTransformer");
			ClassFileTransformer transformer = (ClassFileTransformer) transformerClass
					.getConstructor(String.class).newInstance(agentArgument);

			// register the transformer
			instrumentation.addTransformer(new MyClassFileTransformer(transformer), false);

			// skip class retransformation for now
			if (false) {
				// get all modifiable classes
				Vector<Class<?>> modifiableClasses = new Vector<Class<?>>();
				for (Class<?> clazz : instrumentation.getAllLoadedClasses())
					if (instrumentation.isModifiableClass(clazz))
						modifiableClasses.add(clazz);

				// retransform the classes
				instrumentation.retransformClasses(modifiableClasses
						.toArray(new Class<?>[] {}));
			}

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
